#! /bin/sh

# from https://stackoverflow.com/a/21188136
abspath() {
  # $1 : relative filename
  if [ -d "$(dirname "$1")" ]; then
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
  fi
}

SOURCE_DIR=$(dirname "$(readlink -f "$0")")/..

if [ -z "$TMPDIR" ]; then
    TMPDIR=/tmp
fi

if [ -z "$BFA_TEMP_DIR" ]; then
    BFA_TEMP_DIR=$TMPDIR/backfromaltair_temp
fi
BFA_TEMP_DIR=$(abspath "$BFA_TEMP_DIR")

BUILD_DIR=$BFA_TEMP_DIR/backfromaltair/build/web

PYGBAG_VERSION=0.9
CDN_NAME=cdn
CDN_URL=/backfromaltair/$CDN_NAME/$PYGBAG_VERSION
CDN_DIR=$BUILD_DIR/$CDN_NAME
PYGBAG_CDN_REPO=https://github.com/pygame-web/archives.git
CDN_REPO_DIR="$BFA_TEMP_DIR/pygame-web-archives"

if ! command -v pygbag > /dev/null; then
    echo "pygbag not found. You should probably install it with python3 -m pip install pygbag"
    exit 1;
fi

for cmd in rsync wget git; do
    if ! command -v $cmd > /dev/null; then
        echo "$cmd not found."
        exit 1
    fi
done

if [ "$1" = "--clean" ]; then
    rm -rf $BFA_TEMP_DIR
    echo "Cleaned $BFA_TEMP_DIR"
fi

echo "Temp dir: $BFA_TEMP_DIR"
mkdir -p $BFA_TEMP_DIR/backfromaltair/ || { echo "Cannot mkdir"; exit 1; }

# Generate data.py
"$SOURCE_DIR/script/generate_events.py" "$SOURCE_DIR/events/events.toml" "$SOURCE_DIR/src/backfromaltair/logic/data.py"

# Copy source
# Assets are excluded because they will be copied later. They are not embedded
# in the web version to save bandwidth and improve loading times.
rsync -av --delete --exclude __pycache__ --exclude '*.pyc' --exclude 'assets' "$SOURCE_DIR/src/backfromaltair" $BFA_TEMP_DIR/backfromaltair/
mv "$BFA_TEMP_DIR/backfromaltair/backfromaltair/__main__.py" $BFA_TEMP_DIR/backfromaltair/main.py

# Download collision library
if [ ! -d $BFA_TEMP_DIR/backfromaltair/collision ]; then
    wget -q https://github.com/qwertyquerty/collision/raw/fdf9f5df6e083b618d6b4c6d1af5fc021e7e1c89/dist/collision-1.2.2.tar.gz -O $BFA_TEMP_DIR/collision.tar.gz
    tar --directory $BFA_TEMP_DIR -xf $BFA_TEMP_DIR/collision.tar.gz
    cp -r $BFA_TEMP_DIR/collision-1.2.2/collision/ $BFA_TEMP_DIR/backfromaltair/
fi

mkdir -p $BUILD_DIR

# Download pygame-web files
# See https://stackoverflow.com/a/56504849
if [ ! -d "$CDN_REPO_DIR" ]; then
    echo "Downloading pygame-web's CDN files"
    git clone --no-checkout --depth=1 --filter=tree:0 "$PYGBAG_CDN_REPO" $CDN_REPO_DIR
    (
        cd $CDN_REPO_DIR || { echo "Couldn't enter cdn repo directory"; exit 1; }
        git sparse-checkout set --no-cone /"$PYGBAG_VERSION" '/repo/*/numpy-*' '/repo/*/pygame_static-*' '/repo/*.json'
        git checkout
    ) || { echo "An error occured during the sparse-checkout of pygbag's CDN git repo"; exit 1; }
fi
echo "Copying pygame-web's CDN files"
rsync --delete -av "$CDN_REPO_DIR/" "$CDN_DIR/" --exclude .git

cp "$SOURCE_DIR"/web/favicon* "$SOURCE_DIR/web/wsbridge.js" "$SOURCE_DIR/web/manifest.json" "$SOURCE_DIR/web/about.html" "$SOURCE_DIR/web/exit.html" "$SOURCE_DIR/web/about.css" $BUILD_DIR/

# zip sound themes so that we can get a sound theme with only one request.
echo "Zipping sound themes"
for f in "$SOURCE_DIR"/src/backfromaltair/assets/sounds/*; do
    if [ -d "$f" ] && [ ! -f "$f.zip" ]; then
        python3 -m zipfile -c "$f.zip" "$f"
    fi
done
mkdir -p $BUILD_DIR/sounds/
cp "$SOURCE_DIR"/src/backfromaltair/assets/sounds/*.zip $BUILD_DIR/sounds/

find $BUILD_DIR/sounds/ -type f -name '*.zip' | sed "s:^$BUILD_DIR/sounds/::" > $BUILD_DIR/sounds/index.txt

# Replace placeholders of sw.js and write output to $BUILD_DIR
sed -E "s/#TIMESTAMP#/$(date --iso-8601=sec)/g;s/#VERSION#/$PYGBAG_VERSION/g" "$SOURCE_DIR/web/sw.js" > $BUILD_DIR/sw.js

# Copy favicon.png next to main.py so that pygbag finds it
cp "$SOURCE_DIR/web/favicon.png" $BFA_TEMP_DIR/backfromaltair/
(
    cd $BFA_TEMP_DIR/backfromaltair/ || exit 1
    pygbag --build --ume_block 0 --cdn $CDN_URL/ --template "$SOURCE_DIR/web/index.tmpl" --app_name "Back from Altair" $BFA_TEMP_DIR/backfromaltair/
) || { echo "An error occured during the pygbag build"; exit 1; }

echo "Fixing the CDN information and applying the CDN patches"
# Fix CDN information
find "$CDN_DIR/" -type f -exec sed -i 's,https://pygame-web.github.io/archives/,/backfromaltair/cdn/,g' '{}' \+
# Apply patches to redirect traffic to pygame-web.github.io to our own CDN
(
    cd "$BUILD_DIR" || { echo "Couldn't cd into BUILD_DIR=$BUILD_DIR"; exit 1; }
    for patch_file in "$SOURCE_DIR/web/patches/"*; do
        patch -p1 < "$patch_file"
    done
) || { echo "An error occured during the application of the patches"; exit 1; }

# Copy $BUILD_DIR to $SOURCE_DIR/public, that's what's used in GitLab Pages
rsync -av --delete $BUILD_DIR/ "$SOURCE_DIR/public/"

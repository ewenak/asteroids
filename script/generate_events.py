#! /usr/bin/env python3

from contextlib import nullcontext
import datetime
from pathlib import Path
import struct
import sys
import tomllib

NUMPY_DTYPE = '"<f8"'

event_type_field = {
    'name': 'event_type',
    'class': 'EventType',
    'bintype': 'B',
    'fieldtype': 'enum',
}
timestamp_field = {
    'name': 'timestamp',
    'class': 'float',
    'bintype': 'f',
    'default': 'None',
    'fieldtype': 'plain',
}


def generate_header(data, relative_path):
    relative_path = Path(relative_path)

    header = []
    if data.get('header_code_file') is not None:
        with open(relative_path / data.get('header_code_file')) as f:
            header.append(f.read())

    generation_comment = data.get('generation_comment')
    if generation_comment is None:
        generation_comment = """

##########################################################################
# The rest of this file was automatically generated using
# {program} on {timestamp} UTC
##########################################################################

"""

    generation_comment = generation_comment.format(
        program=Path(__file__).relative_to(Path(__file__).parent.parent),
        timestamp=datetime.datetime.now(datetime.UTC).isoformat()
    )

    header.append(generation_comment)

    return '\n'.join(header)


def generate_event_type_enum(events):
    types = '\n'.join(f'{name} = {ev["id"]}' for name, ev in events.items())
    return f"""class EventType(Enum):
    {indent(4, types)}
"""


def indent(number, string):
    return string.replace('\n', '\n' + number * ' ')


def snake_case_to_camel_case(string):
    return ''.join(s.capitalize() for s in string.split('_'))


def field_definition(field):
    annotation = f": Annotated[{field['class']}, '{field['bintype']}']"
    if field.get("default", None) is not None:
        default = f" = {field['default']}"
    elif field.get("default_factory", None) is not None:
        default = f" = field(default_factory={field['default_factory']})"
    else:
        default = ""
    return f"{field['name']}{annotation}{default}"


def field_serialization(field, obj='self'):
    if field['fieldtype'] == 'enum':
        return f'{obj}.{field["name"]}.value'
    elif field['fieldtype'] == 'nparray':
        return (f'({obj}.{field["name"]}.tobytes() if {obj}.{field["name"]} '
                'is not None else b"\\0")')
    elif field['fieldtype'] == 'multifield':
        return f'{obj}.{field["name"]}.dumps()'
    elif field['fieldtype'] == 'string':
        return f'{obj}.{field["name"]}.encode("utf-8")'
    else:
        return f'{obj}.{field["name"]}'


def field_casting(field, var_name, index):
    if field['fieldtype'] == 'enum':
        return f'{field["class"]}({var_name}[{index}])'
    elif field['fieldtype'] == 'nparray':
        return (f'np.frombuffer({var_name}[{index}], dtype={NUMPY_DTYPE})'
                f'.reshape({field["npshape"]})')
    elif field['fieldtype'] == 'multifield':
        return f'{field["class"]}.loads({var_name}[{index}])'
    elif field['fieldtype'] == 'string':
        return f'{var_name}[{index}].decode("utf-8")'
    else:
        return f'{var_name}[{index}]'


def generate_packable_dataclass(class_name, content, event_type=None, *,
                                is_event=False):
    if is_event:
        if event_type is None:
            raise TypeError('please specify the event_type when is_event is '
                            'True')
        fields = (*content['fields'], timestamp_field)
        fields_with_event_type = (event_type_field, *fields)
        # Don't pass the event_type to the class constructor when unpacking
        start_instance_args_index = 1
    else:
        fields_with_event_type = fields = content['fields']
        start_instance_args_index = 0

    fields_string = '\n    '.join(
        field_definition(f)
        for f in fields
    )
    pack_args = ', '.join(
        field_serialization(f) for f in fields_with_event_type)
    instance_args = ', '.join(
        field_casting(f, 'data', i + start_instance_args_index)
        for i, f in enumerate(fields))
    struct_string = ''.join(('!', *(f['bintype'] for f in
                                    fields_with_event_type)))

    post_init_statements = []

    # Always use float64 dtype
    post_init_statements.extend(
        f'self.{f["name"]} = np.array(self.{f["name"]}, dtype={NUMPY_DTYPE})'
        for f in fields if f['fieldtype'] == 'nparray'
    )

    post_init_statements.extend(
        f"""if not isinstance(self.{f["name"]}, {f["class"]}):
    self.{f["name"]} = {f["class"]}(*self.{f["name"]})"""
        for f in fields if f['fieldtype'] == 'multifield'
    )

    post_init_statements.extend(
        f"""self.{f["name"]} = self.{f["name"]}.split('\\0', 1)[0]
if not self.{f["name"]}.isprintable():
    raise ValueError("Not printable string was sent in {f["name"]}")"""
        for f in fields if f['fieldtype'] == 'string'
    )

    if post_init_statements:
        statements = indent(4, '\n'.join(post_init_statements))
        post_init = f"""def __post_init__(self):
    {statements}
"""
    else:
        post_init = ''

    if is_event:
        header = f"""@register_event_type(EventType.{event_type})
@dataclass
class {class_name}(Event):
    event_type: ClassVar = EventType.{event_type}
    local_event: ClassVar = {bool(content.get("local_event", False))}
"""
    else:
        header = f"""
@dataclass
class {class_name}:
"""

    return f"""{header}
    {fields_string}

    {indent(4, post_init)}

    def dumps(self):
        return self._struct.pack(
            {pack_args}
        )

    @classmethod
    def loads(cls, buffer):
        data = cls._struct.unpack(buffer)
        return cls(
            {instance_args}
        )

    @classmethod
    def unpack_from(cls, buffer, offset):
        data = cls._struct.unpack_from(buffer, offset)
        return cls(
            {instance_args}
        ), cls._struct.size

    @classmethod
    def iter_unpack(cls, buffer):
        for data in cls._struct.iter_unpack(buffer):
            yield cls(
                {instance_args}
            )

    {indent(4, content.get("custom_methods", ""))}

{class_name}._struct = struct.Struct('{struct_string}')

"""


def read_data(input_file):
    with open(input_file, 'rb') as f:
        data = tomllib.load(f)

    for f in data['fields']:
        data['fields'][f]['name'] = f

    for ev in data['events']:
        data['events'][ev]['fields'] = [
            data['fields'][field] for field in data['events'][ev]['fields']
        ]

    return data


def generate_file(data, parent_dir, output=None):
    if output is None:
        output = sys.stdout
    output.write(generate_header(data, parent_dir))
    output.write(generate_event_type_enum(data['events']))
    for field in data['fields'].values():
        if field['fieldtype'] != 'multifield':
            continue
        field['fields'] = [
            f if isinstance(f, dict) else data['fields'][f]
            for f in field['fields']
        ]
        size = struct.calcsize('!' + ''.join(
            f['bintype'] for f in field['fields']))
        field['bintype'] = f'{size}s'
        output.write(generate_packable_dataclass(field['class'], field))
    for event_type, event in data['events'].items():
        class_name = f'{snake_case_to_camel_case(event_type)}Event'
        output.write(generate_packable_dataclass(
            class_name, event, event_type, is_event=True))


def main(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description='Generate event dataclasses')
    parser.add_argument('input', help='Input TOML file with events '
                        'description')
    parser.add_argument('output', help='Output file', default='-', nargs='?')

    args = parser.parse_args(argv)

    args.input = Path(args.input)

    with open(args.input, 'rb') as f:
        data = tomllib.load(f)

    data = read_data(args.input)

    if args.output == '-':
        cm = nullcontext(sys.stdout)
    else:
        cm = open(args.output, 'w')

    with cm as output:
        generate_file(data, args.input.parent, output)


if __name__ == '__main__':
    main()

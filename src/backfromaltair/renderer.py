#! /usr/bin/env python3

import logging
import math

import numpy as np
import pygame

from .constants import (BUTTON_FONT_SIZE, BUTTON_MARGIN, DEATH_FEEDBACK_TEXT,
                        DEATH_FEEDBACK_TEXT_COLOR, STATE_FEEDBACK_FONT_SIZE,
                        STATUS_LINE_HEIGHT, ZOOM_CHANGE, ZOOM_TARGET_THRUSTING)
from .frames import container_x, Frame
from .logic.base import get_wrap_translations, SpaceObject
from .logic.data import GameState
from .logic.items import Item
from .logic.objects import Area
from .logic.ships import Ship
from .status_line import StatusLineDrawer

logger = logging.getLogger(__name__)


class Renderer:
    def __init__(self, frame, game, player_id):
        self.frame = frame
        self.game = game

        self.target_zoom = 1.0
        self.zoom_factor = 1.0
        self.zoom_matrix = np.array(((1.0, 0.0), (0.0, 1.0)))

        self.state_indicator = None
        self.player_id = player_id
        # We are "lazy-loading" the player, as it is not always available
        # during the Renderer's initialization
        self.player = self.game.players.get(self.player_id)

        self.status_line_frame = Frame(
            self.frame, (0, 0),
            (container_x, STATUS_LINE_HEIGHT),
            debug_name='status_line_frame',
        )
        self.status_line_drawer = StatusLineDrawer(
            self.status_line_frame, self.player, self)

        self.game_frame = Frame(
            self.frame, (0, STATUS_LINE_HEIGHT),
            (container_x, lambda height: height - STATUS_LINE_HEIGHT),
            debug_name='game_frame',
        )

        self.viewport = np.array(((0, 0), self.game_frame.dimensions))
        self.viewport_center = np.array((0, 0))

        self.death_overlay = self.generate_death_overlay()

    def generate_death_overlay(self):
        death_overlay = pygame.Surface(
            self.frame.dimensions, flags=pygame.SRCALPHA)
        death_overlay.fill('red')

        # Originally, arr was defined like so:
        #    arr = pygame.surfarray.pixels_alpha(death_overlay)
        # But surfarray is not available in pygame-web. Anyway, the code of
        # that pygame.surfarray.pixels_alpha function is only one line, so
        # let's use that instead.
        arr = np.array(death_overlay.get_view("A"), copy=False)

        def circle_gradient_function(x, y):
            return ((x - self.frame.dimensions[0] / 2) ** 4
                    + 6 * (y - self.frame.dimensions[1] / 2) ** 4)
        max_val = circle_gradient_function(0, 0)
        arr[:] = arr * np.fromfunction(
            lambda x, y: 0.6 * circle_gradient_function(x, y) / max_val,
            self.frame.dimensions,
        )

        return death_overlay

    def target_zoom_since(self, timestamp):
        return (1 + math.log(self.game.get_timestamp() - timestamp) / 15)

    def update(self):
        if self.player is None:
            if (p := self.game.players.get(self.player_id)) is not None:
                logger.debug('Set player %s for renderer %s (player_id %s)', p,
                             self, self.player_id)
                self.status_line_drawer.player = self.player = p
            else:
                logger.debug('Waiting forplayer for renderer %s (player_id '
                             '%s)', self, self.player_id)
                return

        self.viewport_center = self.player.center
        self.viewport[:] = (
            self.viewport_center - self.game_frame.dimensions / 2,
            self.viewport_center + self.game_frame.dimensions / 2
        )

        if not self.player.destroyed:
            self.target_zoom = 1 + (
                self.player.acceleration * (ZOOM_TARGET_THRUSTING - 1))
        else:
            self.target_zoom = self.target_zoom_since(
                self.player.death_timestamp)
            self.state_indicator = 'death'

        if self.game.state != GameState.PLAYING:
            self.target_zoom = self.target_zoom_since(
                self.game.end_time)
            self.state_indicator = ('win' if self.player in self.game.winners
                                    else 'lose')
            if self.state_indicator == 'lose' and len(self.game.players) == 1:
                self.state_indicator = 'death'

        if self.zoom_factor != self.target_zoom:
            if abs(self.target_zoom - self.zoom_factor) < ZOOM_CHANGE:
                self.zoom_factor = self.target_zoom
            elif self.zoom_factor < self.target_zoom:
                self.zoom_factor += ZOOM_CHANGE
            else:
                self.zoom_factor -= ZOOM_CHANGE

        self.zoom_matrix[:] = (
            (self.zoom_factor, 0),
            (0, self.zoom_factor),
        )

    def render(self, objects, *, debug=False):
        self.game_frame.fill('black')

        self.status_line_drawer.render()

        self.render_stars()

        for obj in objects:
            if isinstance(obj, Ship):
                self.render_ship(obj)
            elif isinstance(obj, Item):
                self.render_item(obj)
            else:
                self.render_object(obj)

        if self.player is None:
            return

        self.render_object(self.player.respawn_area)

        if self.game.state != GameState.PLAYING:
            self.game_frame.draw_text(
                BUTTON_MARGIN, self.player.statistics_text())

        if debug:
            self.render_debug_info()

        self.game_frame.render()

        self.render_state_indicator()

        self.frame.render()

    def render_debug_info(self):
        text = self.player.statistics_text(debug=True)
        self.game_frame.draw_text(BUTTON_MARGIN, text)
        self.render_object(Area(
            self.game,
            self.game.dimensions / 2,
            self.game.dimensions,
        ))
        ship_number = (len(self.game.players)
                       + len(self.game.bots))
        if ship_number > 1:
            self.render_object(SpaceObject(
                self.game, (0, 0), 0,
                color='green', shape=np.array([
                    s.center for s in
                    self.game.iter_ships()
                ])
            ))

    def render_stars(self):
        wrap_top = self.viewport[0][1] < 0
        wrap_bottom = self.viewport[1][1] > self.game.dimensions[1]
        wrap_left = self.viewport[0][0] < 0
        wrap_right = self.viewport[1][0] > self.game.dimensions[0]
        translations = [np.array((0, 0))]
        if wrap_top:
            translations.append(np.array((0, -self.game.dimensions[1])))
        elif wrap_bottom:
            translations.append(np.array((0, self.game.dimensions[1])))
        if wrap_left:
            translations.append(np.array((-self.game.dimensions[0], 0)))
        elif wrap_right:
            translations.append(np.array((self.game.dimensions[0], 0)))
        # If (wrap_top or wrap_bottom) and (wrap_left or wrap_right), we should
        # also try to apply both
        if len(translations) > 2:
            translations.append(translations[-1] + translations[-2])

        translations = [t - self.viewport_center for t in translations]
        viewport_translation = (self.zoom_factor + self.viewport_center
                                - self.viewport[0])

        for t in translations:
            positions = (
                (self.game.background_stars_pos + t)
                @ self.zoom_matrix + viewport_translation
            )
            for i, pos in enumerate(positions):
                if self.game.background_stars_attrs[i] is None:
                    # In case there was a problem during the stars transmission
                    # in network game, don't crash
                    break
                size, color = self.game.background_stars_attrs[i]
                if (0 < pos[0] < self.game_frame.dimensions[0]
                        and 0 < pos[1] < self.game_frame.dimensions[1]):
                    self.game_frame.draw_circle(pos, size, color)

    def _render_obj_base(self, obj):
        for shape in obj.calc_points(self):
            self.game_frame.draw_polygon(shape, obj.color)

    def render_object(self, obj):
        if obj.destroyed:
            return
        self._render_obj_base(obj)

    def render_ship(self, ship):
        if ship.wait_to_respawn or ship.destroyed:
            return
        self._render_obj_base(ship)

    def render_item(self, item):
        if item.destroyed:
            return
        self._render_obj_base(item)

        viewport_translation = (self.zoom_factor + self.viewport_center
                                - self.viewport[0])
        for translation in get_wrap_translations(
                item.box, self.game, self):
            in_game_translation = (
                item.box[0] + item.icon_offset - self.viewport_center
                + translation)
            for shape, color, fill in item.icon.path:
                self.game_frame.draw_polygon(
                    (shape + in_game_translation)
                    @ self.zoom_matrix
                    + viewport_translation,
                    color, fill=fill)

    def render_state_indicator(self):
        if self.state_indicator is None:
            return
        elif self.state_indicator == 'death':
            size = self.frame.text_size(
                DEATH_FEEDBACK_TEXT, point_size=STATE_FEEDBACK_FONT_SIZE)
            pos = self.frame.dimensions / 2 - (size[0] / 2, size[1] / 2)
            self.frame.draw_text(
                pos, DEATH_FEEDBACK_TEXT, color=DEATH_FEEDBACK_TEXT_COLOR,
                point_size=STATE_FEEDBACK_FONT_SIZE)
            self.frame.draw_frame((0, 0), self.death_overlay)
            return

        if len(self.game.winners) == 0:
            winners_label = 'You all lost, you losers!'
        elif len(self.game.winners) == 1:
            winners_label = 'The winner is:'
        else:
            winners_label = 'The winners are:'
        winners_text = '\n'.join(
            (winners_label, *(p.properties.name for p in self.game.winners)))

        winners_text_size = self.frame.text_size(
            winners_text, point_size=BUTTON_FONT_SIZE)

        if self.state_indicator == 'win':
            main_label = 'You won!'
        else:      # state_indicator == 'lose'
            main_label = 'You lost :('
        main_label_size = self.frame.text_size(
            main_label, point_size=STATE_FEEDBACK_FONT_SIZE)
        main_label_color = 'green' if self.state_indicator == 'win' else 'red'

        total_height = winners_text_size[1] + main_label_size[1]
        main_label_pos = (self.frame.dimensions / 2
                          - (main_label_size[0] / 2, total_height / 2))
        winners_text_pos = (
            self.frame.dimensions[0] / 2 - winners_text_size[0] / 2,
            main_label_pos[1] + main_label_size[1],
        )

        self.frame.draw_text(
            main_label_pos, main_label, color=main_label_color,
            point_size=STATE_FEEDBACK_FONT_SIZE)
        self.frame.draw_text(
            winners_text_pos, winners_text, color='white',
            point_size=BUTTON_FONT_SIZE)

#! /usr/bin/env python3

import asyncio
from dataclasses import dataclass, field
import io
import logging
from pathlib import Path
import re
import sys
import tempfile
import time
import weakref

from .constants import MULTIPLAYER_DEFAULT_PORT
from .logic.data import (Event, EventType, InitPlayerEvent, GameSettingsEvent,
                         GameState, ErrorCode, ErrorEvent,
                         LatencyMeasurementEvent, ShipProperties,
                         StartGameEvent, PlayerNameEvent)
from .settings import ArgparseField, BaseSettings

CHUNK_SIZE = 1024 * 64

if sys.platform == 'emscripten':
    # in pygbag, the platform module allows us to make web requests
    import platform

    import base64

    class WSBridge:
        def __init__(self, url, on_open, on_message, on_error, on_close):
            self.url = url
            self.on_open = on_open
            self.on_message = on_message
            self.on_error = on_error
            self.on_close = on_close

            self.js_ws_bridge = platform.window.connectWebSocket(
                self.url, self.on_open, self.handle_message, self.on_error,
                self.on_close,
            )

        def handle_message(self, event, data):
            self.on_message(event, bytes(data))

        async def send_bytes(self, data):
            self.js_ws_bridge.send_bytes(
                base64.b64encode(data).decode('utf-8'))

        async def close(self):
            self.js_ws_bridge.close()
else:
    # we use aiohttp as a websocket server for multiplayer games. Web browsers
    # don't allow to run websocket servers, so aiohttp is not needed in pygbag.
    import aiohttp
    from aiohttp import web

logger = logging.getLogger(__name__)


class ClientSettings(BaseSettings):
    player_name: str = ArgparseField(
        field(default=''),
        help='Player name shown to the host and the players',
        hide_default=True)
    server_address: str = ArgparseField(
        field(default=''), help='URL of the Back from Altair game you want to '
        'join. Port number, protocol and path can be omitted if using the '
        'default ones.', hide_default=True)


class HostSettings(BaseSettings):
    port_number: int = ArgparseField(
        field(default=MULTIPLAYER_DEFAULT_PORT), help='Port to use to host a '
        'game.')
    join_during_game: bool = ArgparseField(
        field(default=False), help='Wether or not to allow players to join '
        'the game after it has started')
    player_name: str = ArgparseField(
        field(default='host'), help='Player name shown to the other players')


@dataclass
class OnlinePlayer:
    name: str
    player_id: int
    ws: 'web.WebSocketResponse'
    latency: float = -1

    def __post_init__(self):
        self._latest_latency_measurements = []
        self._next_latency_measurement_time = -1
        self._measuring_latency = False

    def add_latency_measurement(self, latency):
        self._latest_latency_measurements.append(latency)
        if len(self._latest_latency_measurements) >= 10:
            self.latency = (sum(self._latest_latency_measurements)
                            / len(self._latest_latency_measurements))
            self._latest_latency_measurements.clear()
        self._next_latency_measurement_time = time.monotonic() * 1000 + 500
        self._measuring_latency = False

    async def ping(self):
        if (not self._measuring_latency
            and self._next_latency_measurement_time < time.monotonic() * 1000):
            self._measuring_latency = True
            await self.ws.send_bytes(LatencyMeasurementEvent(
                server_ts=time.monotonic() * 1000, timestamp=-1).dumps())


class Room:
    def __init__(self, name, game, settings):
        self.name = name
        self.game = game
        self.settings = settings

        self.connected_players: dict[int, OnlinePlayer] = {}
        self.websockets = set()
        self.events_to_send = []

    def join(self, ws, name):
        if len(self.game.players) >= self.game.game_settings.player_number:
            return (ErrorCode.ROOM_FULL, None)

        if (self.game.state != GameState.INITIALIZED
            and not self.settings.host.join_during_game):
            return (ErrorCode.GAME_STARTED, None)

        self.websockets.add(ws)
        player_id = self.game.add_player(ShipProperties(name=name))
        player = OnlinePlayer(name=name, player_id=player_id, ws=ws)
        self.connected_players[player_id] = player
        return (None, player)

    def quit(self, ws: 'web.WebSocketResponse', player: OnlinePlayer):
        self.websockets.remove(ws)
        if player is not None:
            self.game.player_quit(player.player_id)
            del self.connected_players[player.player_id]

    async def send_events(self):
        if len(self.events_to_send) > 0:
            joined_events = b''.join(self.events_to_send)
            self.events_to_send.clear()
            tasks = (ws.send_bytes(joined_events) for ws in self.websockets)
        else:
            tasks = ()
        await asyncio.gather(
            *tasks, *(p.ping() for p in self.connected_players.values()))


class WebIO:
    def __init__(self, tempdir, settings, event_callback=None):
        self.settings = settings

        self.exception = False

        if event_callback is None:
            def event_callback(event):
                pass

        if sys.platform != 'emscripten':
            self.aiohttp_client_session = aiohttp.ClientSession()
            self.ws_server = WSServer(
                event_callback, settings, on_task_done=self.on_task_done)
        else:
            self.aiohttp_client_session = None
            self.ws_server = None

        self.http_client = HTTPClient(
            tempdir, settings, on_task_done=self.on_task_done,
            session=self.aiohttp_client_session)

        self.ws_client = WSClient(
            event_callback, settings, on_task_done=self.on_task_done,
            session=self.aiohttp_client_session)

    def get_request(self, url, file, callback):
        self.http_client.url_queue.put_nowait((url, file, callback))

    def emit_event(self, ev):
        """Add an event to the event queue.
        Called with each event created during the game update."""
        self.ws_client.emit_event(ev)
        if self.ws_server is not None:
            self.ws_server.emit_event(ev)

    async def send_events(self):
        """Send the event queue to the clients.
        Called after every game update."""
        coroutines = [w.send_events() for w in (self.ws_client, self.ws_server)
                      if w is not None and w.running]
        for c in coroutines:
            await c

    async def exit(self):
        tasks = [self.http_client.exit(), self.ws_client.exit()]
        if self.ws_server is not None:
            tasks.append(self.ws_server.exit())
        if self.aiohttp_client_session is not None:
            tasks.append(self.aiohttp_client_session.close())
        await asyncio.gather(*tasks)

    def on_task_done(self, task):
        if task.cancelled():
            return

        if (exc := task.exception()) is None:
            return

        self.exception = True

        raise exc from None


class HTTPClient:
    def __init__(self, tempdir, settings, on_task_done, session):
        self.tempdir = tempdir
        self.settings = settings

        self.url_queue = asyncio.Queue()
        self._coroutine = None

        self.on_task_done = on_task_done
        self.session = session

    async def _web_request(self, url: str, *, use_tempfile=True):
        logger.debug('GET url=%s use_tempfile=%s', url, use_tempfile)
        if not use_tempfile:
            if sys.platform == 'emscripten':
                async with platform.fopen(url, 'r') as f:
                    return f.read()
            else:
                async with self.session.get(url) as resp:
                    return (await resp.read()).decode()
        else:
            suffix = ''.join(Path(url).suffixes)
            fd, file = tempfile.mkstemp(
                prefix='backfromaltair-', suffix=suffix, dir=self.tempdir)
            out_file = io.FileIO(fd, 'wb')
            out_file.name = file
            if sys.platform == 'emscripten':
                async with platform.fopen(url, 'rb') as f:
                    while (chunk := f.read(CHUNK_SIZE)):
                        out_file.write(chunk)
            else:
                async with self.session.get(url) as resp:
                    async for chunk in resp.content.iter_chunked(CHUNK_SIZE):
                        out_file.write(chunk)
            out_file.close()
            return Path(out_file.name)

    def start(self):
        self._coroutine = asyncio.create_task(self.http_worker())
        self._coroutine.add_done_callback(self.on_task_done)

    async def http_worker(self):
        while True:
            url, use_tempfile, callback = await self.url_queue.get()
            content = await self._web_request(
                url=url, use_tempfile=use_tempfile)
            callback(url, content)
            self.url_queue.task_done()

    async def exit(self):
        await self.url_queue.join()
        self._coroutine.cancel()


class WSClient:
    def __init__(self, event_callback, settings, on_task_done, session):
        self.event_callback = event_callback
        self.settings = settings
        self.on_task_done = on_task_done

        self.events_to_send = []
        self.websocket = None
        self._coroutine = None
        self.running = False
        self.session = session
        self.close_websocket = False
        self.latency = -1
        self._latency_measurements = []

    @staticmethod
    def complete_server_address(server_address):
        if not server_address.startswith(('ws://', 'wss://')):
            # Search for : outside of the IPv6 address that could be
            # used (written between [ ] in URLs)
            if ':' not in re.sub(r'\[[^\]]+\]', '', server_address, count=1):
                server_address = re.sub(
                    '(/)|$', fr':{MULTIPLAYER_DEFAULT_PORT}\1', server_address,
                    count=1
                )
            if '/' not in server_address:
                server_address = f'{server_address}/game'
            server_address = f'ws://{server_address}'
        return server_address

    def start(self, ws_url=None):
        if ws_url is None:
            if self.settings.client.server_address is not None:
                ws_url = self.complete_server_address(
                    self.settings.client.server_address)
                if ws_url == '':
                    self.event_callback(ErrorEvent(
                        ErrorCode.SERVER_NOT_SPECIFIED))
                    return
            else:
                self.event_callback(ErrorEvent(ErrorCode.SERVER_NOT_SPECIFIED))
                return
        if not self.settings.client.player_name:
            self.event_callback(ErrorEvent(ErrorCode.UNSET_PLAYER_NAME))
            return
        logger.info('Starting websocket client. Listening at url %s', ws_url)
        self.latency = -1
        self._latency_measurements.clear()
        self._coroutine = asyncio.create_task(self.connect(ws_url))
        self._coroutine.add_done_callback(self.on_task_done)

    async def connect(self, ws_url):
        self.close_websocket = False
        if sys.platform != 'emscripten':
            await self._connect_aiohttp(ws_url)
        else:
            await self._connect_web_interface(ws_url)

    def handle_handshake(self):
        logger.info('WebSocket connection succeeded')
        self.running = True
        self.emit_event(PlayerNameEvent(name=self.settings.client.player_name))

    def handle_binary_message(self, data):
        for ev in Event.iter_unpack(data):
            logger.debug('Received event: %s', ev)
            if ev.event_type == EventType.LATENCY_MEASUREMENT:
                if ev.server_ts < 0:
                    logger.error("server_ts can't be unset in "
                                 "WSClient.handle_binary_message")
                elif ev.client_ts < 0:
                    ev.client_ts = time.monotonic() * 1000
                    self.emit_event(ev)
                elif ev.server_ack_ts < 0:
                    logger.error("server_ack_ts can't be unset in "
                                 "WSClient.handle_binary_message")
                elif ev.client_ack_ts < 0:
                    ev.client_ack_ts = time.monotonic() * 1000
                    self.emit_event(ev)
                    self._latency_measurements.append(
                        (ev.server_ack_ts - ev.server_ts)
                        - (ev.client_ack_ts - ev.client_ts) / 2
                    )
                    if len(self._latency_measurements) >= 10:
                        self.latency = (sum(self._latency_measurements)
                                        / len(self._latency_measurements))
                        self._latency_measurements.clear()
                continue
            self.event_callback(ev)
            if (ev.event_type == EventType.ERROR
                and ev.error in (ErrorCode.ROOM_FULL, ErrorCode.GAME_STARTED)):
                # Don't show a network error in addition of the ROOM_FULL error
                self.close_websocket = True

    def handle_error(self):
        self.event_callback(ErrorEvent(ErrorCode.CONNECTION_CLOSED))

    def handle_close(self):
        if not self.close_websocket:
            self.event_callback(ErrorEvent(ErrorCode.CONNECTION_CLOSED))

    async def _connect_aiohttp(self, ws_url):
        try:
            async with self.session.ws_connect(ws_url) as websocket:
                self.websocket = websocket
                self.handle_handshake()
                async for msg in websocket:
                    if msg.type == aiohttp.WSMsgType.BINARY:
                        self.handle_binary_message(msg.data)
                    elif msg.type == aiohttp.WSMsgType.ERROR:
                        logger.info('Connection closed with code %s', msg.data)
                        self.handle_error()
                    elif msg.type in (aiohttp.WSMsgType.CLOSED,
                                      aiohttp.WSMsgType.CLOSE):
                        logger.info('Connection closed with code %s', msg.data)
                    else:
                        logger.debug("Client received WS message: %s", msg)
        except aiohttp.client_exceptions.InvalidURL:
            logger.exception("An invalid URL was provided")
            self.event_callback(ErrorEvent(ErrorCode.SERVER_NOT_SPECIFIED))
            return
        except (aiohttp.client_exceptions.ClientConnectorError,
                aiohttp.client_exceptions.WSServerHandshakeError):
            logger.exception(
                "Couldn't connect to the server through WebSocket")
            self.event_callback(ErrorEvent(ErrorCode.CLIENT_CONNECTION_FAILED))
            return
        else:
            self.handle_close()

    async def _connect_web_interface(self, ws_url):
        def on_message(event, data):
            self.handle_binary_message(data)

        self.websocket = WSBridge(
            ws_url, self.handle_handshake, on_message, self.handle_error,
            self.handle_close,
        )

    def event_to_bytes(self, ev: Event):
        if ev.timestamp is None:
            # This event wasn't sent from Game.emit_event. We need a
            # timestamp, but it isn't important. Let's put -1.
            ev.timestamp = -1
        return ev.dumps()

    def emit_event(self, ev):
        if self.running:
            self.events_to_send.append(self.event_to_bytes(ev))

    async def send_events(self):
        if (not self.running or len(self.events_to_send) == 0
            or self.close_websocket):
            return
        joined_events = b''.join(self.events_to_send)
        self.events_to_send.clear()
        await self.websocket.send_bytes(joined_events)

    async def exit(self):
        logger.debug('cancelling client websocket')
        self.close_websocket = True
        if self.websocket is not None:
            await self.websocket.close()
        if self._coroutine is not None:
            self._coroutine.cancel()
        self.running = False


class WSServer:
    def __init__(self, event_callback, settings, on_task_done):
        self.event_callback = event_callback
        self.settings = settings
        self.on_task_done = on_task_done

        self.running = False
        self._coroutine = None
        self.app = None
        self.ws_server_site = None
        self.ws_key = None
        self.runner = None
        self.room = None

    async def handshake(self, ws, room):
        player = None

        msg = await ws.receive()
        if msg.type != aiohttp.WSMsgType.BINARY:
            logger.info('websocket connection was closed because of an '
                        'invalid initialization sequence (msg=%s)', msg)
            await ws.close()
            return None
        events = Event.iter_unpack(msg.data)
        for ev in events:
            if ev.event_type == EventType.PLAYER_NAME:
                if player is not None:
                    logger.info(
                        "websocket connection was closed because of an "
                        "invalid initialization sequence (resetting the "
                        "player's name) (ev=%s)", ev
                    )
                    await ws.close()
                    return None
                error, player = room.join(ws, ev.name)
                if error is not None:
                    logger.info('websocket connection was closed because '
                                'of error %s', error)
                    await ws.send_bytes(self.event_to_bytes(
                        ErrorEvent(error=error)))
                    await ws.close()
                    return None

            else:
                logger.info(
                    'websocket connection was closed because of an '
                    'invalid event in the initialization sequence (ev=%s)',
                    ev
                )
                await ws.close()
                return None

        events = [
            InitPlayerEvent(player_id=player.player_id),
            GameSettingsEvent(
                dimensions=room.game.game_settings.dimensions,
                star_density=room.game.game_settings.star_density,
                player_number=room.game.game_settings.player_number,
                bot_number=room.game.game_settings.bot_number,
                game_mode=room.game.game_settings.game_mode,
            )
        ]

        if self.room.game.state != GameState.INITIALIZED:
            # We need to send events to create the objects in the game and to
            # tell the client to start the game.
            events.extend(self.room.game.create_object_event(obj)
                          for obj in self.room.game.iter_objects()
                          if obj.id != player.player_id)
            events.extend(self.room.game.create_stars_event())
            events.append(StartGameEvent(
                timestamp=self.room.game.get_timestamp()))

        await ws.send_bytes(b''.join(self.event_to_bytes(ev) for ev in events))

        return player

    async def handle_event(self, event, player):
        logger.debug('received event: %s', event)
        if event.event_type == EventType.CHAT_MESSAGE:
            if event.player_id != player.player_id:
                logger.debug(
                    'Received invalid event: event.player_id=%d '
                    'while player id is: %d', event.player_id,
                    player.player_id)
                return
            self.event_callback(event)
            self.emit_event(event)
        elif event.event_type == EventType.LATENCY_MEASUREMENT:
            if event.client_ts < 0:
                logger.error("client_ts can't be unset in "
                             "WSServer.handle_event")
            elif event.server_ack_ts < 0:
                event.server_ack_ts = time.monotonic() * 1000
                await player.ws.send_bytes(self.event_to_bytes(event))
            elif event.client_ack_ts < 0:
                logger.error("client_ack_ts can't be unset in "
                             "WSServer.handle_event")
            else:
                player.add_latency_measurement(
                    (event.server_ack_ts - event.server_ts)
                    - (event.client_ack_ts - event.client_ts) / 2
                )
            return
        elif event.event_type in (EventType.CHANGE_MOVEMENT, EventType.TURN,
                                  EventType.FIRE):
            if event.object_id != player.player_id:
                # This client is not allowed to emit this event
                logger.debug(
                    'Received invalid event: event.object_id=%d '
                    'while player id is: %d', event.object_id,
                    player.player_id)
                return
            logger.debug(
                'Received valid event (event.object_id=%d)',
                event.object_id
            )
            self.emit_event(event)
            self.event_callback(event)
        else:
            logger.info('Unexpected event type received: %s',
                        event.event_type)

    async def _handle_websocket(self, request):
        """Handle one client."""

        # TODO: handle multiple rooms?
        room = self.room

        ws = web.WebSocketResponse()
        await ws.prepare(request)

        logger.info('new websocket client connecting')

        player = None
        request.app[self.ws_key].add(ws)
        try:
            player = await self.handshake(ws, room)
            if player is None:
                await ws.close()
                return ws

            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.BINARY:
                    for ev in Event.iter_unpack(msg.data):
                        await self.handle_event(ev, player)
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    logger.error('ws connection was closed with exception %s',
                                 ws.exception())
        finally:
            if self.running and player is not None:
                room.quit(ws, player)
            # Else, if not self.running, we're shutting down, no need to make
            # players quit gracefully.
            request.app[self.ws_key].discard(ws)

        logger.info('websocket connection was closed')

        return ws

    def event_to_bytes(self, ev: Event):
        if ev.timestamp is None:
            # This event wasn't sent from Game.emit_event. We need a
            # timestamp, but it isn't important. Let's put -1.
            ev.timestamp = -1
        return ev.dumps()

    def emit_event(self, ev: Event):
        if self.running:
            if ev.event_type == EventType.START_GAME:
                logger.debug('Will send START_GAME event: %s', ev)
            self.room.events_to_send.append(self.event_to_bytes(ev))

    async def send_events(self):
        if self.running:
            await self.room.send_events()

    async def run_app(self, app, bind_host=None, bind_port=None):
        """Asynchronous equivalent of aiohttp.web.run_app
        See
        https://github.com/messa/tips/blob/master/Python%20-%20aiohttp%20server.md#how-to-start-server-from-async-code
        """  # NOQA: E501, RUF100
        self.runner = web.AppRunner(app)
        await self.runner.setup()
        self.ws_server_site = web.TCPSite(self.runner, bind_host, bind_port)
        await self.ws_server_site.start()
        while self.running:
            # https://github.com/aio-libs/aiohttp/blob/master/aiohttp/web.py#L347-L348 :)  # NOQA: E501
            # We sleep only for 5 seconds to be able to stop server quickly
            # enough
            await asyncio.sleep(5)
        await self.runner.cleanup()

    def start(self, game=None):
        self._coroutine = asyncio.create_task(self.server_worker(game))
        self._coroutine.add_done_callback(self.on_task_done)

    async def server_worker(self, game=None):
        logger.info("Starting WebSocket server...")

        self.app = web.Application()
        self.app.add_routes([
            web.get('/game', self._handle_websocket, name='game')
        ])

        self.ws_key = web.AppKey("websockets", weakref.WeakSet)
        self.app[self.ws_key] = weakref.WeakSet()

        self.room = Room('game', game, self.settings)

        if not self.settings.server_mode:
            game.player_id = game.add_player(
                ShipProperties(name=self.settings.host.player_name))

        self.running = True

        await self.run_app(self.app, bind_port=self.settings.host.port_number)

    def get_url(self):
        if self.running and len(self.runner.addresses) > 0:
            path = self.app.router['game'].url_for()
            addr = self.runner.addresses[0]
            host = f'{addr[0]}:{addr[1]}'
            return f'ws://{host}{path}'
        return None

    async def exit(self):
        if not self.running:
            return
        self.running = False
        await self.ws_server_site.stop()
        self._coroutine.cancel()
        try:
            await self._coroutine
        except asyncio.CancelledError:
            pass
        logger.info('WS server terminated')
        self._coroutine = None

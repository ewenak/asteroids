#! /usr/bin/env python3

import logging
import sys

if sys.platform != 'emscripten':
    import logging.config
    import os


class ColorFormatter(logging.Formatter):
    def __init__(self, fmt=None, datefmt=None, style='%',
                 validate=True, *, defaults=None):        # NOQA: FBT002
        super().__init__(fmt=fmt, datefmt=datefmt, style=style,
                         validate=validate, defaults=defaults)

    def formatMessage(self, record):
        level_ansi_color = None
        if record.levelno <= logging.DEBUG:
            # FG: green
            level_ansi_color = '\033[32m'
        elif record.levelno <= logging.INFO:
            # FG: blue
            level_ansi_color = '\033[34m'
        elif record.levelno <= logging.WARNING:
            # FG: yellow
            level_ansi_color = '\033[33m'
        elif record.levelno <= logging.ERROR:
            # FG: red
            level_ansi_color = '\033[31m'
        else:                # logging.CRITICAL
            # FG: white; BG: red
            level_ansi_color = '\033[37;41m'

        color_number = 31 + sum(record.name.encode('utf-8')) % 6
        message_ansi_color = f'\033[{color_number}m'

        original_levelname = record.levelname
        original_message = record.message

        record.levelname = f'{level_ansi_color}{original_levelname}\033[0m'
        record.message = f'{message_ansi_color}{original_message}\033[0m'

        message = super().formatMessage(record)

        record.levelname = original_levelname
        record.message = original_message

        return message


def setup_logging(app_dirs, debugged_modules=None):
    if sys.platform == 'emscripten':
        import platform
        if 'logging' not in platform.window.location.hash:
            return
        logging.basicConfig(level=logging.INFO)
        return

    if debugged_modules is None:
        debugged_modules = os.environ.get('BFA_DEBUG', '').split(':')

    logdir = app_dirs.user_log_path
    logdir.mkdir(parents=True, exist_ok=True)

    logging_config = {
        'version': 1,
        'formatters': {
            'precise': {
                'format': "%(levelname)s:%(asctime)s:%(name)s: %(message)s",
                'class': 'logging.Formatter',
            },
            'brief': {
                'format': "%(levelname)5s:%(asctime)s:%(name)s: %(message)s",
                'datefmt': '%H:%M:%S,%03d',
                'class': 'backfromaltair.logging.ColorFormatter',
            },
        },
        'handlers': {
            'file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'precise',
                'filename': logdir / 'backfromaltair.log',
                'maxBytes': 1024 * 1024,
                'backupCount': 5,
            },
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'brief',
            },
        },
        'loggers': {},
        'root': {
            'level': 'INFO',
            'handlers': ['console', 'file'],
        },
        'disable_existing_loggers': False,
    }

    for m in debugged_modules:
        logging_config['loggers'][m] = {'level': 'DEBUG'}

    logging.config.dictConfig(logging_config)

#! /usr/bin/env python3

import argparse
import dataclasses
import enum
import functools
import json
import logging
import os
from pathlib import Path
import re
import sys
import types
import typing

import numpy as np

if sys.platform == 'emscripten':
    import platform

logger = logging.getLogger(__name__)


def pascal_case_to_spaces(string):
    return re.sub('(.)([A-Z]+)', r'\1 \2', string).lower()


@dataclasses.dataclass
class ArgparseField:
    field: dataclasses.Field = dataclasses.field(
        default_factory=dataclasses.field)
    help: str = None       # NOQA: A003
    short_option: str = None
    type: type = None      # NOQA: A003
    action: argparse.Action = None
    choices: list[str] | None = None
    hide_default: bool = False
    hidden_setting: bool = False
    unsaved_setting: bool = False
    args: list | None = None


def serialize_value(value):
    if isinstance(value, np.ndarray):
        return str(value.tolist())
    elif value is None:
        return None
    elif isinstance(value, enum.Enum):
        return value.name
    elif isinstance(value, (type, types.FunctionType)):
        return value.__name__
    return str(value)


def parse_value(type_, value):
    if type_ == bool:
        value = value.strip().lower()
        if value in ('true', 't', '1', 'yes', 'y', 'on'):
            return True
        elif value in ('false', 'f', '0', 'no', 'n', 'off', ''):
            return False
        else:
            raise ValueError(f"invalid truth value: {value}")

    return type_(value)


def vector2(s):
    """Vector2 type, to be user as an argparse type"""
    # We may raise ValueError s, which will be catched by argparse and will
    # stop the program.
    val = [float(v) for v in re.sub(r'[^0-9.]', ' ', s).strip(' ').split(' ')]
    if len(val) != 2:
        raise ValueError('please enter two numbers')
    return np.array(val)


def get_default(field):
    if field.default == dataclasses.MISSING:
        if field.default_factory == dataclasses.MISSING:
            return None
        return field.default_factory()
    else:
        return field.default


def format_default_value(field):
    default = get_default(field)
    if isinstance(default, str):
        return repr(default)
    return serialize_value(default)


def convert_to_enum(s, enumclass):
    val = getattr(enumclass, s, None)
    if val is None:
        raise ValueError('invalid enum value')
    return val


class SettingsType(type):
    def __new__(cls, name, bases, dct):
        settings_class = super().__new__(cls, name, bases, dct)
        settings_annotations = settings_class.__dict__.get(
            '__annotations__', {})
        settings_fields = {}
        for name, type_ in settings_annotations.items():
            val = getattr(settings_class, name)
            if name == 'filename':
                raise NameError(
                    f'{settings_class}: "filename" is a reserved name that '
                    f'cannot be used as a field')
            if isinstance(val, ArgparseField):
                if val.type is None:
                    val.type = type_
                settings_fields[name] = val
                setattr(settings_class, name, val.field)
            elif isinstance(val, dataclasses.Field):
                settings_fields[name] = ArgparseField(val, type=type_)
            elif type_ == typing.ClassVar:
                continue
            else:
                settings_fields[name] = ArgparseField(
                    dataclasses.field(default=val), type=type_)

        settings_class.filename = None
        settings_class.__annotations__['filename'] = dataclasses.InitVar[
            str | bytes | os.PathLike | None]

        human_readable_name = settings_class.__name__
        if human_readable_name.endswith('Settings'):
            human_readable_name = human_readable_name[:-len('Settings')]
        human_readable_name = pascal_case_to_spaces(human_readable_name)

        description = getattr(settings_class, 'description', None)

        prefix = getattr(settings_class, 'prefix', None)
        if prefix is None:
            prefix = f"{human_readable_name.replace(' ', '-')}-"

        subsettings = []
        attributes_mapping = {}

        argument_parser = argparse.ArgumentParser(
            'backfromaltair', description=description, parents=[
                f.type.argument_parser for f in settings_fields.values()
                if isinstance(f.type, cls)
            ], add_help=prefix == '',
        )
        if prefix != '':
            argument_group = argument_parser.add_argument_group(
                f'{human_readable_name.capitalize()} options', description)
        else:
            argument_group = argument_parser

        for name, f in settings_fields.items():
            help_text = []
            if f.help:
                help_text.append(f.help)
            if ((default := format_default_value(f.field)) is not None
                    and not f.hide_default):
                help_text.append(f'(default: {default})')

            if isinstance(f.type, enum.EnumType):
                if not f.choices:
                    f.choices = list(f.type)
                converter = functools.partial(
                    convert_to_enum, enumclass=f.type)
                converter.__name__ = f.type.__name__
                f.type = converter
                help_text.append(f'(possible values: '
                                 f'{", ".join(v.name for v in f.choices)})')

            f.help = ' '.join(help_text)

            if f.args is None:
                f.args = [f'--{prefix}{name.replace("_", "-")}']

            kwargs = {
                'help': f.help,
                'dest': f'{settings_class.__name__}__{name}',
                'metavar': f'{name.upper()}',
                'choices': f.choices,
            }
            if f.short_option is not None:
                f.args.append(f'-{f.short_option}')
            if f.hidden_setting:
                continue
            elif f.action is not None:
                kwargs['action'] = f.action
            elif f.type == str:
                pass
            elif f.type == bool:
                kwargs['action'] = argparse.BooleanOptionalAction
            elif isinstance(f.type, cls):
                subsettings.append(name)
                continue
            else:
                kwargs['type'] = f.type
            attributes_mapping[kwargs['dest']] = name
            argument_group.add_argument(*f.args, **kwargs)

        settings_class.__annotations__['argument_parser'] = typing.ClassVar[
            argparse.ArgumentParser]
        settings_class.argument_parser = argument_parser
        settings_class.__annotations__['attributes_mapping'] = typing.ClassVar[
            dict[str, str]]
        settings_class.attributes_mapping = attributes_mapping
        settings_class.__annotations__['settings_fields'] = typing.ClassVar[
            dict[str, ArgparseField]]
        settings_class.settings_fields = settings_fields
        settings_class.__annotations__['subsettings'] = typing.ClassVar[
            list[str]]
        settings_class.subsettings = subsettings

        return dataclasses.dataclass(settings_class)


class BaseSettings(metaclass=SettingsType):
    def update_from_namespace(self, namespace):
        for dest, attr in self.attributes_mapping.items():
            if (value := getattr(namespace, dest)) is not None:
                setattr(self, attr, value)
        for s in self.subsettings:
            getattr(self, s).update_from_namespace(namespace)

    def update_from_dict(self, dictionary, *, flat=False):
        """Update settings from values read from dictionary.
        If flat is false, the dictionary must represent the settings structure,
        with dictionaries as the values for subsettings.
        If flat is true, the dictionary must only contain pairs of string, with
        the keys in the format {cls.__name__}__{name}, like for namespaces.
        """
        for name, f in self.settings_fields.items():
            if f.unsaved_setting:
                continue
            elif isinstance(f.type, SettingsType):
                if not flat:
                    getattr(self, name).update_from_dict(
                        dictionary.get(name, {}), flat=False)
                else:
                    getattr(self, name).update_from_dict(dictionary, flat=True)
                continue
            key = name if not flat else f'{self.__class__.__name__}__{name}'
            if (val := dictionary.get(key, Ellipsis)) is not Ellipsis:
                try:
                    setattr(self, name, parse_value(f.type, val))
                except (argparse.ArgumentTypeError, ValueError, TypeError):
                    # These are the exceptions supported by argparse type
                    # argument. Let's not crash on error.
                    pass

    def to_dict(self):
        settings = {}
        for name, f in self.settings_fields.items():
            if f.unsaved_setting:
                continue
            elif isinstance(f.type, SettingsType):
                settings[name] = getattr(self, name).to_dict()
                continue
            settings[name] = serialize_value(getattr(self, name))
        return settings

    def parse_command_line_args(self, args):
        namespace = self.argument_parser.parse_args(args)
        self.update_from_namespace(namespace)

    def parse_json(self, json_data):
        try:
            settings = json.loads(json_data)
        except json.JSONDecodeError:
            return
        self.update_from_dict(settings)

    def write_json(self):
        settings = self.to_dict()
        return json.dumps(settings)

    def parse_json_file(self, filename):
        with open(filename) as file:
            self.parse_json(file.read())

    def write_json_file(self, filename):
        settings = self.to_dict()
        with open(filename, 'w') as file:
            json.dump(settings, file, indent=4)

    def save(self):
        if sys.platform == 'emscripten':
            platform.window.localStorage.setItem('settings', self.write_json())
            logger.info('Saved settings')
        elif self.filename is not None:
            self.write_json_file(self.filename)
            logger.info('Saved settings')

    def setter(self, param):
        field = self.settings_fields[param]
        if field.type == bool:
            def setval(value=None):
                if value is None:
                    setattr(self, param, not getattr(self, param))
                else:
                    setattr(self, param, value)
        elif field.type == str:
            def setval(value):
                setattr(self, param, value)
        else:
            def setval(value):
                if isinstance(value, str):
                    setattr(self, param, field.type(value))
                else:
                    setattr(self, param, value)
        setval.default = getattr(self, param)
        return setval

    def __post_init__(self, filename=None):
        if filename is not None:
            self.filename = Path(filename)
            if self.filename.exists():
                self.parse_json_file(filename)
            elif not self.filename.parent.exists():
                self.filename.parent.mkdir(parents=True)
        else:
            self.filename = None

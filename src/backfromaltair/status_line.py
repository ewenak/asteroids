#! /usr/bin/env python3

import logging
import math

import numpy as np
import pygame

from .constants import (BUTTON_MARGIN, PLAYER_RADIUS, RECENT_MESSAGES_BOTTOM,
                        RECENT_MESSAGES_FADE_OUT_AGE, RECENT_MESSAGES_MAX_AGE,
                        RECENT_MESSAGES_MAX_NUMBER, STATUS_LINE_BG_COLOR,
                        STATUS_LINE_HEIGHT, STATUS_LINE_MARGIN,
                        STATUS_LINE_SEP_COLOR)
from .frames import ScrollableFrame, container_x
from .icons import BulletIcon, TankIcon
from .logic.data import ChatMessageEvent, EventType
from .logic.ships import Ship
from .widgets import Button, CompoundItem, TextEntry

logger = logging.getLogger(__name__)


class StatusLineDrawer:
    def __init__(self, frame, player, renderer):
        self.frame = frame
        self.player = player
        self.renderer = renderer
        self.tank_icon = TankIcon(STATUS_LINE_HEIGHT - STATUS_LINE_MARGIN * 2)
        self.bullet_icon = BulletIcon(
            STATUS_LINE_HEIGHT - STATUS_LINE_MARGIN * 2)

        self.ship_radius = PLAYER_RADIUS
        self.ship_shape = Ship.calc_shape(
            None, math.pi / 2, np.array((self.ship_radius, self.ship_radius)),
            radius=self.ship_radius)

    def draw_lives(self):
        x = self.frame.dimensions[0] - self.ship_radius * 1.8
        y = STATUS_LINE_MARGIN
        for _ in range(self.player.lives):
            move = np.array((x, y))
            points = [p + move for p in self.ship_shape]
            self.frame.draw_polygon(points, "white")
            x -= self.ship_radius * 1.5

    def draw_tank(self, topleft: np.array) -> np.array:
        self.tank_icon.fraction_filled = (
            self.player.tank_filling / self.player.properties.full_tank)
        self.tank_icon.generate_path()
        self.tank_icon.draw_to(topleft, self.frame)
        text_pos = topleft + np.array(
            (self.tank_icon.width + STATUS_LINE_MARGIN, 0)
        )
        text_size = self.frame.draw_text(
            text_pos, f'{int(self.tank_icon.fraction_filled * 100)}%')

        return np.array((text_pos[0] + text_size[0] + STATUS_LINE_MARGIN * 2,
                         topleft[1]))

    def draw_bullets(self, topleft: np.array) -> np.array:
        for i in range(2):
            translation = np.array((
                i * (self.bullet_icon.width + STATUS_LINE_MARGIN), 0))
            self.bullet_icon.draw_to(topleft + translation, self.frame)

        text_pos = np.array((
            topleft[0] + 2 * (self.bullet_icon.width + STATUS_LINE_MARGIN),
            topleft[1]))
        text_size = self.frame.draw_text(
            text_pos, f'{self.player.bullets_number}')

        return np.array((text_pos[0] + text_size[0] + STATUS_LINE_MARGIN * 2,
                         topleft[1]))

    def render(self):
        self.frame.fill(STATUS_LINE_BG_COLOR)
        self.frame.draw_polygon(
            (
                (0, self.frame.dimensions[1] - 1),
                self.frame.dimensions - (1, 1),
            ),
            STATUS_LINE_SEP_COLOR,
        )

        if self.player is None:
            return

        topleft = self.draw_tank(
            np.array((STATUS_LINE_MARGIN, STATUS_LINE_MARGIN)))
        topleft = self.draw_bullets(topleft)

        self.draw_lives()

        self.frame.render()


class Chat:
    def __init__(self, application, interface, frame, shell, players,
                 player_id):
        logger.debug('creating Chat represenation')

        self.application = application
        self.interface = interface
        self.frame = frame
        self.shell = shell
        self.players = players
        if player_id is not None:
            self.player_id = player_id
        else:
            self.player_id = next(iter(self.players.values())).id

        self.messages = []
        self._drawing_pos = np.array((0, 0))
        self.collapsed = True
        self._message_height = self.frame.get_linesize() + BUTTON_MARGIN[1]

        self.text_entry = TextEntry("Message", callback=self.send_message,
                                    max_length=255, validate_on_blur=False)
        self.send_button = Button("Send", callback=self.send_message)
        self.widget = CompoundItem(
            self.text_entry, self.send_button,
            interface=self.interface, frame=self.frame,
            width=self.frame.dimensions[0],
        )
        self.text_entry.width = (self.frame.dimensions[0]
                                 - self.send_button.width)
        self.widget.set_pos((0, self.frame.dimensions[1] - self.widget.height))
        self.messages_frame = ScrollableFrame(
            self.frame,
            pos=(0, 0),
            real_dimensions=(container_x, lambda h: h - self.widget.height),
            virtual_dimensions=(0, 0),
        )

        self.application.add_event_handler(self.receive_event)

    def receive_event(self, event):
        if event.event_type == EventType.CHAT_MESSAGE:
            self.receive_message(event)

    def on_scroll(self, movement, pos):
        if pos[1] < self.messages_frame._real_dimensions[1]:
            self.messages_frame.scroll(movement)

    def on_click(self, pos):
        logger.debug("Clicked chat at %s", pos)
        if self.widget.contains_point(pos):
            logger.debug("Clicked chat in widget")
            self.widget.activate_click(pos)

    def receive_message(self, message):
        logger.debug("Received message: %s", message)
        self.messages.append(message)
        width = container_x
        height = self.messages_frame.dimensions[1] + self._message_height
        self.messages_frame.set_virtual_dimensions((width, height))

    def update(self, dt):
        if not self.collapsed:
            self.widget.update(dt)

    def render(self):
        if self.collapsed:
            self.render_recent_messages()
            return
        self.frame.fill('black')
        self.render_messages()
        self.widget.render()
        self.frame.render()
        self.frame.render_outline()

    def send_message(self, message=None):
        if message is None:
            logger.debug('obtaining message from text_entry')
            message = self.text_entry.value

        logger.debug('sending message: %s', repr(message))
        self.application.emit_event(
            ChatMessageEvent(self.player_id, message,
                             timestamp=self.application.game.get_timestamp())
        )
        self.text_entry.value = ''

    def render_message(self, message: ChatMessageEvent, frame=None,
                       color='white'):
        if frame is None:
            frame = self.messages_frame

        player_name = self.players[message.player_id].properties.name
        text = f"{player_name}: {message.message}"
        frame.draw_text(
            self._drawing_pos, text, color=color,
            max_width=self.frame.dimensions[0]
        )
        self._drawing_pos[1] += self._message_height

    def render_messages(self):
        self._drawing_pos[:] = BUTTON_MARGIN
        self.messages_frame.fill('black')

        for message in self.messages:
            self.render_message(message)
        self.messages_frame.render()

    def render_recent_messages(self):
        beginning_index = len(self.messages)
        min_timestamp = (
            self.application.game.get_timestamp() - RECENT_MESSAGES_MAX_AGE)
        fadeout_timestamp = min_timestamp + RECENT_MESSAGES_FADE_OUT_AGE
        message_number = 0

        while (
            message_number + 1 <= RECENT_MESSAGES_MAX_NUMBER
            and beginning_index - 1 >= 0
            and self.messages[beginning_index - 1].timestamp > min_timestamp
        ):
            beginning_index -= 1
            message_number += 1

        self._drawing_pos[:] = (
            0,
            RECENT_MESSAGES_BOTTOM - message_number * self._message_height,
        )

        color = pygame.Color('white')
        for m in self.messages[beginning_index:]:
            if m.timestamp < fadeout_timestamp:
                color.a = 255 - int(
                    255 * (fadeout_timestamp - m.timestamp)
                    / RECENT_MESSAGES_FADE_OUT_AGE
                )
            else:
                color.a = 255
            self.render_message(m, frame=self.interface.main_frame,
                                color=color)

#! /usr/bin/env python3

import logging
import time

import numpy as np
import pygame

from .constants import (BUTTON_BORDER_COLOR, BUTTON_MARGIN, MENU_BG_COLOR,
                        MENU_FG_COLOR, MENU_TOP_MARGIN,
                        NUMBER_ENTRY_DEFAULT_DIGIT_NUMBER,
                        NOTIFICATION_DURATION, NOTIFICATION_FADE_OUT_DURATION)
from .frames import container_dims, container_x, ScrollableFrame

logger = logging.getLogger(__name__)


def setup_items_linked_list(items: list):
    logger.debug('setting up items linked list.')
    last_button: Button = items[-1]
    for item in items:
        if item.interactive:
            last_button.next_interactive_item = item
            item.previous_interactive_item = last_button
            last_button = item


class Notification:
    def __init__(self, frame, width, message, debug_info=''):
        self.timestamp = time.monotonic() * 1000
        self.fadeout_end = self.timestamp + NOTIFICATION_DURATION
        self.fadeout_start = self.fadeout_end - NOTIFICATION_FADE_OUT_DURATION
        self.frame = frame
        self.width = width
        self.text_width = self.width - 2 * BUTTON_MARGIN[0]

        self.message = message
        self.message_size = self.frame.text_size(
            self.message, max_width=self.text_width)

        self.debug_info = debug_info
        self.debug_info_size = self.frame.text_size(
            self.debug_info, max_width=self.text_width)

    def render(self, bottom, color=None, *, debug=False):
        if color is None:
            if self.fadeout_start > time.monotonic() * 1000:
                color = BUTTON_BORDER_COLOR
            elif time.monotonic() * 1000 > self.fadeout_end:
                color = 'black'
            else:
                color = pygame.Color(BUTTON_BORDER_COLOR).lerp(
                    'black',
                    1 - ((self.fadeout_end - time.monotonic() * 1000)
                         / NOTIFICATION_FADE_OUT_DURATION)
                )
        height = self.message_size[1] + 2 * BUTTON_MARGIN[1]
        if debug:
            height += self.debug_info_size[1] + BUTTON_MARGIN[1]

        pos = (self.frame.dimensions[0] / 2 - self.width / 2,
               bottom - height)
        self.frame.draw_rect(
            pos, (pos[0] + self.width, pos[1] + height), color)
        self.frame.draw_text(
            pos + BUTTON_MARGIN, self.message, color=color,
            max_width=self.text_width)
        if debug:
            self.frame.draw_text(
                BUTTON_MARGIN + (
                    pos[0], pos[1] + BUTTON_MARGIN[1] + self.message_size[1]),
                self.debug_info, color=color, max_width=self.text_width)

        return pos


class MenuItem:
    interactive = False

    def __init__(self, text, *, interface=None, frame=None, pos=None,
                 center_text=True, width=None, height=None,
                 bg_color=MENU_BG_COLOR, fg_color=MENU_FG_COLOR,
                 border_color=None, highlightable=False):
        self._set_pos_after_dimensions = None

        self.interface = interface
        self.frame = frame
        self.metrics = np.zeros((2,))

        # If text is a callable, it will be updated on each activation of the
        # button
        if callable(text):
            self.text_func = text
            self.text = self.text_func()
        else:
            self.text_func = None
            self.text = text

        self.width = width
        self.height = height
        self.center_text = center_text

        lazy_attrs = []

        if interface is not None:
            self.set_interface(interface)
        else:
            lazy_attrs.append('interface')

        if frame is not None:
            self.set_frame(frame)
        else:
            lazy_attrs.append('frame')

        if pos is not None:
            self.set_pos(pos)
        else:
            lazy_attrs.append('pos')

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug('%s: lazily set attributes: %s', self.text,
                         ', '.join(lazy_attrs))

        self.bg_color = bg_color
        self.fg_color = fg_color
        self.border_color = border_color
        self.highlightable = highlightable

        self.highlighted = False

    def set_interface(self, interface):
        self.interface = interface

    def set_frame(self, frame):
        self.frame = frame

        self.metrics[:] = np.array(self.frame.text_size(self.text))
        self.width = self.width or (self.metrics[0] + BUTTON_MARGIN[0] * 2)
        self.height = self.height or (self.metrics[1] + BUTTON_MARGIN[1] * 2)

        if self._set_pos_after_dimensions is not None:
            self.set_pos(self._set_pos_after_dimensions)
            self._set_pos_after_dimensions = None

    def set_pos(self, pos):
        if self.width is None or self.height is None:
            logger.debug(
                "Deferring set_pos because the button's dimensions are still "
                "unknown"
            )
            self._set_pos_after_dimensions = pos
            return

        self.topleft = np.array(pos)
        self.bottomright = self.topleft + (self.width, self.height)
        if not self.center_text:
            self.text_pos = self.topleft + BUTTON_MARGIN
        else:
            self.text_pos = self.topleft + np.array((
                self.width / 2 - self.metrics[0] / 2, BUTTON_MARGIN[1]))

    def render(self):
        if self.frame is None:
            raise RuntimeError('trying to render a MenuItem without set frame')
        if self.topleft is None:
            raise RuntimeError('trying to render a MenuItem without set pos')

        if not self.highlightable or not self.highlighted:
            fg_color = self.fg_color
        else:
            fg_color = pygame.Color(0, 0, 0)
            fg_color.hsla = time.monotonic() * 100 % 360, 100, 75, 100

        if self.border_color is not None:
            # Draw border
            self.frame.draw_rect(
                self.topleft, self.bottomright, fg_color, self.bg_color)

        # Draw text
        if self.center_text:
            self.frame.draw_text(
                self.topleft + (np.array((self.width, self.height))
                                - self.metrics) / 2,
                self.text, fg_color)
        else:
            self.frame.draw_text(self.topleft + BUTTON_MARGIN, self.text,
                                 fg_color)

    def update_text(self):
        if self.frame is None:
            raise RuntimeError(
                'trying to update a MenuItem without having set frame')
        if self.text_func is not None:
            self.text = self.text_func()
            self.metrics[:] = np.array(self.frame.text_size(self.text))
            if (w := self.metrics[0] + BUTTON_MARGIN[0]) > self.width:
                self.width = w
            if (h := self.metrics[1] + BUTTON_MARGIN[1]) > self.height:
                self.height = h

    def update(self, dt):
        self.update_text()

    def contains_point(self, pos):
        if self.topleft is None:
            raise RuntimeError("can't check if a MenuItem contains a point if "
                               "its position hasn't been set")
        area = [self.topleft, self.bottomright]
        return (area[0][0] < pos[0] < area[1][0]
                and area[0][1] < pos[1] < area[1][1])

    def activate_hotkey(self, key=None):
        pass

    def activate_click(self, position=None):
        pass

    def __str__(self):
        return f'{self.__class__.__qualname__}(text={self.text!r})'


class CompoundItem(MenuItem):
    interactive = True

    def __init__(self, *items: MenuItem, bg_color=None, interface=None,
                 frame=None, pos=None, width=None, height=None):
        self._highlightable = False
        self._highlighted = False
        self._highlighted_item = None

        self._set_pos_after_dimensions = None

        self.items = items

        setup_items_linked_list(self.items)

        self._highlighted_item = next(
            (item for item in items if item.interactive), None)

        self.force_width = None
        self.force_height = None

        if width is not None:
            self.force_width = width
        if height is not None:
            self.force_height = height

        self.width = self.force_width
        self.height = self.force_width

        self.bg_color = bg_color

        self._hotkeys_mapping = {
            hotkey: item for item in items for hotkey in (
                item.hotkeys if item.interactive else ())
        }
        self.hotkeys = list(self._hotkeys_mapping.keys())

        if interface is not None:
            self.set_interface(interface)
        if frame is not None:
            self.set_frame(frame)
        if pos is not None:
            self.set_pos(pos)

    @property
    def text(self):
        return '|'.join(i.text for i in self.items)

    @property
    def highlighted(self):
        return self._highlighted

    @highlighted.setter
    def highlighted(self, value):
        self._highlighted = bool(value)
        if self._highlighted_item is not None:
            self._highlighted_item.highlighted = self._highlighted

    @property
    def highlightable(self):
        return self._highlightable

    @highlightable.setter
    def highlightable(self, value):
        self._highlightable = bool(value)
        for i in self.items:
            i.highlightable = self._highlightable

    def set_highlighted_item(self, item):
        self._highlighted_item.highlighted = False
        self._highlighted_item = item
        item.highlighted = True

    def set_interface(self, interface):
        for item in self.items:
            item.set_interface(interface)

    def set_frame(self, frame):
        self.width = 0
        self.height = 0

        for item in self.items:
            item.set_frame(frame)
            self.width += item.width
            if item.height > self.height:
                self.height = item.height

        if self.force_width is not None:
            self.width = self.force_width
        if self.force_height is not None:
            self.height = self.force_height

        if self._set_pos_after_dimensions:
            self.set_pos(self._set_pos_after_dimensions)
            self._set_pos_after_dimensions = None

    def set_pos(self, pos):
        if self.width is None or self.height is None:
            logger.debug(
                "%s: deferring set_pos because the button's dimensions are "
                "still unknown", self.text
            )
            self._set_pos_after_dimensions = pos
            return
        self.topleft = np.array(pos)
        self.bottomright = self.topleft + (self.width, self.height)

        x, y = pos
        for item in self.items:
            item.set_pos((x, y))
            x += item.width

    def render(self):
        if self.frame is None:
            raise RuntimeError(
                'trying to render a CompoundItem without having set its frame'
            )
        if self.topleft is None:
            raise RuntimeError(
                'trying to render a CompoundItem without having set its '
                'position'
            )

        for item in self.items:
            item.render()

    def update_text(self):
        for item in self.items:
            item.update_text()

    def activate_hotkey(self, key=None):
        logger.debug('Hotkey activated on CompoundItem: looking for the right '
                     'item')
        if (item := self._hotkeys_mapping.get(key)) is not None:
            logger.debug('Activating hotkey on %s', item)
            item.activate_hotkey(key)
        elif key == pygame.K_RIGHT:
            self.set_highlighted_item(
                self._highlighted_item.next_interactive_item)
        elif key == pygame.K_LEFT:
            self.set_highlighted_item(
                self._highlighted_item.previous_interactive_item)
        else:
            self._highlighted_item.activate_hotkey(key)

    def activate_click(self, position=None):
        if position is None:
            self._highlighted_item.activate_click(position)
        for item in self.items:
            if item.contains_point(position):
                item.activate_click(position)
                break


class TextItem(MenuItem):
    pass


class Button(MenuItem):
    interactive = True

    def __init__(self, text, callback, hotkeys=None, *, interface=None,
                 frame=None, pos=None, center_text=True, width=None,
                 height=None, bg_color=MENU_BG_COLOR, fg_color=MENU_FG_COLOR,
                 border_color=BUTTON_BORDER_COLOR):

        super().__init__(text, interface=interface, frame=frame, pos=pos,
                         center_text=center_text, width=width, height=height,
                         bg_color=bg_color, fg_color=fg_color,
                         border_color=border_color)

        self.callback = self.setup_button_action(callback)
        if hotkeys is not None:
            try:
                self.hotkeys = list(hotkeys)
            except TypeError:
                self.hotkeys = [hotkeys]
        else:
            self.hotkeys = []

        self.previous_interactive_item = None
        self.next_interactive_item = None

    def setup_button_action(self, callback):
        if isinstance(callback, type) and getattr(callback, 'concrete_view',
                                                  False):
            def set_view():
                self.interface.set_view(callback)
            return set_view
        elif callable(callback):
            return callback
        else:
            raise TypeError(f"Button's callback argument must be a concrete "
                            f"View or a callable, not {type(callback)}")

    def activate_hotkey(self, key=None):
        if key in (pygame.K_LEFT, pygame.K_RIGHT):
            return
        self.callback()

    def activate_click(self, position=None):
        self.callback()


class NumberEntry(CompoundItem):
    def __init__(self, label, callback=None, hotkeys=None, *, interface=None,
                 frame=None, pos=None, center_text=True, width=None,
                 height=None, bg_color=MENU_BG_COLOR, fg_color=MENU_FG_COLOR,
                 border_color=BUTTON_BORDER_COLOR, default=None, minimum=None,
                 maximum=None):
        self.minimum = minimum
        self.maximum = maximum

        self.value = (default if default is not None
                      else getattr(callback, 'default', 0))
        self.callback = callback

        self.minus_button = Button('-', self.decrease)

        if not isinstance(label, MenuItem):
            self.label = TextItem(label, center_text=False,
                                  border_color=border_color)
        else:
            self.label = label

        self.text_box = TextBox(
            self.submit_text_box, interface=interface, frame=frame, pos=pos,
            center_text=center_text, width=width, height=height,
            bg_color=bg_color, fg_color=fg_color, border_color=border_color,
            default=str(self.value), validator=self.validate_number)

        self.plus_button = Button('+', self.increase, hotkeys=hotkeys)

        super().__init__(self.minus_button, self.label, self.text_box,
                         self.plus_button)

    @property
    def highlighted(self):
        return self._highlighted

    @highlighted.setter
    def highlighted(self, value):
        self._highlighted = bool(value)
        for item in self.items:
            item.highlighted = self._highlighted

    def activate_hotkey(self, key=None):
        if key == pygame.K_LEFT:
            self.decrease()
        elif key == pygame.K_RIGHT:
            self.increase()
        elif pygame.key.get_mods() & pygame.KMOD_SHIFT:
            self.decrease()
        elif key == pygame.K_RETURN:
            self.text_box.activate_hotkey(key)
        else:
            self.increase()

    def activate_click(self, position=None):
        if self.text_box.contains_point(position):
            self.text_box.activate_click(position)
        elif position[0] - self.topleft[0] > self.width / 2:
            self.increase()
        else:
            self.decrease()

    def set_value(self, value):
        if self.maximum is not None and value > self.maximum:
            self.value = self.maximum
        elif self.minimum is not None and value < self.minimum:
            self.value = self.minimum
        else:
            self.value = value
        self.text_box.value = str(self.value)
        self.callback(self.value)

    def decrease(self):
        if self.minimum is not None and self.value - 1 < self.minimum:
            if self.maximum is not None:
                self.set_value(self.maximum)
        else:
            self.set_value(self.value - 1)

    def increase(self):
        if self.maximum is not None and self.value + 1 > self.maximum:
            if self.minimum is not None:
                self.set_value(self.minimum)
        else:
            self.set_value(self.value + 1)

    def set_frame(self, frame):
        self.frame = frame
        if self.text_box.width is None:
            if (
                self.maximum is None
                or self.maximum < (10 ** NUMBER_ENTRY_DEFAULT_DIGIT_NUMBER)
            ):
                dimensions = self.frame.text_size(
                    '9' * NUMBER_ENTRY_DEFAULT_DIGIT_NUMBER)
            else:
                dimensions = self.frame.text_size(str(self.maximum))
            self.text_box.width = dimensions[0] + 2 * BUTTON_MARGIN[0]
        super().set_frame(frame)

    def set_pos(self, pos):
        if None in (self.minus_button.width, self.plus_button.width,
                        self.text_box.width):
            raise RuntimeError('%s.set_pos needs its components to have its '
                               'dimensions set beforehand', self.text)
        if self.width is not None:
            used_width = (self.minus_button.width + self.plus_button.width
                          + self.text_box.width)
            self.label.width = self.width - used_width
        # Else, set_pos will be deferred to after set_frame has been called
        super().set_pos(pos)

    def validate_number(self, value):
        try:
            int(value)
        except ValueError:
            self.interface.message(
                title='Invalid number',
                message='Please enter a valid integer.')
            raise

    def submit_text_box(self, value):
        self.set_value(int(value))


class TextBox(Button):
    def __init__(self, callback=None, hotkeys=None, *, interface=None,
                 frame=None, pos=None, center_text=True, width=None,
                 height=None, bg_color=MENU_BG_COLOR, fg_color=MENU_FG_COLOR,
                 border_color=BUTTON_BORDER_COLOR, show_cursor=True,
                 default=None, validator=None, max_length=None,
                 validate_on_blur=True):
        self.value = (default if default is not None
                      else str(getattr(callback, 'default', '')))
        self.show_cursor = show_cursor
        self.validator = validator
        if max_length is not None:
            self.max_length = max_length
        else:
            self.max_length = float('inf')
        self.validate_on_blur = validate_on_blur

        self.old_value = self.value
        self.focused = False
        self.ignore_key = False
        self.old_repeat_mode = pygame.key.get_repeat()
        self.cursor_pos = 0

        self.metrics = np.array((0, 0))

        super().__init__(
            self.get_text, callback, hotkeys, interface=interface, frame=frame,
            pos=pos, center_text=center_text, width=width, height=height,
            bg_color=bg_color, fg_color=fg_color, border_color=border_color)

    def get_text(self):
        return self.value

    def validate(self):
        self.interface.release_focus()
        if self.validator is not None:
            try:
                self.validator(self.value)
            except ValueError:
                self.value = self.old_value
                return
        self.callback(self.value)

    def activate(self, event_value=None):
        self.old_value = self.value
        self.interface.claim_focus(self, handled_events=(
            pygame.TEXTINPUT, pygame.KEYUP, pygame.KEYDOWN,
            pygame.MOUSEBUTTONUP,
        ))
        if self.interface.in_web_browser and self.interface.is_mobile:
            # Since the user doesn't have a physical keyboard, let's use
            # prompt() to let him input a value.
            import platform
            restore_fullscreen = self.interface.fullscreen
            self.interface.fullscreen = False
            self.value = platform.window.prompt('Enter value:')
            self.validate()
            self.interface.fullscreen = restore_fullscreen

    activate_click = activate

    def activate_hotkey(self, key):
        if key != pygame.K_RETURN:
            self.ignore_key = True
        self.activate()

    def enter_focus(self):
        self.focused = True
        self.cursor_pos = len(self.value)
        self.old_repeat_mode = pygame.key.get_repeat()
        pygame.key.set_repeat(300, 40)
        pygame.key.start_text_input()
        pygame.key.set_text_input_rect((self.topleft, self.bottomright))

    def exit_focus(self):
        self.focused = False
        pygame.key.set_repeat(*self.old_repeat_mode)
        pygame.key.stop_text_input()

    def handle_event(self, event):
        # Don't show modifiers / home / end / ... key presses when debugging
        # (there is already too much clutter). Only show ASCII characters.
        MAX_KEY_VALUE = 127
        if (event.type not in (pygame.KEYDOWN, pygame.KEYUP)
            or event.key <= MAX_KEY_VALUE):
            logger.debug(
                'TextBox received Event: %s (cursor_pos=%s, value=%s)',
                event, repr(self.cursor_pos), repr(self.value)
            )
        if self.ignore_key:
            logger.debug('TextBox ignored keypress')
            self.ignore_key = False
            return
        if event.type == pygame.TEXTINPUT:
            if len(self.value) + 1 > self.max_length:
                return
            self.value = ''.join((self.value[:self.cursor_pos], event.text,
                                  self.value[self.cursor_pos:]))
            self.cursor_pos += 1
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                self.validate()
            elif event.key == pygame.K_ESCAPE:
                self.value = self.old_value
                self.interface.release_focus()
            elif event.key == pygame.K_BACKSPACE:
                if not self.cursor_pos > 0:
                    return
                self.value = ''.join((self.value[:self.cursor_pos - 1],
                                      self.value[self.cursor_pos:]))
                self.cursor_pos -= 1
            elif event.key == pygame.K_DELETE:
                if self.cursor_pos >= len(self.value):
                    return
                self.value = ''.join((self.value[:self.cursor_pos],
                                      self.value[self.cursor_pos + 1:]))
            elif event.key == pygame.K_LEFT:
                self.cursor_pos -= 1
            elif event.key == pygame.K_RIGHT:
                self.cursor_pos += 1
            elif event.key == pygame.K_END:
                self.cursor_pos = len(self.value)
            elif event.key == pygame.K_HOME:
                self.cursor_pos = 0
            elif event.mod & pygame.KMOD_CTRL:
                if event.key == pygame.K_k:
                    self.value = self.value[:self.cursor_pos:]
                elif event.key == pygame.K_u:
                    self.value = self.value[self.cursor_pos:]
                    self.cursor_pos = 0
                elif event.key == pygame.K_v:
                    clipboard = ''.join(
                        c for c in self.interface.get_clipboard()
                        if c.isprintable()
                    )
                    if len(self.value) + 1 > self.max_length:
                        return
                    if len(self.value) + len(clipboard) > self.max_length:
                        clipboard = clipboard[:self.max_length
                                               - len(self.value)]
                    self.value = ''.join((
                        self.value[:self.cursor_pos], clipboard,
                        self.value[self.cursor_pos:],
                    ))
                    self.cursor_pos += len(clipboard)
        elif event.type == pygame.MOUSEBUTTONUP:
            if not self.contains_point(event.pos):
                if self.validate_on_blur:
                    self.validate()
                else:
                    self.interface.release_focus()
            else:
                mouse_offset = (event.pos[0] - self.topleft[0]
                                - BUTTON_MARGIN[0])

                if len(self.value) == 0:
                    return
                elif len(self.value) == 1:
                    if mouse_offset >= self.frame.text_size(self.value)[0] / 2:
                        self.cursor_pos = 1
                    else:
                        self.cursor_pos = 0
                    return

                bounds = [0, len(self.value)]
                pos = round(sum(bounds) / 2)
                text_length = self.frame.text_size(self.value[:pos])[0]
                if text_length > mouse_offset:
                    bounds[1] = pos
                else:
                    bounds[0] = pos

                pos = round(pos * mouse_offset / text_length)
                while bounds[1] - bounds[0] > 1:
                    text_length = self.frame.text_size(self.value[:pos])[0]
                    if text_length > mouse_offset:
                        bounds[1] = pos
                    else:
                        bounds[0] = pos
                    pos = round(sum(bounds) / 2)
                bound_1_diff = abs(self.frame.text_size(
                    self.value[:bounds[0]])[0] - mouse_offset)
                bound_2_diff = abs(self.frame.text_size(
                    self.value[:bounds[1]])[0] - mouse_offset)
                if bound_1_diff < bound_2_diff:
                    self.cursor_pos = bounds[0]
                else:
                    self.cursor_pos = bounds[1]

    def render(self):
        super().render()
        if self.focused and self.show_cursor and int(time.monotonic() * 2) % 2:
            text_before_cursor_metrics = self.frame.text_size(
                self.value[:self.cursor_pos])
            bottom = self.topleft + BUTTON_MARGIN + text_before_cursor_metrics
            top = (bottom[0], self.topleft[1] + BUTTON_MARGIN[1])
            self.frame.draw_polygon((top, bottom), self.fg_color)


class TextEntry(CompoundItem):
    def __init__(self, label, callback=None, hotkeys=None, *, interface=None,
                 frame=None, pos=None, center_text=False, width=None,
                 height=None, bg_color=MENU_BG_COLOR, fg_color=MENU_FG_COLOR,
                 border_color=BUTTON_BORDER_COLOR, show_cursor=True,
                 default=None, validator=None, max_length=None,
                 validate_on_blur=True):
        if not isinstance(label, MenuItem):
            label = TextItem(label, border_color=border_color)

        text_box = TextBox(callback, hotkeys, interface=interface, frame=frame,
                           pos=pos, center_text=center_text, bg_color=bg_color,
                           fg_color=fg_color, border_color=border_color,
                           show_cursor=show_cursor, default=default,
                           validator=validator, max_length=max_length,
                           validate_on_blur=validate_on_blur)

        self.label = label
        self.text_box = text_box

        super().__init__(label, text_box, interface=interface, frame=frame,
                         pos=pos, width=width, height=height)

    @property
    def value(self):
        return self.text_box.value

    @value.setter
    def value(self, value):
        self.text_box.value = value

    def set_frame(self, frame):
        super().set_frame(frame)
        if self.force_width is None:
            self.width = 2 * self.label.width
        self.text_box.width = self.width - self.label.width

    def set_pos(self, pos):
        if self.width is None:
            # set_pos will be deferred by the super() call
            pass
        elif self.label.width is None:
            raise RuntimeError('%s.set_pos needs its label to have its '
                               'dimensions set beforehand', self.text)
        else:
            self.text_box.width = self.width - self.label.width
        super().set_pos(pos)


class Menu:
    def __init__(self, interface, title, menu_items, frame=None):
        self.interface = interface

        if frame is not None:
            self.frame = frame
            self._new_frame_created = False
        else:
            self.frame = ScrollableFrame(
                self.interface.main_frame, (0, 0),
                real_dimensions=container_dims, virtual_dimensions=[0, 0],
                debug_name=f'menu {title=}')
            self._new_frame_created = True

        self._frame_is_scrollable = isinstance(self.frame, ScrollableFrame)

        self.title = title

        self.items = []
        self.hotkeys = {}

        for item in menu_items:
            item.set_interface(self.interface)
            item.set_frame(self.frame)
            item.highlightable = True
            self.items.append(item)
            if item.interactive:
                for hotkey in item.hotkeys:
                    self.hotkeys[hotkey] = item

        setup_items_linked_list(menu_items)

        self.highlighted_item = next(
            b for b in reversed(self.items) if b.interactive)
        self.highlighted_item.highlighted = True

        self.calc_positions()

    def set_highlighted_item(self, item: MenuItem):
        if (self._frame_is_scrollable
            and self.highlighted_item is next(b for b in reversed(self.items)
                                              if b.interactive)):
            self.frame.scroll_to_top()
        self.highlighted_item.highlighted = False
        self.highlighted_item = item
        item.highlighted = True

        if self._frame_is_scrollable:
            self.frame.scroll_into_view(
                self.highlighted_item.topleft - BUTTON_MARGIN,
                self.highlighted_item.bottomright + BUTTON_MARGIN,
            )

    def calc_positions(self):
        self.items_width = max(b.width for b in self.items)

        self.title_dimensions = self.frame.text_size(self.title)
        self.title_pos = (self.frame.dimensions[0] / 2
                          - self.title_dimensions[0] / 2,
                          MENU_TOP_MARGIN)

        x = self.frame.dimensions[0] / 2 - self.items_width / 2
        y = self.title_pos[1] + self.title_dimensions[1] + BUTTON_MARGIN[1]

        for b in self.items:
            b.width = self.items_width
            b.set_pos((x, y))
            y += b.height + BUTTON_MARGIN[1]

        self.total_height = y
        if self._frame_is_scrollable:
            self.frame.set_virtual_dimensions((container_x, self.total_height))

    def draw_title(self):
        self.frame.draw_text(self.title_pos, self.title, MENU_FG_COLOR)

    def on_key_press(self, key):
        if (button := self.hotkeys.get(key, None)) is not None:
            self.set_highlighted_item(button)
            button.activate_hotkey(key)
        elif key in (pygame.K_DOWN, pygame.K_UP):
            button = self.highlighted_item
            if key == pygame.K_DOWN:
                self.set_highlighted_item(button.next_interactive_item)
            elif key == pygame.K_UP:
                self.set_highlighted_item(button.previous_interactive_item)
        elif key in (pygame.K_RETURN, pygame.K_SPACE, pygame.K_LEFT,
                     pygame.K_RIGHT):
            self.highlighted_item.activate_hotkey(key)
        elif key == pygame.K_ESCAPE:
            self.interface.previous_view()
        elif self._frame_is_scrollable:
            if self.frame.handle_key_press(key):
                return

    def on_click(self, position):
        for item in self.items:
            if item.contains_point(position):
                if item.interactive:
                    self.set_highlighted_item(item)
                item.activate_click(position)
                break

    def on_scroll(self, movement, pos):
        if self._frame_is_scrollable:
            self.frame.scroll(movement)

    def update(self, dt):
        for item in self.items:
            item.update(dt)
        self.calc_positions()

    def render(self, *, fill=True):
        if fill:
            self.frame.fill(MENU_BG_COLOR)
        self.draw_title()
        for b in self.items:
            b.render()
        if self._new_frame_created:
            self.frame.render()

    def exit(self):
        if self._new_frame_created:
            self.frame.delete()

#! /usr/bin/env python3

import argparse
import functools
import re
import shlex
import traceback

from .logic.data import NetworkMode
from .settings import parse_value, serialize_value, SettingsType

try:
    import cmd
except ImportError:
    # No cmd in pygbag
    class CmdModule:
        class Cmd:
            ruler = '='
            prompt = '(Cmd) '

            def __init__(self):
                pass

            def default(self, line):
                pass

            def onecmd(self, line):
                line = line.strip()
                if line and line[0] == '?':
                    line = f'help {line[1:]}'
                if ' ' in line:
                    cmd, args = line.split(' ', 1)
                else:
                    cmd = line
                    args = ''
                func = getattr(self, f'do_{cmd}', None)
                if func is not None:
                    return func(args)
                else:
                    return self.default(line)

            def columnize(self, iterable, displaywidth=80):
                iterable = list(iterable)
                if len(iterable) == 0:
                    print('<empty>')
                    return
                i = 0
                while i < len(iterable):
                    line_length = 0
                    while (i < len(iterable)
                           and line_length + len(iterable[i]) + len(', ')
                           < displaywidth):
                        print(iterable[i], end=', ')
                        i += 1
                    if i >= len(iterable):
                        print()
                        return
                    print()
                    if len(iterable[i]) + len(', ') >= displaywidth:
                        print(iterable[i], end=', \n')
                        i += 1
                print()

            def cmdloop(self):
                stop = False
                while not stop:
                    try:
                        line = input(self.prompt)
                    except EOFError:
                        line = 'EOF'
                    stop = self.onecmd(line)

            def do_help(self, line):
                """Show help about some topic or list available help topics and
commands"""

                line = line.strip()
                if line == '':
                    command_functions = [getattr(self, c) for c in dir(self) if
                                         c.startswith('do_')]
                    print('Documented commands:')
                    print(self.ruler * 80)
                    self.columnize(f.__name__[len('do_'):] for f in
                                   command_functions if f.__doc__ is not None)

                    undocumented_commands = [
                        f.__name__[len('do_'):] for f in command_functions
                        if f.__doc__ is None]
                    if undocumented_commands:
                        print()
                        print('Undocumented commands:')
                        print(self.ruler * 80)
                        self.columnize(undocumented_commands)
                else:
                    func = getattr(self, f'do_{line}', None)
                    if func is None:
                        print(f'No command or topic named {line}')
                    elif func.__doc__ is None:
                        print(f'No help about {func}')
                    else:
                        print(func.__doc__)

    cmd = CmdModule()
    del CmdModule


def servermodeonly(func):
    @functools.wraps(func)
    def wrapper(self, *args):
        if self.application.settings.server_mode:
            return func(self, *args)
        else:
            self.perror(
                'exit can only be used when Back from Altair is run in server '
                'mode.')
            return None
    return wrapper


class GameShell(cmd.Cmd):
    def __init__(self, application, game, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.application = application
        self.game = game

        self.eval_globals = {
            'application': self.application,
            'game': self.game,
            'interface': self.application.interface,
            'web_io': self.application.web_io
        }
        self.eval_locals = {}

    def poutput(self, *args):
        print(*args)

    def perror(self, *args):
        print(f'\033[31m{" ".join(args)}\033[0m')

    def default(self, line):
        if line == 'EOF':
            return self.do_exit(line)
        return super().default(line)

    def do_state(self, line):
        """Print the state of the game and the network mode."""
        self.poutput(f'Game state: {self.game.state}')
        self.poutput(
            f'Network mode: {self.application.game_settings.network_mode}')

        if self.application.game_settings.network_mode == NetworkMode.SERVER:
            players = (self.application.web_io.ws_server.room
                       .connected_players.values())
            max_clients = self.application.game_settings.player_number
            if not self.application.settings.server_mode:
                max_clients -= 1
            self.poutput(f'Connected players ({len(players)}/{max_clients}):')
            self.poutput(self.ruler * 80)
            self.columnize([p.name for p in players])
            self.poutput()
            self.poutput(self.application.debug_info())

    def complete_settings(self, text, line, begidx, endidx):
        args_string = re.sub(fr'^\s*[{self.identchars}]+\s*', '', line)
        # Add a letter, because we want to count the last space as another
        # argument. For example, if args='settings debug ', we're completing
        # the second argument.
        argnum = len(shlex.split(f'{args_string}a'))
        args = shlex.split(args_string)
        if argnum not in (1, 2):
            return []
        if not args:
            args = ['']
        *attrs, beginning = args[0].split('.')
        try:
            settings = functools.reduce(
                lambda a, b: getattr(a, b), attrs,
                self.application.settings)
        except AttributeError:
            return []
        if argnum == 1:
            if not isinstance(settings, SettingsType):
                return [f'{text} ']
            prefix = ''.join(f'{a}.' for a in attrs)
            return [
                (f'{prefix}{a}.' if isinstance(field.type, SettingsType)
                 else f'{prefix}{a} ')
                for a, field in settings.settings_fields.items()
                if a.startswith(beginning)
            ]

        if not isinstance(settings, SettingsType):
            return []
        field = settings.settings_fields.get(beginning)
        if field is None:
            return []
        if field.choices is not None:
            return [as_string for c in field.choices
                    if (as_string := serialize_value(c)).startswith(text)]
        elif field.type == bool:
            return [c for c in ('true', 'false', 'yes', 'no') if
                    c.startswith(text)]

        return []

    def do_settings(self, line):
        """Get and set settings.
Syntax:
    to get settings: settings the.setting.youre.looking.for
    to set settings: settings the.setting.you.want.to.set new_value
"""

        def print_settings(settings):
            self.poutput('Sub-settings groups:')
            self.poutput(self.ruler * 80)
            self.columnize(settings.subsettings, 80)
            self.poutput()
            self.poutput('Settings:')
            self.poutput(self.ruler * 80)
            self.columnize(list(settings.attributes_mapping.values()), 80)

        args = shlex.split(line)
        if len(args) == 0:
            print_settings(self.application.settings)
            return
        attrs = args[0].split('.')
        try:
            settings = functools.reduce(
                lambda a, b: getattr(a, b), attrs[:-1],
                self.application.settings)
            if len(args) == 1:
                val = getattr(settings, attrs[-1])
                if isinstance(val, SettingsType):
                    print_settings(val)
                else:
                    self.poutput(serialize_value(getattr(settings, attrs[-1])))
            elif len(args) == 2:
                setattr(
                    settings, attrs[-1],
                    parse_value(settings.settings_fields[attrs[-1]].type,
                                args[1]))
        except AttributeError:
            self.perror("this setting isn't available")
            if self.application.settings.debug:
                self.poutput(traceback.format_exc())
        except (argparse.ArgumentTypeError, ValueError, TypeError):
            self.perror("Invalid setting value")
            if self.application.settings.debug:
                self.poutput(traceback.format_exc())

    @servermodeonly
    def do_startgame(self, line):
        """In server mode, start the game. Else, print an error."""
        self.game.start_game()

    def do_py(self, line):
        """Execute python code"""
        try:
            self.poutput('Result:', eval(
                line, self.eval_globals, self.eval_locals))
        except SyntaxError:
            exec(line, self.eval_globals, self.eval_locals)
        except BaseException:
            self.perror(traceback.format_exc())

    @servermodeonly
    def do_exit(self, line):
        """If Back from Altair is running in server mode, shut the server down.
Else, print an error."""
        self.application.continue_ = False
        return True

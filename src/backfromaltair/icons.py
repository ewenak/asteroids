#! /usr/bin/env python3

import numpy as np

from .constants import STATUS_LINE_MARGIN


class Icon:
    def __init__(self, path=None):
        if path is not None:
            self.path = list(self.convert_path(path))
        else:
            self.generate_path()

    def convert_path(self, path):
        color = 'white'
        fill = None
        current_path = []
        for t in path:
            if t is None:
                yield current_path, color, fill
                current_path = []
            if len(t) == 3:
                x, y, color = t
            elif len(t) == 4:
                x, y, color, fill = t
            else:
                x, y = t
            current_path.append(np.array((x, y)))
        yield current_path, color, fill

    def draw_rect(self, topleft, bottomright, stroke, fill=None):
        x1, y1 = topleft
        x2, y2 = bottomright

        self.path.append([
            np.array((
                (x1, y1),
                (x2, y1),
                (x2, y2),
                (x1, y2)
            )),
            stroke, fill,
        ])

    def draw_polygon(self, points, stroke, fill=None):
        self.path.append((points, stroke, fill))

    def generate_path(self):
        return []

    def draw_to(self, topleft, frame):
        for shape, color, fill in self.path:
            frame.draw_polygon((shape + topleft), color, fill=fill)


class TankIcon(Icon):
    def __init__(self, height, width=None, fraction_filled=None):
        if fraction_filled is None:
            self.fraction_filled = 1
        else:
            self.fraction_filled = fraction_filled
        self.height = height
        if width is None:
            self.width = 0.6 * height
        else:
            self.width = width
        super().__init__()

    def generate_path(self):
        self.path = []
        height_empty = (1 - self.fraction_filled) * self.height
        # We keep 1 pixel on the right and on the left for the lines
        self.draw_rect(np.array((1, 0)), np.array((
            self.width - 1, self.height)), 'white')
        x1 = 0
        x2 = self.width
        for line in [0, 0.25, 0.75, 1]:
            y = line * self.height
            self.draw_polygon((np.array((x1, y)), np.array((x2, y))), 'white')
        # Add one to the self.height because the floor of the value is used
        # Without it, there is a 1px black line at the bottom in the tank
        # drawing
        self.draw_rect(np.array((1, height_empty + 1)),
                       np.array((self.width - 1, self.height + 1)),
                       stroke='white', fill='white')


class BulletIcon(Icon):
    def __init__(self, height, width=None):
        self.height = height
        if width is None:
            self.width = int(0.4 * height)
        else:
            self.width = width
        super().__init__()

    def generate_path(self):
        self.path = []
        top_part_height = int(self.height * 0.75)
        top_part_points = np.array((
            (self.width / 2, 0),
            (self.width, top_part_height / 2),
            (self.width, top_part_height),
            (0, top_part_height),
            (0, top_part_height / 2)
        ))
        bottom_part_box = np.array((
            (0, int(top_part_height + STATUS_LINE_MARGIN / 2)),
            (self.width, self.height)
        ))
        self.draw_polygon(top_part_points, 'white', fill='white')
        self.draw_rect(bottom_part_box[0], bottom_part_box[1],
                       stroke='white', fill='white')

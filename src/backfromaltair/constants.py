#! /usr/bin/env python3

from enum import Enum
from math import pi

import numpy as np

# Units:
# - Length are in pixels
# - Times are in ms
# - Speeds are in px/ms
# - Accelerations are in px/ms/ms
# - Angles are in radians

DEFAULT_DIMENSIONS = (650, 650)
BACKGROUND_COLOR = 'black'
APP_NAME = "backfromaltair"
APP_TITLE = "Back from Altair"
FPS_LIMIT = 30
FRAME_MIN_PERIOD = 1 / FPS_LIMIT

GAME_AREA = np.array((2000, 2000))

# Only used in original pygame. Changed using font.set_point_size in pygame-ce
INITIAL_FONT_SIZE = 30

# Splitscreen layouts by player number.
# Values are a tuple of viewports top left coordinates and viewports dimesions
# in fraction of screen width/height, followed by a name for the player
# corresponding to that viewport.
SPLITSCREEN_PLAYER_LAYOUTS = {
    1: (
        ((0, 0), (1, 1), 'you'),
    ),
    2: (
        ((0, 0), (1, 0.5), 'top player'),
        ((0, 0.5), (1, 0.5), 'bottom player'),
    ),
    3: (
        ((0, 0), (0.5, 0.5), 'top left player'),
        ((0.25, 0.5), (0.5, 0.5), 'top right player'),
        ((0.5, 0), (0.5, 0.5), 'bottom player'),
    ),
    4: (
        ((0, 0), (0.5, 0.5), 'top left player'),
        ((0, 0.5), (0.5, 0.5), 'top right player'),
        ((0.5, 0), (0.5, 0.5), 'bottom left player'),
        ((0.5, 0.5), (0.5, 0.5), 'bottom right player'),
    ),
}
MAX_LOCAL_PLAYER_NUMBER = len(SPLITSCREEN_PLAYER_LAYOUTS)
MAX_BOT_NUMBER = 10

BASE_URL = 'https://ewenak.gitlab.io/backfromaltair/'

RELATIVE_ABOUT_URL = 'about.html'
RELATIVE_EXIT_URL = 'exit.html'

# Time to wait after game is over before quitting on click / keydown
END_TIMEOUT = 2000

MULTIPLAYER_DEFAULT_PORT = 9897
MULTIPLAYER_PORT_MINIMUM = 1024
MULTIPLAYER_PORT_MAXIMUM = 65535


ASTEROID_COUNT = 5
ASTEROID_FULL_SIZE = 100
ASTEROID_SIDES = 7
ASTEROID_MIN_ROTATION = 0.01 / 30
ASTEROID_MAX_ROTATION = 0.06 / 30
ASTEROID_MIN_SPEED = 0.4
ASTEROID_MAX_SPEED = 1.2
ASTEROID_SPLIT_COUNT = 3

BULLET_MAX_DISTANCE = ASTEROID_FULL_SIZE * 3
BULLET_SIZE = 7
BULLET_SLOW_DOWN = -0.01
BULLET_SPEED = 1
BULLET_WIDTH = 0

# Maximum and minimum delay between two items of the same type
ITEM_MAX_DELAY = 1 * 60 * 1000
ITEM_MIN_DELAY = 5 * 1000
ITEM_DELAY_RANGE_SIZE = 15 * 1000
ITEM_SIZE = 15

PLAYER_ACCELERATION = 0.2 / 30
# Angles of the vertices in radians
PLAYER_VERTICES = (0, 5 * pi / 6, 7 * pi / 6)
PLAYER_FIRE_PERIOD = 0.1 * 1000
PLAYER_LIVES = 3
PLAYER_MAX_SPEED = 1
PLAYER_MAX_SPEED_SQUARED = PLAYER_MAX_SPEED ** 2
PLAYER_MIN_TIME_DEAD = 3 * 1000
PLAYER_MAX_BULLETS_NUMBER = 900
PLAYER_RADIUS = 10
PLAYER_FULL_TANK = 20
PLAYER_TURN_SPEED = 0.2 / 30
PLAYER_COLORS = (
    (150, 150, 255),
    (255, 150, 150),
    (150, 255, 150),
    (195, 195, 150),
)

# Approximately the average dt * BotShip.properties.turn_speed. See
# BotShip.update (src/backfromaltair/logic/ships.py:332) method for details
BOT_TURN_THRESHOLD = 0.15

RESPAWN_AREA_RADIUS = ASTEROID_FULL_SIZE * 2

SPACE_OBJ_COLOR = 'white'

STAR_COLORS = ((220, 220, 220), (150, 150, 150))
# Star per pixel
STAR_DENSITY = 4 / 100000
# Stars radius in pixels
STAR_SIZES = (1, 2)

STATUS_LINE_HEIGHT = 30
STATUS_LINE_BG_COLOR = BACKGROUND_COLOR
STATUS_LINE_MARGIN = 5
STATUS_LINE_SEP_COLOR = (180, 180, 180)

ZOOM_CHANGE = 0.03
ZOOM_TARGET_THRUSTING = 1.2


class Sound(Enum):
    # 1: radius of 100; 2: radius of 50; 3: radius of 25; 4: radius of 12.5
    ASTEROID_1 = 'asteroid_1.ogg'
    ASTEROID_2 = 'asteroid_2.ogg'
    ASTEROID_3 = 'asteroid_3.ogg'
    ASTEROID_4 = 'asteroid_4.ogg'
    THRUST = 'thrust.ogg'
    BULLET = 'bullet.ogg'
    LOSE = 'lose.ogg'
    SHIP_EXPLOSION = 'ship_explosion.ogg'
    WIN = 'win.ogg'
    ITEM_APPEARED = 'item_appeared.ogg'
    POWER_UP = 'power_up.ogg'


Sound.ASTEROID_BY_RADIUS = {
    100: Sound.ASTEROID_1,
    50: Sound.ASTEROID_2,
    25: Sound.ASTEROID_3,
    12.5: Sound.ASTEROID_4,
}
SOUND_EFFECTS_DIR = 'sounds'
SOUND_MUSIC_DIR = 'music'

MENU_BG_COLOR = 'black'
MENU_FG_COLOR = 'white'
MENU_TOP_MARGIN = 100

CHAT_HEIGHT = 200
CHAT_OPACITY = 190

RECENT_MESSAGES_BOTTOM = CHAT_HEIGHT
RECENT_MESSAGES_MAX_AGE = 10e3
RECENT_MESSAGES_MAX_NUMBER = 5
RECENT_MESSAGES_FADE_OUT_AGE = 2e3

BUTTON_BORDER_COLOR = MENU_FG_COLOR
# Only used in pygame-ce
BUTTON_FONT_SIZE = 20
BUTTON_MARGIN = np.array((5, 5))
NUMBER_ENTRY_DEFAULT_DIGIT_NUMBER = 3

DEATH_FEEDBACK_TEXT = 'You died!'
DEATH_FEEDBACK_TEXT_COLOR = (140, 0, 30)
STATE_FEEDBACK_FONT_SIZE = 2 * BUTTON_FONT_SIZE

NOTIFICATION_WIDTH_RATIO = 2 / 3
NOTIFICATION_DURATION = 1500
NOTIFICATION_FADE_OUT_DURATION = 1/5 * NOTIFICATION_DURATION

SCROLL_INCREMENT = 15

VIRTUAL_JOYSTICK_BG_COLOR = (70, 70, 70)
VIRTUAL_JOYSTICK_RADIUS = 40
# Horizontal ratio between virtual joystick area and fire area
VIRTUAL_JOYSTICK_RATIO = 0.5
VIRTUAL_JOYSTICK_TOUCH_COLOR = (100, 100, 100)

# Consider a joystick value < 0.5 like 0, to have a margin for joystick
# measurement error.
JOYSTICK_AXIS_THRESHOLD = 0.5
JOYSTICK_BUTTON_ENTER = 0
JOYSTICK_BUTTON_BACK = 2

#! /usr/bin/env python3

import itertools
import logging
import os
from pathlib import Path
from urllib.parse import urljoin
import zipfile

import pygame

from .constants import Sound, SOUND_EFFECTS_DIR, SOUND_MUSIC_DIR
from .logic.data import ErrorCode, ErrorEvent, EventType

logger = logging.getLogger(__name__)


class InvalidSoundTheme(ValueError):
    pass


class SoundList:
    def __init__(self, application, sound_theme_path=None):
        self.application = application
        self.application.add_event_handler(self.handle_event)
        self.sound_theme_path = sound_theme_path
        self.sound_music_end_event = pygame.event.custom_type()
        self.sounds = {}
        self.music_files = None

    def list_themes(self, callback, path=None):
        if path is None:
            path = self.sound_theme_path
        if isinstance(path, str) and path.startswith(('https://', 'http://')):
            def process(url, content):
                callback({Path(filename).stem: urljoin(path, filename)
                          for filename in content.split('\n') if
                          filename.strip() != ''})
            self.application.web_io.get_request(
                urljoin(path, 'index.txt'), file=None, callback=process)
        else:
            sound_themes = {}
            for file in Path(path).iterdir():
                if file.is_dir():
                    if file.name not in sound_themes:
                        sound_themes[file.name] = file
                elif zipfile.is_zipfile(file):
                    name = file.stem
                    if name not in sound_themes:
                        sound_themes[name] = file
            callback(sound_themes)

    def load_sounds(self, path) -> bool:
        if path is None:
            pygame.mixer.music.stop()
            pygame.mixer.stop()
            self.sounds = {}
            return True

        if isinstance(path, str) and path.startswith(('https://', 'http://')):
            logger.info('downloading sound theme %s', path)

            def callback(url, file):
                self.load_sounds(file)
                os.unlink(file)
            self.application.web_io.get_request(
                path, file=True, callback=callback)
            return True

        path = Path(path)

        pygame.mixer.init()
        self.sounds = {}

        if path.is_dir():
            logger.info('loading sound theme %s (directory format)', path)
            directory = path
            for sound in Sound:
                path = directory / SOUND_EFFECTS_DIR / sound.value
                if path.exists():
                    self.sounds[sound] = pygame.mixer.Sound(file=path)
        elif zipfile.is_zipfile(path):
            logger.info('loading sound theme %s (zip file)', path)
            try:
                directory = self.load_sound_zipfile(
                    path,
                    output_directory=self.application.tempdir / path.stem
                )
            except InvalidSoundTheme:
                logger.error('Invalid sound theme')
                self.application.emit_event(
                    ErrorEvent(ErrorCode.INVALID_SOUND_THEME))
                return False
        else:
            self.application.emit_event(
                ErrorEvent(ErrorCode.INVALID_SOUND_THEME))
            return False

        self.load_music_dir(directory / SOUND_MUSIC_DIR)

        return True

    def load_sound_zipfile(self, path, output_directory):
        try:
            zf = zipfile.ZipFile(path, 'r')
            root = zipfile.Path(zf)
            root_directories = list(root.iterdir())
            root_directory = root_directories[0]
            if not len(root_directories) == 1 or not root_directory.is_dir():
                raise InvalidSoundTheme()
            music_dir = (root_directory / SOUND_MUSIC_DIR).at
            sound_effects_dir = (root_directory / SOUND_EFFECTS_DIR).at
            for file in zf.infolist():
                if file.filename.startswith(music_dir):
                    logger.debug('extracting music file %s', file)
                    zf.extract(file, path=output_directory)
                elif file.filename.startswith(sound_effects_dir):
                    logger.debug('extracting sound effect file %s', file)
                    try:
                        sound = Sound(file.filename[len(sound_effects_dir):])
                    except ValueError:
                        continue
                    with zf.open(file) as audio:
                        self.sounds[sound] = pygame.mixer.Sound(audio.read())
                else:
                    logger.debug('other file: %s', file)
        except zipfile.BadZipFile:
            raise InvalidSoundTheme() from None
        finally:
            zf.close()
        return output_directory / root_directories[0].at

    def load_music_dir(self, directory):
        logger.info('loading music files from directory %s', directory)

        directory = Path(directory)
        if not directory.is_dir():
            return

        self.music_files = itertools.cycle(
            sorted(f for f in directory.iterdir() if f.name.endswith('.ogg')))
        try:
            pygame.mixer.music.load(next(self.music_files))
        except StopIteration:
            logger.info('no music file found')
            self.music_files = None
        else:
            pygame.mixer.music.queue(next(self.music_files))
            pygame.mixer.music.set_endevent(self.sound_music_end_event)
            pygame.mixer.music.play()

    def queue_next_music(self):
        if self.music_files is not None:
            pygame.mixer.music.queue(next(self.music_files))

    def play(self, name: Sound):
        if name in self.sounds:
            self.sounds[name].play()

    def handle_event(self, event):
        if event.event_type == EventType.SOUND:
            self.play(event.sound)

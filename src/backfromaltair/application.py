#! /usr/bin/env python3

import asyncio
from collections import deque
from dataclasses import field
import logging
from pathlib import Path
import sys
import tempfile
import time
import typing

from .constants import FRAME_MIN_PERIOD
from .shell import GameShell
from .logic.game import Game
from .logic.data import (ChangeNetworkModeEvent, Event, EventType,
                         GameSettings, GameState, NetworkMode)
from .settings import ArgparseField, BaseSettings
from . import views
from .web_io import ClientSettings, HostSettings, WebIO

if sys.platform == 'emscripten':
    import platform

logger = logging.getLogger(__name__)


def subclasses(cls):
    s = cls.__subclasses__()
    classes = [*s]
    for c in s:
        classes.extend(subclasses(c))
    return classes


view_list = {v.__name__: v for v in subclasses(views.View) if v.concrete_view}


def initial_view(s):
    v = view_list.get(s)
    if v is None:
        raise ValueError(f'invalid view name: {s}')
    return v


class Settings(BaseSettings):
    prefix: typing.ClassVar = ''

    client: ClientSettings = field(
        default_factory=lambda: ClientSettings())
    host: HostSettings = field(
        default_factory=lambda: HostSettings())
    game_settings: GameSettings = field(
        default_factory=lambda: GameSettings())

    initial_view: type = ArgparseField(
        field=field(default=views.MainMenuView), choices=view_list.values(),
        type=initial_view, help=f"Launch the game in a view other than the "
        f"default. Valid choices are: {', '.join(view_list)}",
        unsaved_setting=True,
    )

    debug: bool = ArgparseField(
        field(default=False), help='Show debug information')

    server_mode: bool = ArgparseField(
        field(default=False), help="Run backfromaltair in server mode, "
        "without interface. The pygame dependency is not needed in this mode.",
        unsaved_setting=True)


# To be used with external tools, like argparse-manpage
argument_parser = Settings.argument_parser


class Application:
    def __init__(self, app_dirs=None, args=None):
        self.app_dirs = app_dirs

        self.settings = Settings()

        if app_dirs is not None:
            self.settings = Settings(
                filename=app_dirs.user_config_path / 'settings.json')
            for path in app_dirs.iter_config_paths():
                if (json_file := path / 'settings.json').exists():
                    self.settings.parse_json_file(json_file)

        if sys.platform == 'emscripten':
            if json_data := platform.window.localStorage.getItem('settings'):
                self.settings.parse_json(json_data)

            search_query = platform.window.location.search
            search_params = platform.new(
                platform.window.URLSearchParams, search_query)
            self.settings.update_from_dict(
                dict(list(search_params)), flat=True)

        if args is not None:
            self.settings.parse_command_line_args(args)

        logger.info('Current settings: %s', self.settings)

        self.queued_events = []
        self._event_handlers = []
        self._async_event_handlers = []

        self.latest_frame_delays = deque(maxlen=15)

        self.add_async_event_handler(self.event_handler)

        self._tempdir = tempfile.TemporaryDirectory(prefix='backfromaltair-')
        self.tempdir = Path(self._tempdir.name)

        self.game_settings = self.settings.game_settings

        if not self.settings.server_mode:
            from .interface import Interface
            self.interface = Interface(self, self.settings)
        else:
            self.interface = None

        self.game = Game(self.game_settings, self.emit_event)
        self.add_event_handler(self.game.receive_event)

        self.web_io = WebIO(self.tempdir, self.settings, self.receive_event)
        self.web_io.http_client.start()

        self.game_shell = GameShell(self, self.game)

        def handle_event_unpack_error(event_type=None):
            if self.game.state == GameState.INITIALIZED:
                self.emit_event(ChangeNetworkModeEvent(NetworkMode.OFFLINE))
                if self.interface is not None:
                    self.interface.previous_view()
            self.notify(
                title='Error communicating with the server',
                message="Couldn't decode some events received from the server",
                debug_info=f"(event_type={event_type})"
            )

        Event.error_handler = handle_event_unpack_error

        self.continue_ = True

        if self.interface is not None:
            self.interface.set_view(self.settings.initial_view)

        self.shell_thread = None
        if self.settings.server_mode:
            self.emit_event(ChangeNetworkModeEvent(NetworkMode.SERVER))

            from threading import Thread
            self.shell_thread = Thread(target=self.game_shell.cmdloop)
            self.shell_thread.daemon = True
            self.shell_thread.start()

    def add_event_handler(self, handler):
        logger.debug('Adding event handler %s', handler)
        self._event_handlers.append(handler)

    def add_async_event_handler(self, handler):
        logger.debug('Adding async event handler %s', handler)
        self._async_event_handlers.append(handler)

    async def event_handler(self, event: Event):
        if event.event_type == EventType.CHANGE_NETWORK_MODE:
            self.game_settings.network_mode = event.network_mode

            # Always cancel client task, we'll create a new one if
            # we are in client mode.
            await self.web_io.ws_client.exit()
            if (event.network_mode != NetworkMode.SERVER
                    and self.web_io.ws_server is not None):
                await self.web_io.ws_server.exit()

            if event.network_mode == NetworkMode.SERVER:
                if (self.web_io.ws_server is not None
                        and not self.web_io.ws_server.running):
                    self.web_io.ws_server.start(self.game)
            elif event.network_mode == NetworkMode.CLIENT:
                self.web_io.ws_client.start()

    def emit_event(self, event):
        """Called by the game object to emit events when playing in online
        multiplayer mode, or to play sounds."""
        logger.debug('Emit event: %s', event)

        if not event.local_event:
            self.web_io.emit_event(event)
            if self.game_settings.network_mode != NetworkMode.CLIENT:
                self.receive_event(event)
        else:
            self.receive_event(event)

    def receive_event(self, event):
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(
                "Application received Event(event_type=%s, object_id=%s, "
                "object_type=%s, ...)", event.event_type,
                getattr(event, 'object_id', None),
                getattr(event, 'object_type', None),
            )

        # Queue of events that will be consumed with async functions
        self.queued_events.append(event)

        for event_handler in self._event_handlers:
            event_handler(event)

    def notify(self, title, message, debug_info):
        if self.interface is not None:
            if self.game.state == GameState.PLAYING:
                self.interface.notify(message, debug_info)
            else:
                self.interface.message(
                    title=title, message=f'{message} {debug_info}')
        else:
            logger.warning(message)
            logger.info('Debugging information: %s', debug_info)

    def debug_info(self):
        fps = (1000 * len(self.latest_frame_delays)
               / sum(self.latest_frame_delays))
        if self.game_settings.network_mode == NetworkMode.OFFLINE:
            ping_info = ''
        elif self.game_settings.network_mode == NetworkMode.CLIENT:
            lat = self.web_io.ws_client.latency
            ping_info = f'Ping: {lat} ms'
        elif len(self.web_io.ws_server.room.connected_players) > 0:
            player_latencies = '\n'.join(
                f'{p.name}: {p.latency} ms' for p in
                self.web_io.ws_server.room.connected_players.values())
            count = 0
            sum_ = 0
            for p in self.web_io.ws_server.room.connected_players.values():
                if p.latency < 0:
                    continue
                else:
                    sum_ += p.latency
                    count += 1
            avg = sum_ / count if count > 0 else '...'
            ping_info = f"""Ping:
{player_latencies}
(avg: {avg} ms)"""
        else:
            ping_info = ''
        return f"""FPS: {fps}
{ping_info}"""

    async def mainloop(self):
        try:
            tick = time.monotonic() * 1000
            while self.continue_:
                dt = int(time.monotonic() * 1000 - tick)
                tick = time.monotonic() * 1000

                self.latest_frame_delays.append(dt)

                if self.interface is not None:
                    self.interface.view.update(dt)
                    self.interface.render()
                elif self.game.state == GameState.PLAYING:
                    self.game.update(dt)

                for event in self.queued_events:
                    for event_handler in self._async_event_handlers:
                        await event_handler(event)

                self.queued_events.clear()

                await self.web_io.send_events()

                if self.web_io.exception:
                    # Quit on coroutine exception
                    break

                # If we have time left, sleep for that time. Else, just
                # sleep(0) to give control back to the asyncio scheduler.
                delay = (FRAME_MIN_PERIOD
                         - (time.monotonic() * 1000 - tick) / 1000)
                if delay < 0:
                    delay = 0
                await asyncio.sleep(delay)
            self.settings.save()
        finally:
            if self.shell_thread is not None and self.shell_thread.is_alive():
                self.game_shell.do_exit('exit')
            self.cleanup()
            await self.web_io.exit()

    def cleanup(self):
        self._tempdir.cleanup()

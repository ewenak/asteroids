#! /usr/bin/env python3

# Add PEP517 block for pygbag
# /// script
# dependencies = [
#     "numpy",
#     "pygame-ce",
# ]
# ///

import asyncio
import sys

from backfromaltair.constants import APP_NAME
from backfromaltair.logging import setup_logging
from backfromaltair.application import Application

if sys.platform != 'emscripten':
    import platformdirs
    backfromaltair_dirs = platformdirs.PlatformDirs(APP_NAME, roaming=True)
else:
    backfromaltair_dirs = None


application = None


async def main_coroutine():
    # Easier debugging with pygbag
    global application    # NOQA: PLW0603

    setup_logging(backfromaltair_dirs)

    application = Application(backfromaltair_dirs, sys.argv[1:])
    await application.mainloop()

    sys.exit()


def main():
    asyncio.run(main_coroutine())


if __name__ == '__main__':
    main()

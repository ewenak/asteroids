#! /usr/bin/env python3

from collections import defaultdict
import logging
import sys
import time

import pygame

from .constants import (
    BUTTON_MARGIN, FPS_LIMIT, APP_TITLE,
    MAX_LOCAL_PLAYER_NUMBER, BASE_URL, RELATIVE_ABOUT_URL,
    RELATIVE_EXIT_URL, JOYSTICK_AXIS_THRESHOLD, JOYSTICK_BUTTON_ENTER,
    JOYSTICK_BUTTON_BACK, NOTIFICATION_DURATION, NOTIFICATION_WIDTH_RATIO,
    SCROLL_INCREMENT,
)
from .controls import default_controls, default_mobile_controls
from .frames import Frame, container_dims
from .logic.data import Event, EventType, NetworkMode
from .sound import SoundList
from .views import GameView, MainMenuView, MessageView, View
from .widgets import Notification

logger = logging.getLogger(__name__)


class Interface:
    def __init__(self, application, settings):
        self.application = application
        self.settings = settings

        self.application.add_event_handler(self.event_handler)

        pygame.display.init()
        pygame.font.init()
        pygame.joystick.init()
        self.joysticks = []

        self._surface = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
        pygame.display.set_caption(APP_TITLE)
        self.main_frame = Frame(
            self._surface, pos=(0, 0),
            dimensions=container_dims,
            debug_name='main_frame'
        )

        try:
            pygame.scrap.init()
        except Exception:
            # pygame.scrap is experimental, so let's not assume it works
            self.clipboard_supported = False
        else:
            self.clipboard_supported = True

        self.pressed_scancodes = {}

        self._fullscreen = False

        self.fps_limit = FPS_LIMIT

        self.about_url = BASE_URL + RELATIVE_ABOUT_URL
        self.exit_url = BASE_URL + RELATIVE_EXIT_URL

        self.controls = [
            {'type': default_controls,
             'settings': default_controls.default_settings()}
            for _ in range(MAX_LOCAL_PLAYER_NUMBER)
        ]

        self._text_surf_cache = {}

        self.in_web_browser = sys.platform == 'emscripten'
        self.has_touch_support = False
        self.is_mobile = False
        if self.in_web_browser:
            import platform
            # We can't use hasattr(platform.window, ...), it's always True
            platform.eval('window.has_touch_support = (("ontouchstart" in '
                          'window) || (navigator.maxTouchPoints > 0) || '
                          '(navigator.msMaxTouchPoints > 0))')
            self.has_touch_support = platform.window.has_touch_support
            self.base_url = (platform.window.location.origin
                             + platform.window.location.pathname)
            self.about_url = self.base_url + RELATIVE_ABOUT_URL
            self.exit_url = self.base_url + RELATIVE_EXIT_URL
            self.in_pwa = platform.window.matchMedia(
                ' (display-mode: fullscreen)').matches
            self.canvas = platform.document.querySelector('canvas')
            self.is_mobile = platform.window.mobile()
            if self.has_touch_support:
                self.controls = [
                    {'type': default_mobile_controls,
                     'settings': default_mobile_controls.default_settings()}
                ]

        if not self.in_web_browser:
            from .assets import get_assets_directory
            self.sound_list = SoundList(
                self.application, get_assets_directory() / 'sounds/')
        else:
            self.sound_list = SoundList(
                self.application, self.base_url + 'sounds/')

        self.view = MainMenuView(self.application, self)
        self.view_stack = []

        self.notifications = []
        self.notification_width = (NOTIFICATION_WIDTH_RATIO
                                   * self.main_frame.dimensions[0])

        self.focused_object = None
        self._focused_object_events = ()

        self.shown = True

        self.touches = {}
        self.last_joystick_positions = defaultdict(
            lambda: {'axis': {}, 'hats': {}})

    def set_view(self, view):
        if isinstance(view, View):
            new_view = view
        else:
            new_view = view(self.application, self)
        self.view_stack.append(self.view)
        self.view = new_view
        if isinstance(self.view, MainMenuView):
            for v in reversed(self.view_stack):
                v.exit()
            self.view_stack.clear()
        self.view.enter()
        logger.info('View set to %s', view)
        logger.debug('View stack: %s', self.view_stack)
        self.settings.save()

    def previous_view(self):
        logger.info('Setting view to the previous one...')
        if len(self.view_stack) > 0:
            self.view.exit()
            self.view = self.view_stack.pop()
            logger.info('View set to previous view: %s', self.view)
            logger.debug('View stack: %s', self.view_stack)
            self.settings.save()

    def claim_focus(self, obj, handled_events=()):
        if self.focused_object is not None:
            logger.error('tried to focus an object while another was already '
                         'focused; this should not have happened.')
            self.focused_object.exit_focus()
        logger.info('%s gained focus', obj)
        self.focused_object = obj
        self._focused_object_events = handled_events
        obj.enter_focus()

    def release_focus(self):
        logger.info('focus released (focused_object=%s)', self.focused_object)
        self.focused_object.exit_focus()
        self.focused_object = None

    def message(self, title, message=''):
        self.set_view(MessageView(self.application, self, title, message))

    def notify(self, message, *, debug_info=''):
        self.notifications.append(Notification(
            self.main_frame, self.notification_width, message, debug_info))

    def event_handler(self, event: Event):
        if (
            event.event_type == EventType.START_GAME
            and self.application.game_settings.network_mode
            != NetworkMode.SERVER
        ):
            self.view.event_handler(event)
            self.start_game()
        elif event.event_type == EventType.ERROR:
            self.view.event_handler(event)
            self.message(event.error.title, event.error.message)

    def start_game(self):
        logger.info('Starting game')
        self.set_view(GameView)

    @property
    def fullscreen(self):
        return self._fullscreen

    @fullscreen.setter
    def fullscreen(self, value):
        if self._fullscreen != bool(value):
            self._fullscreen = bool(value)
            if self._fullscreen:
                # TODO: frame resizing
                self.screen = pygame.display.set_mode(
                    (0, 0), pygame.FULLSCREEN)
            else:
                self.screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
            self.resize_callbacks()

    def resize_callbacks(self):
        self.main_frame.on_resize(self._surface.get_size())
        for view in (*self.view_stack, self.view):
            view.on_resize(list(self.main_frame.dimensions))

    def get_clipboard(self):
        if self.clipboard_supported:
            return pygame.scrap.get_text()
        else:
            return ''

    def render(self):
        for event in pygame.event.get():
            logger.debug('handling event: %s', event)
            if event.type == pygame.QUIT:
                self.application.continue_ = False
                pygame.quit()
                return
            elif (self.focused_object is not None
                  and (len(self._focused_object_events) == 0
                       or event.type in self._focused_object_events)):
                self.focused_object.handle_event(event)
            elif event.type == pygame.KEYDOWN:
                self.pressed_scancodes[event.scancode] = True
                self.view.on_keydown(event)
            elif event.type == pygame.KEYUP:
                self.pressed_scancodes[event.scancode] = False
                self.view.on_keyup(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.view.on_mousedown(event.pos)
            elif event.type == pygame.MOUSEBUTTONUP:
                self.view.on_mouseup(event.pos)
            elif event.type == pygame.MOUSEWHEEL:
                self.view.on_scroll(
                    (-event.x * SCROLL_INCREMENT, -event.y * SCROLL_INCREMENT),
                    pygame.mouse.get_pos()
                )
            elif event.type == pygame.WINDOWRESIZED:
                self.resize_callbacks()
            elif event.type == pygame.WINDOWHIDDEN:
                self.shown = False
            elif event.type == pygame.WINDOWSHOWN:
                self.shown = True
            elif event.type in (pygame.FINGERDOWN, pygame.FINGERUP,
                                pygame.FINGERMOTION):
                if event.type != pygame.FINGERUP:
                    if (
                        event.type == pygame.FINGERMOTION and (
                            old_ev := self.touches.get(event.finger_id)
                        ) is not None
                    ):
                        self.view.on_scroll(
                            ((old_ev['x'] - event.x)
                             * self.main_frame.dimensions[0],
                             (old_ev['y'] - event.y)
                             * self.main_frame.dimensions[1]),
                            pygame.mouse.get_pos(),
                        )
                    self.touches[event.finger_id] = event.dict
                else:
                    try:
                        del self.touches[event.finger_id]
                    except KeyError:
                        # Ignore any KeyError
                        pass
            elif event.type == self.sound_list.sound_music_end_event:
                logger.info('music ended')
                self.sound_list.queue_next_music()
            elif event.type == pygame.JOYAXISMOTION:
                axis = self.last_joystick_positions[event.joy]['axis']
                previous_pos = axis.get(event.axis, 0)
                if event.axis % 2 == 0:
                    direction = (pygame.K_RIGHT, pygame.K_LEFT)
                else:
                    direction = (pygame.K_DOWN, pygame.K_UP)
                if previous_pos <= JOYSTICK_AXIS_THRESHOLD < event.value:
                    self.view.on_joystick_motion(direction[0], event)
                elif event.value < JOYSTICK_AXIS_THRESHOLD <= previous_pos:
                    self.view.on_joystick_motion(direction[1], event)
                axis[event.axis] = event.value
            elif event.type == pygame.JOYHATMOTION:
                hats = self.last_joystick_positions[event.joy]['hats']
                previous_pos = hats.get(event.hat, (0, 0))
                if event.value[0] < 0 and previous_pos[0] >= 0:
                    self.view.on_joystick_motion(pygame.K_LEFT, event)
                elif event.value[0] > 0 and previous_pos[0] <= 0:
                    self.view.on_joystick_motion(pygame.K_RIGHT, event)
                if event.value[1] < 0 and previous_pos[1] >= 0:
                    self.view.on_joystick_motion(pygame.K_DOWN, event)
                elif event.value[1] > 0 and previous_pos[1] <= 0:
                    self.view.on_joystick_motion(pygame.K_UP, event)
                hats[event.hat] = event.value
            elif event.type == pygame.JOYBUTTONDOWN:
                if event.button == JOYSTICK_BUTTON_ENTER:
                    self.view.on_joystick_motion(pygame.K_RETURN, event)
                elif event.button == JOYSTICK_BUTTON_BACK:
                    self.view.on_joystick_motion(pygame.K_ESCAPE, event)
            elif event.type == pygame.JOYDEVICEADDED:
                self.joysticks.append(pygame.joystick.Joystick(
                    event.device_index))
                self.joysticks[-1].init()

        self.view.render()

        bottom = self.main_frame.dimensions[1] - BUTTON_MARGIN[1]
        for notification in reversed(self.notifications):
            pos = notification.render(
                bottom, debug=self.application.settings.debug)
            bottom = pos[1] - BUTTON_MARGIN[1]
        oldest_notification_timestamp = (time.monotonic() * 1000
                                         - NOTIFICATION_DURATION)
        self.notifications = [n for n in self.notifications
                              if n.timestamp > oldest_notification_timestamp]

        if self.shown:
            pygame.display.update()

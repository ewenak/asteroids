#! /usr/bin/env python3

import functools
import logging
import webbrowser

from .constants import (
    END_TIMEOUT, MAX_BOT_NUMBER, MAX_LOCAL_PLAYER_NUMBER, CHAT_HEIGHT,
    CHAT_OPACITY, SPLITSCREEN_PLAYER_LAYOUTS, STATUS_LINE_HEIGHT,
    APP_TITLE, MULTIPLAYER_PORT_MINIMUM, MULTIPLAYER_PORT_MAXIMUM,
    PLAYER_COLORS,
)
from .logic.data import (ChangeNetworkModeEvent, ErrorCode, EventType,
                         GameMode, GameState, NetworkMode)
from .logic.ships import ShipProperties

try:
    import pygame
except ImportError:
    # A Back from Altair server can be run without pygame. It still needs to
    # access to the list of the views, for argparse's options, so let's allow
    # the import of views without it.
    pygame = None
else:
    from .controls import controls_list, GamepadControls
    from .frames import container_x, Frame, ProportionalToContainer
    from .renderer import Renderer
    from .status_line import Chat
    from .widgets import (Button, CompoundItem, NumberEntry, Menu, TextItem,
                          TextEntry)

logger = logging.getLogger(__name__)


class View:
    # A view is considered concrete if it can be displayed to the user
    # immediately, without needing specific instantiation options (see
    # PauseMenuView, which needs a reference to the GameView) or method call
    # (see ControlsConfigurationView, which needs setup_menu to be called).
    # These views can be passed as Settings.initial_view arguments and as
    # callbacks for buttons.
    concrete_view = False

    def __init__(self, application, interface):
        if pygame is None:
            raise ModuleNotFoundError(
                "pygame could not be loaded, but is necessary to play.")
        self.application = application
        self.interface = interface

    def on_keydown(self, event):
        pass

    def on_keyup(self, event):
        pass

    def on_click(self, pos):
        pass

    def on_mousedown(self, pos):
        pass

    def on_mouseup(self, pos):
        pass

    def on_resize(self, new_size):
        pass

    def on_joystick_motion(self, keyboard_equivalent, event):
        pass

    def on_scroll(self, movement, pos):
        pass

    def render(self):
        pass

    def update(self, dt):
        pass

    def event_handler(self, event):
        if event.event_type == EventType.ERROR:
            self.interface.previous_view()

    def enter(self):
        pass

    def exit(self):
        pass


class GameView(View):
    concrete_view = True

    def __init__(self, application, interface):
        super().__init__(application, interface)

        GamepadControls.used_joysticks_guid = []

        # We are "lazy-loading" the controls, as the players aren't always
        # available at the initialization
        self.controls = None

        self.on_resize(self.interface.main_frame.dimensions)

        self.chat = Chat(
            self.application, self.interface,
            Frame(self.interface.main_frame, (0, 0),
                  (container_x, CHAT_HEIGHT), opacity=CHAT_OPACITY),
            self.application.game_shell, self.application.game.players,
            player_id=self.application.game.player_id,
        )

    def pause(self):
        self.interface.set_view(PauseMenuView(
            self.application, self.interface, game_view=self))

    def quit(self, *, allow_play_again=True):
        if self.application.game_settings.network_mode == NetworkMode.OFFLINE:
            self.interface.set_view(MainMenuView)
        elif self.application.game_settings.network_mode == NetworkMode.CLIENT:
            self.interface.previous_view()
            if allow_play_again:
                self.interface.set_view(EndMultiplayerGameView)
        else:     # NetworkMode.SERVER:
            # Reset the game, to be ready to start another round. We still need
            # to keep the players.
            self.application.game.play_again_same_players()
            self.interface.previous_view()
            if not allow_play_again:
                # Quit ClientListView
                self.interface.previous_view()

    def quit_if_finished(self):
        # Check game status
        # If game ended, verify that no mod key is pressed and that we waited
        # for the end timeout before going back to the main menu
        if (self.application.game.state != GameState.PLAYING
                and self.application.game.get_timestamp()
                > self.application.game.end_time + END_TIMEOUT):
            logger.info('quitting game...')
            self.quit()
            return True
        return False

    def on_keydown(self, event):
        if (event.mod & ~(pygame.KMOD_NUM | pygame.KMOD_CAPS)
                == pygame.KMOD_NONE):
            quit_ = self.quit_if_finished()
            if quit_:
                return

        if event.key == pygame.K_ESCAPE:
            if not self.chat.collapsed:
                self.chat.collapsed = True
            else:
                self.pause()
        elif event.key == pygame.K_F12:
            self.application.settings.debug = \
                not self.application.settings.debug
        elif event.key == pygame.K_t:
            self.chat.collapsed = False

    def on_joystick_motion(self, keyboard_equivalent, event):
        self.quit_if_finished()

    def on_keyup(self, event):
        pass

    def on_mouseup(self, pos):
        quit_ = self.quit_if_finished()
        if quit_:
            return
        if self.chat.frame.contains_point(pos) and not self.chat.collapsed:
            self.chat.on_click(self.chat.frame.translate_point(pos))
        elif 0 < pos[1] < STATUS_LINE_HEIGHT:
            # We only checked for height. There is always a status line there
            # anyway.
            self.pause()

    def on_scroll(self, movement, pos):
        if self.chat.frame.contains_point(pos) and not self.chat.collapsed:
            self.chat.on_scroll(movement, self.chat.frame.translate_point(pos))

    def on_resize(self, new_size):
        if self.application.game.player_id is not None:
            # We're in an online game, we must show only a specific player
            ship_ids = [self.application.game.player_id]
        else:
            ship_ids = [ship.id for ship in
                        self.application.game.players.values()]
        layout = SPLITSCREEN_PLAYER_LAYOUTS[len(ship_ids)]
        width, height = new_size

        frames = [
            Frame(
                self.interface.main_frame,
                pos=ProportionalToContainer(pos),
                dimensions=ProportionalToContainer(dimensions),
                debug_name=name,
            ) for pos, dimensions, name in layout
        ]
        self.renderers = [
            Renderer(frame, self.application.game, ship_id)
            for frame, ship_id in zip(frames, ship_ids)
        ]

        # Separation bars between split screen surfaces
        absolute_layout = [((x * width, y * height), (w * width, h * height))
                           for (x, y), (w, h), name in layout]
        self.separation_bars = [
            b for (x, y), (w, h) in absolute_layout for b in
            (((x - 1, y), (x - 1, y + h)),
             ((x, y + h), (x + w, y + h)),
             ((x + w, y), (x + w, y + h)))
        ]

    def try_controls_instantiation(self):
        if self.application.game.player_id is None:
            if (len(self.application.game.players)
                    < self.application.game_settings.player_number):
                return
            self.controls = [
                config['type'](
                    self.interface, player, index, config['settings'])
                for index, (config, player) in enumerate(zip(
                    self.interface.controls,
                    self.application.game.players.values()
                ))
            ]
        elif player := self.application.game.players.get(
                self.application.game.player_id):
            index = 0
            config = self.interface.controls[index]
            self.controls = [config['type'](
                self.interface, player, index, config['settings']
            )]

    def event_handler(self, event):
        if (event.event_type == EventType.ERROR
            and event.error != ErrorCode.INVALID_SOUND_THEME):
            # Ignore INVALID_SOUND_THEME errors, they may happen during the
            # game but won't disrupt it.
            self.quit(allow_play_again=False)

    def render(self):
        for renderer in self.renderers:
            renderer.render(self.application.game.iter_objects(),
                            debug=self.application.settings.debug)

        if self.application.settings.debug:
            # We render it to the 3/4 of the screen, so that it does not
            # interfere with the debug info of the player in the top right
            self.interface.main_frame.draw_text(
                (3 * self.interface.main_frame.dimensions[0] / 4,
                 STATUS_LINE_HEIGHT),
                self.application.debug_info(),
            )

        self.chat.render()

        if self.controls is not None:
            for controls in self.controls:
                controls.render()

        for bar in self.separation_bars:
            self.interface.main_frame.draw_polygon(bar, 'white')

    def update(self, dt):
        if self.controls is None:
            # We don't necessarily have access to the players from the start,
            # so we defer the instantiation of the controls, which need it.
            self.try_controls_instantiation()
        else:
            for controls in self.controls:
                # Set acceleration to 0, it will be set to something else
                # during the controls handling if accelerating. It is what
                # makes the renderer zoom.
                controls.player.acceleration = 0
                controls.handle_inputs(dt)

        self.chat.update(dt)
        self.application.game.update(dt)

        for renderer in self.renderers:
            renderer.update()

    def exit(self):
        for renderer in self.renderers:
            renderer.frame.delete()


class MenuView(View):
    concrete_view = False

    menu_title = None

    def __init__(self, application, interface):
        super().__init__(application, interface)
        self.menu = Menu(self.interface, self.get_menu_title(),
                         self.get_menu_data())

    def on_keydown(self, event):
        self.menu.on_key_press(event.key)

    def on_keyup(self, event):
        pass

    def on_mousedown(self, pos):
        pass

    def on_mouseup(self, pos):
        self.menu.on_click(pos)

    def on_resize(self, new_size):
        self.menu.calc_positions()

    def on_joystick_motion(self, keyboard_equivalent, event):
        self.menu.on_key_press(keyboard_equivalent)

    def on_scroll(self, movement, pos):
        self.menu.on_scroll(movement, pos)

    def render(self):
        self.menu.render()

    def update(self, dt):
        self.menu.update(dt)

    def exit(self):
        self.menu.exit()

    def get_menu_data(self):
        return None

    def get_menu_title(self):
        return self.menu_title

    # Actions

    def play_offline(self):
        self.application.game.reset()
        self.application.game_settings.network_mode = NetworkMode.OFFLINE
        layout = SPLITSCREEN_PLAYER_LAYOUTS[
            self.application.game_settings.player_number]
        for i in range(self.application.game_settings.player_number):
            properties = ShipProperties(
                name=layout[i][2], color=PLAYER_COLORS[i])
            self.application.game.add_player(properties)
        self.application.game.start_game()

    def play_coop(self):
        self.application.game_settings.game_mode = GameMode.COOP
        self.play_offline()

    def play_pvp(self):
        self.application.game_settings.game_mode = GameMode.PVP
        self.play_offline()

    def play_team(self):
        self.application.game_settings.game_mode = GameMode.TEAM
        self.play_offline()

    def controls_type(self, player):
        new_controls = controls_list[
            controls_list.index(self.interface.controls[player]['type']) - 1]
        self.interface.controls[player] = {
            'type': new_controls,
            'settings': new_controls.default_settings(),
        }

    def configure_controls(self, player):
        self.interface.set_view(ControlsConfigurationView)
        self.interface.view.setup_menu(player)

    def quit(self):
        self.application.continue_ = False
        if self.interface.in_web_browser:
            pygame.quit()
            import platform
            # We're preventing accidental close during game, but disable it now
            platform.window.onbeforeunload = None
            # Close window in PWA, redirect to about if not in PWA
            if self.interface.in_pwa:
                platform.window.close()
            else:
                platform.window.location.assign(self.interface.exit_url)

    def fullscreen(self):
        self.interface.fullscreen = not self.interface.fullscreen

    def start_server(self):
        self.application.game.reset()
        self.interface.set_view(ClientListView)

    def validate_port_number(self, port):
        error_title = 'Error in port number'
        error_message = ('Please enter a valid number between 1024 and 65535, '
                         'or 0 to use any available one.')
        try:
            port = int(port)
        except ValueError:
            logger.exception('Not an integer')
            self.interface.message(title=error_title, message=error_message)
            raise
        if port != 0 and not (MULTIPLAYER_PORT_MINIMUM <= port
                              <= MULTIPLAYER_PORT_MAXIMUM):
            logger.error(
                'Not 0 or between %d and %d', MULTIPLAYER_PORT_MINIMUM,
                MULTIPLAYER_PORT_MAXIMUM)
            self.interface.message(title=error_title, message=error_message)
            raise ValueError(error_message)

    def start_online_game(self):
        self.application.game.start_game()
        self.interface.start_game()

    def open_about_page(self):
        # webbrowser.get() doesn't work in pygame-web, and webbrowser.open may
        # not open url in background, which would hang the game.
        if not self.interface.in_web_browser:
            browser = webbrowser.BackgroundBrowser(webbrowser.get().name)
            browser.open(self.interface.about_url)
        else:
            webbrowser.open(self.interface.about_url)

    def set_sound_theme_factory(self, path):
        def set_sound_theme():
            if self.interface.sound_list.load_sounds(path):
                self.back()
        return set_sound_theme

    def back(self):
        self.interface.previous_view()


class MainMenuView(MenuView):
    concrete_view = True

    menu_title = APP_TITLE

    def __init__(self, application, interface):
        super().__init__(application, interface)
        self.menu.set_highlighted_item(
            next(b for b in self.menu.items if b.interactive))

    def get_menu_data(self):
        return [
            Button('Play', self.play_offline, pygame.K_p),
            Button('Multiplayer game', MultiplayerGameMenuView, pygame.K_m),
            Button(lambda: (f'Fullscreen: '
                            f'{"on" if self.interface.fullscreen else "off"}'),
                   self.fullscreen, pygame.K_f),
            Button('Settings', SettingsMenuView, pygame.K_s),
            Button('About', self.open_about_page, pygame.K_a),
            Button('Quit', self.quit, pygame.K_q),
        ]


class MultiplayerGameMenuView(MenuView):
    concrete_view = True

    menu_title = 'Multiplayer game'

    def get_menu_data(self):
        menu_data = [
            Button('Local', LocalMultiplayerMenuView, pygame.K_l),
            Button('Join server', OnlineClientMenuView, pygame.K_j),
        ]
        if not self.interface.in_web_browser:
            menu_data.append(
                Button('Host server', OnlineHostMenuView, pygame.K_h))
        menu_data.append(Button('<- Back', self.back, pygame.K_ESCAPE))
        return menu_data


class LocalMultiplayerMenuView(MenuView):
    concrete_view = True

    menu_title = 'Custom game settings'

    def get_menu_data(self):
        menu_data = [
            NumberEntry('Player number',
                        self.application.game_settings.setter('player_number'),
                        pygame.K_m, minimum=1,
                        maximum=MAX_LOCAL_PLAYER_NUMBER),
            NumberEntry(
                'Bot number',
                self.application.game_settings.setter('bot_number'),
                pygame.K_b, minimum=0, maximum=MAX_BOT_NUMBER),
            Button('Co-op', self.play_coop, pygame.K_c),
            Button('PvP', self.play_pvp, pygame.K_p),
            Button('Team', self.play_team, pygame.K_t),
            Button('<- Back', self.back, pygame.K_ESCAPE),
        ]
        if self.interface.has_touch_support:
            # No local multiplayer (split-screen) on touch screen devices
            del menu_data[0]
        return menu_data


class OnlineClientMenuView(MenuView):
    concrete_view = True

    menu_title = 'Join a server'

    def get_menu_data(self):
        return [
            Button('Ready!', WaitGameStartView, pygame.K_p),
            TextEntry(
                'Name', self.application.settings.client.setter('player_name'),
                pygame.K_n, max_length=64,
            ),
            TextEntry(
                'Server address',
                self.application.settings.client.setter('server_address'),
                pygame.K_a,
            ),
            Button('<- Back', self.back, pygame.K_ESCAPE),
        ]


class OnlineHostMenuView(MenuView):
    concrete_view = True

    menu_title = 'Host a server'

    def get_menu_data(self):
        return [
            NumberEntry('Online player number',
                        self.application.game_settings.setter('player_number'),
                        pygame.K_m, minimum=1),
            NumberEntry('Bot number',
                        self.application.game_settings.setter('bot_number'),
                        pygame.K_b, minimum=0, maximum=MAX_BOT_NUMBER),
            TextEntry('Your name',
                      self.application.settings.host.setter('player_name'),
                      pygame.K_a),
            TextEntry('Port',
                      self.application.settings.host.setter('port_number'),
                      pygame.K_n, validator=self.validate_port_number),
            Button(
                lambda: (f'Allow players to join after the game start: '
                         f'{self.application.settings.host.join_during_game}'),
                self.application.settings.host.setter('join_during_game'),
                pygame.K_j,
            ),
            Button('Start server', self.start_server, pygame.K_p),
            Button('<- Back', self.back, pygame.K_ESCAPE),
        ]


class ClientListView(MenuView):
    concrete_view = True

    menu_title = 'Waiting clients...'

    def enter(self):
        self.application.emit_event(ChangeNetworkModeEvent(NetworkMode.SERVER))

    def get_menu_data(self):
        return [
            TextItem(lambda: (
                f'Serving at {self.application.web_io.ws_server.get_url()}'
            )),
            TextItem(self.get_connected_clients),
            Button('Start game', self.start_online_game, pygame.K_p),
            Button('Shutdown server', self.back, pygame.K_ESCAPE),
        ]

    def get_connected_clients(self):
        if self.application.web_io.ws_server.room is None:
            return 'Connected players (0/...): ...'
        players = (self.application.web_io.ws_server.room
                   .connected_players.values())
        players_name = '\n'.join(p.name for p in players)
        client_number = self.application.game_settings.player_number - 1
        return f"""Connected players ({len(players)}/{client_number}):
{players_name}"""

    def exit(self):
        self.application.emit_event(ChangeNetworkModeEvent(
            NetworkMode.OFFLINE))


class PauseMenuView(MenuView):
    concrete_view = False

    menu_title = 'Paused'

    def __init__(self, application, interface, game_view):
        super(MenuView, self).__init__(application, interface)
        # We're rendering directly on main frame, so that we see the game in
        # the background
        self.menu = Menu(self.interface, self.get_menu_title(),
                         self.get_menu_data(), frame=self.interface.main_frame)
        self.game_view = game_view

    def get_menu_data(self):
        return [
            Button('Resume', self.back, pygame.K_ESCAPE),
            Button('Main menu', MainMenuView, pygame.K_m),
            Button('Quit', self.quit, pygame.K_q),
        ]

    def update(self, dt):
        self.menu.update(dt)

    def render(self):
        self.game_view.render()
        self.menu.render(fill=False)


class WaitGameStartView(MenuView):
    concrete_view = True

    menu_title = 'Please wait for the game to start...'

    def enter(self):
        self.application.emit_event(ChangeNetworkModeEvent(NetworkMode.CLIENT))

    def get_menu_data(self):
        return [
            Button('Cancel', self.back, pygame.K_ESCAPE),
        ]

    def exit(self):
        self.application.emit_event(ChangeNetworkModeEvent(
            NetworkMode.OFFLINE))


class EndMultiplayerGameView(MenuView):
    concrete_view = True

    menu_title = 'Game over! Do you want to play again?'

    def get_menu_data(self):
        return [
            Button('Play again!', self.back, pygame.K_p),
            Button('Exit room', lambda: (self.back(), self.back()),
                   pygame.K_ESCAPE),
        ]

    def event_handler(self, event):
        if event.event_type == EventType.START_GAME:
            self.back()
        else:
            super().event_handler(event)


class SettingsMenuView(MenuView):
    concrete_view = True

    menu_title = 'Settings'

    def get_menu_data(self):
        def label_factory(i):
            return lambda: (f'Controls for player {i + 1}: '
                            f'{self.interface.controls[i]["type"].name}')

        items = [
            CompoundItem(
                Button(
                    label_factory(i),
                    functools.partial(self.controls_type, player=i),
                    pygame.K_1 + i,
                ),
                Button(
                    'Configure',
                    functools.partial(self.configure_controls, player=i)
                )
            ) for i in range(len(self.interface.controls))
        ]
        items.append(Button('Load sounds', LoadSoundsMenuView, pygame.K_l))
        items.append(Button('<- Back', self.back, pygame.K_ESCAPE))
        return items


class LoadSoundsMenuView(MenuView):
    concrete_view = True

    def __init__(self, application, interface):
        super().__init__(application, interface)

        def set_menu(sound_themes):
            items = [
                Button(f'{name} ({i + 1})', self.set_sound_theme_factory(path),
                       pygame.K_1 + i)
                for i, (name, path) in enumerate(sound_themes.items())
            ]
            items.append(
                Button('None (0)', self.set_sound_theme_factory(None),
                       pygame.K_0))
            items.append(
                Button('<- Back', self.back, pygame.K_ESCAPE))
            self.menu = Menu(self.interface, self.get_menu_title(), items)
        self.interface.sound_list.list_themes(callback=set_menu)

    menu_title = 'Load sounds'

    def get_menu_data(self):
        return [
            Button('Loading...', lambda *args: None),
            Button('<- Back', self.back, pygame.K_ESCAPE),
        ]


class ControlsConfigurationView(MenuView):
    concrete_view = False

    def __init__(self, application, interface):
        # Don't init the menu, it will be changed later
        super(MenuView, self).__init__(application, interface)
        self.controls_class = None
        self.player_number = None
        self.settings = None

    def item_text(self, key, item_information):
        text = (item_information.get(
            'text') or key.capitalize().replace('_', ' ')) + ': '
        format_ = item_information.get('format')
        if format_ is None:
            if item_information['type'] == 'key':
                def format_(k):
                    return pygame.key.name(k['key'])
            elif item_information['type'] == 'joystick':
                def format_(j):
                    return j.get_name()
            else:
                format_ = str

        def render_text():
            if self.settings[key] is None:
                return text + 'default'
            return text + format_(self.settings[key])
        return render_text

    def item_callback(self, key, item_information):
        set_setting = functools.partial(self.set_setting, key)
        type_ = item_information['type']
        if type_ == 'key':
            def callback():
                self.interface.set_view(KeyChooser(
                    self.application, self.interface, set_setting,
                ))
            return callback
        elif type_ == 'joystick':
            def callback():
                joysticks = [pygame.joystick.Joystick(i) for i in
                             range(pygame.joystick.get_count())]
                if len(joysticks) == 0:
                    self.interface.message('No joystick found')
                    return
                values = [(j.get_name(), j) for j in joysticks]

                callback = functools.partial(
                    self.check_joystick, setting_key=key)
                self.interface.set_view(ChoiceView(
                    self.application, self.interface, 'Choose a joystick',
                    values, callback,
                ))
            return callback
        elif type_ == 'list':
            def callback():
                text = item_information.get('text') or key.replace('_', ' ')
                title = f'Choose {text.lower()}'
                self.interface.set_view(ChoiceView(
                    self.application, self.interface, self.screen, title,
                    item_information['values'], set_setting
                ))
            return callback
        elif callable(type_):
            return functools.partial(type_, callback=set_setting)
        else:
            raise ValueError(f"Invalid item callback type {type_}: must be "
                             "'key', 'joystick', 'list' or a callable.")

    def set_setting(self, key, value):
        self.settings[key] = value

    def check_joystick(self, joystick, setting_key):
        for controls in self.interface.controls:
            for value in controls['settings'].values():
                if (
                    isinstance(value, pygame.joystick.JoystickType)
                    and value.get_guid() == joystick.get_guid()
                ):
                    self.interface.message('Already used joystick')
                    self.interface.set_view(self.interface.choice_view)
                    return
        self.set_setting(setting_key, joystick)

    def setup_menu(self, player_number):
        self.controls_class = self.interface.controls[player_number]['type']
        self.player_number = player_number
        self.settings = self.interface.controls[player_number]['settings']
        title = self.controls_class.name.capitalize().replace('_', ' ')
        self.menu = Menu(
            self.interface, title,
            [*(Button(self.item_text(key, item), self.item_callback(key, item))
               for key, item in self.controls_class.configuration.items()),
             Button('Save and exit', self.save_and_exit, pygame.K_ESCAPE)]
        )

    def save_and_exit(self):
        self.interface.controls[self.player_number]['settings'] = self.settings
        self.interface.previous_view()


class KeyChooser(MenuView):
    concrete_view = False

    menu_title = 'Press a key...'

    def __init__(self, application, interface, callback):
        self.callback = callback
        super().__init__(application, interface)

    def get_menu_data(self):
        return [Button('cancel', self.back)]

    def on_keydown(self, event):
        self.interface.previous_view()
        self.callback({'key': event.key, 'mod': event.mod,
                       'scancode': event.scancode})


class ChoiceView(MenuView):
    concrete_view = False

    def __init__(self, application, interface, title, choices,
                 callback):
        self.menu_title = title
        self.choices = choices
        self.callback = callback
        super().__init__(application, interface)

    def choose(self, choice):
        self.callback(choice)
        self.interface.previous_view()

    def get_menu_data(self):
        return [
            *(Button(label, functools.partial(self.choose, choice=obj))
              for label, obj in self.choices),
            Button('Cancel', self.back, pygame.K_ESCAPE),
        ]


class MessageView(MenuView):
    concrete_view = False

    def __init__(self, application, interface, title, message):
        self.menu_title = title
        self.message = message
        super().__init__(application, interface)

    def get_menu_data(self):
        return [
            TextItem(self.message, center_text=True),
            Button('OK', self.back)
        ]

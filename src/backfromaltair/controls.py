#! /usr/bin/env python3

import logging

import pygame
import numpy as np

from .constants import (BUTTON_MARGIN, VIRTUAL_JOYSTICK_BG_COLOR,
                        VIRTUAL_JOYSTICK_RADIUS, VIRTUAL_JOYSTICK_TOUCH_COLOR,
                        VIRTUAL_JOYSTICK_RATIO)
from .widgets import Button

logger = logging.getLogger(__name__)


class Controls:
    name = None
    configuration = {}

    def __init__(self, interface, player, player_index, settings=None):
        self.settings = settings if settings is not None else {}
        self.interface = interface
        self.player = player
        self.player_index = player_index

    def render(self):
        pass

    def handle_inputs(self, dt):
        pass

    @classmethod
    def default_settings(cls):
        return {k: v.get('default') for k, v in cls.configuration.items()}


class KeyboardControls(Controls):
    name = 'keyboard'
    configuration = {
        'thrust': {'type': 'key'},
        'left': {'type': 'key'},
        'right': {'type': 'key'},
        'fire': {'type': 'key'},
    }
    default_keys = {
        0: {'thrust': pygame.KSCAN_W, 'left': pygame.KSCAN_A,
            'right': pygame.KSCAN_D, 'fire': pygame.KSCAN_Q},
        1: {'thrust': pygame.KSCAN_UP, 'left': pygame.KSCAN_LEFT,
            'right': pygame.KSCAN_RIGHT, 'fire': pygame.KSCAN_RSHIFT},
        2: {'thrust': pygame.KSCAN_KP8, 'left': pygame.KSCAN_KP4,
            'right': pygame.KSCAN_KP6, 'fire': pygame.KSCAN_KP7},
        3: {'thrust': pygame.KSCAN_I, 'left': pygame.KSCAN_J,
            'right': pygame.KSCAN_L, 'fire': pygame.KSCAN_U}
    }

    def __init__(self, interface, player, player_index, settings=None):
        super().__init__(interface, player, player_index, settings)

        self.keys = {}
        for action, func in [('thrust', self.player.thrust),
                             ('left', self.player.turn_left),
                             ('right', self.player.turn_right),
                             ('fire', self.player.fire_event)]:
            if settings.get(action) is not None:
                key = settings[action]['scancode']
            else:
                key = self.default_keys[player_index][action]
            self.keys[key] = func

    def handle_inputs(self, dt):
        for k, func in self.keys.items():
            if self.interface.pressed_scancodes.get(k):
                func(dt)


class GamepadControls(Controls):
    name = 'gamepad / joystick'
    used_joysticks_guid = []
    configuration = {
        'joystick': {'type': 'joystick'},
    }

    def __init__(self, interface, player, player_index, settings=None):
        super().__init__(interface, player, player_index, settings)
        self.joystick = settings.get('joystick') or self.find_joystick()

    def find_joystick(self):
        for num in range(pygame.joystick.get_count()):
            joystick = pygame.joystick.Joystick(num)
            if joystick.get_guid() not in self.used_joysticks_guid:
                logger.info('Found joystick: %s', joystick.get_name())
                self.used_joysticks_guid.append(joystick.get_guid())
                return joystick
        return None

    def handle_inputs(self, dt):
        if self.joystick is None:
            return
        for b in range(self.joystick.get_numbuttons()):
            if self.joystick.get_button(b):
                self.player.fire_event(dt)
        if self.joystick.get_numaxes() >= 2:
            x_axis = self.joystick.get_axis(0)
            y_axis = 0 - self.joystick.get_axis(1)
        elif self.joystick.get_numballs() >= 2:
            x_axis, y_axis = self.joystick.get_ball(0)
        elif self.joystick.get_numhats() >= 1:
            x_axis, y_axis = self.joystick.get_hat(0)
        else:
            self.interface.previous_view()
            self.interface.message(
                'Unsupported joystick',
                f'Joystick {self.joystick.get_name()} unsupported')
            return
        self.player.turn_event(
            dt, round(x_axis, 2) * self.player.properties.turn_speed)
        self.player.thrust(
            dt, max(round(y_axis, 2), 0) * self.player.properties.acceleration)


class ButtonMobileControls(Controls):
    name = 'on-screen buttons'
    actions = [('turn left', 'left'),
               ('turn right', 'right'),
               ('fire', 'fire'),
               ('accelerate', 'accelerate')]
    configuration = {
        0: {'text': 'First button', 'type': 'list', 'values': actions,
            'default': actions[0][1]},
        1: {'text': 'Second button', 'type': 'list', 'values': actions,
            'default': actions[1][1]},
        2: {'text': 'Third button', 'type': 'list', 'values': actions,
            'default': actions[2][1]},
        3: {'text': 'Fourth button', 'type': 'list', 'values': actions,
            'default': actions[3][1]},
    }

    def __init__(self, interface, player, player_index, settings=None):
        super().__init__(interface, player, player_index, settings)
        self.left = False
        self.right = False
        self.accelerate = False
        self.fire = False

        actions_mapping = {'left': ('<', self.set_left),
                           'right': ('>', self.set_right),
                           'fire': ('!', self.set_fire),
                           'accelerate': ('^', self.set_accelerate)}
        self.buttons = [
            Button(interface=self.interface, frame=self.interface.main_frame,
                   pos=(0, 0), text=text, callback=func, fg_color=(42, 42, 42),
                   bg_color=None)
            for text, func in (
                actions_mapping[action] for action in settings.values())
        ]
        self.set_buttons_pos()
        self.pressed_buttons = []

    def set_left(self):
        self.left = True

    def set_right(self):
        self.right = True

    def set_accelerate(self):
        self.accelerate = True

    def set_fire(self):
        self.fire = True

    def render(self):
        for button in self.buttons:
            button.render()

    def set_buttons_pos(self):
        column_width = (
            (self.interface.main_frame.dimensions[0]
             - 2 * BUTTON_MARGIN[0]) / 6
        )
        line_height = self.interface.main_frame.dimensions[1] / 3
        for button, column in zip(self.buttons, (0, 1, 4, 5)):
            button.width = column_width - 2 * BUTTON_MARGIN[0]
            button.height = line_height - 2 * BUTTON_MARGIN[1]
            button.set_pos((column * column_width + 2 * BUTTON_MARGIN[0],
                            2 * line_height))
            # pygame touch event positions are in fraction of the screen
            button.pos_fraction = (
                button.topleft / self.interface.main_frame.dimensions,
                button.bottomright / self.interface.main_frame.dimensions
            )

    def handle_inputs(self, dt):
        self.left = False
        self.right = False
        self.accelerate = False
        self.fire = False
        for touch in self.interface.touches.values():
            for button in self.buttons:
                area = button.pos_fraction
                if (area[0][0] < touch['x'] < area[1][0]
                        and area[0][1] < touch['y'] < area[1][1]):
                    button.callback()
        if self.left:
            self.player.turn_left(dt)
        elif self.right:
            self.player.turn_right(dt)
        if self.fire:
            self.player.fire_event(dt)
        if self.accelerate:
            self.player.thrust(dt)


class Joystick:
    def __init__(self, interface, radius, player):
        self.interface = interface
        self.player = player
        self.radius = radius
        self.center = np.array((0, 0))
        self.finger_pos = np.array((0, 0))
        self.finger_id = None
        self.finger_radius = radius / 2

    def get_direction(self):
        if self.finger_id is None:
            return np.array((0, 0))
        diff = self.finger_pos - self.center
        direction = np.array((0, 0))
        if diff[0] > self.radius / 2:
            direction[0] = 1
        elif diff[0] < -self.radius / 2:
            direction[0] = -1
        if diff[1] > self.radius / 2:
            direction[1] = 1
        elif diff[1] < -self.radius / 2:
            direction[1] = -1
        return direction

    def render(self):
        if self.finger_id is None:
            return
        self.interface.main_frame.draw_circle(self.center, self.radius,
                                              VIRTUAL_JOYSTICK_BG_COLOR)
        diff = self.finger_pos - self.center
        if np.linalg.norm(diff) < self.radius / 2:
            touch_circle_center = self.center
        else:
            touch_circle_center = (diff / np.linalg.norm(diff)
                                   * self.finger_radius * 2 + self.center)
        self.interface.main_frame.draw_circle(
            touch_circle_center, self.finger_radius,
            VIRTUAL_JOYSTICK_TOUCH_COLOR)

    def handle_inputs(self, dt):
        if self.finger_id is None:
            for touch in self.interface.touches.values():
                if touch['x'] < VIRTUAL_JOYSTICK_RATIO:
                    self.finger_id = touch['finger_id']
                    self.finger_pos[:] = (
                        touch['x'] * self.interface.main_frame.dimensions[0],
                        touch['y'] * self.interface.main_frame.dimensions[1])
                    self.center[:] = self.finger_pos
                    break
        elif self.finger_id in self.interface.touches:
            touch = self.interface.touches[self.finger_id]
            self.finger_pos[:] = (
                touch['x'] * self.interface.main_frame.dimensions[0],
                touch['y'] * self.interface.main_frame.dimensions[1],
            )
        else:
            # There isn't anymore our finger identifier in event.touches,
            # set finger_id to None
            self.finger_id = None

        # get_direction returns a vector with 1, 0, or -1
        direction = self.get_direction()
        if direction[0] != 0:
            self.player.turn_event(
                dt, direction[0] * self.player.properties.turn_speed)
        # We want to accelerate when the joystick is up, but (0, 0) is at
        # the topleft corner
        if direction[1] < 0:
            self.player.thrust(dt)


class JoystickMobileControls(Controls):
    name = 'on-screen joystick'
    configuration = {
    }

    def __init__(self, interface, player, player_index, settings=None):
        super().__init__(interface, player, player_index, settings)
        self.joystick = Joystick(
            self.interface, VIRTUAL_JOYSTICK_RADIUS, self.player)
        self.firing_finger_id = None

    def render(self):
        self.joystick.render()

    def handle_inputs(self, dt):
        self.joystick.handle_inputs(dt)
        if self.firing_finger_id is None:
            for touch in self.interface.touches.values():
                if (touch['finger_id'] != self.joystick.finger_id
                        and touch['x'] > VIRTUAL_JOYSTICK_RATIO):
                    self.firing_finger_id = touch['finger_id']
                    break
        elif self.firing_finger_id not in self.interface.touches:
            self.firing_finger_id = None
        if self.firing_finger_id is not None:
            self.player.fire_event(dt)


touch_controls = [
    ButtonMobileControls,
    JoystickMobileControls,
]
controls_list = [
    *touch_controls,
    KeyboardControls,
    GamepadControls,
]

default_controls = KeyboardControls
default_mobile_controls = ButtonMobileControls

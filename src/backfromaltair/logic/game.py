#! /usr/bin/env python3

import heapq
import itertools
import logging
import random
import time

import numpy as np

from .base import SpaceObject
from .data import (AddWinnerEvent, Color, CreateObjectEvent, CreateShipEvent,
                   CreateStarEvent, EndGameEvent, EndSyncEvent, Event,
                   EventType, GameMode, GameSettings, GameState, NetworkMode,
                   PlayerQuitEvent, SoundEvent, SpaceObjectType,
                   StartGameEvent, StartSyncEvent, SyncObjectEvent)
from .items import BulletRefillItem, TankRefillItem
from .objects import Asteroid
from .ships import BotShip, PlayerShip

from ..constants import (
    # Global constants
    Sound,
    # Asteroid constants
    ASTEROID_COUNT, ASTEROID_FULL_SIZE,
    # Item constants
    ITEM_MAX_DELAY, ITEM_MIN_DELAY, ITEM_DELAY_RANGE_SIZE,
    # Star constants
    STAR_COLORS, STAR_SIZES,
)

logger = logging.getLogger(__name__)


class Game:
    def __init__(self, game_settings: GameSettings, emit_event=None):
        self.game_settings = game_settings

        self.end_time = 0

        # Set during online games
        self.player_id = None

        # Called for game / sound events
        self._emit_event = emit_event
        if self._emit_event is None:
            # If emit_event is not overriden, just do as if we received it
            # directly.
            self._emit_event = self.receive_event

        self._sync_id_counter = itertools.count()

        self._current_sync_id = None
        self._current_sync_events = []

        self.reset()

        Event.validate_function = self.validate_event

    def validate_event(self, event: Event):
        if event.event_type in (
                EventType.CREATE_OBJECT, EventType.CREATE_SHIP):
            return
        object_id = getattr(event, 'object_id', None)
        if object_id in self._hidden_objects:
            obj = self._hidden_objects.pop(object_id)
            self.objects[object_id] = obj
        if (object_id is not None and object_id not in self.objects
            and object_id not in self._hidden_objects):
            raise ValueError('invalid object_id')

    @property
    def dimensions(self):
        return self.game_settings.dimensions

    @property
    def game_mode(self):
        return self.game_settings.game_mode

    def get_timestamp(self):
        return time.monotonic() * 1000 - self.game_start_time

    def emit_event(self, event: Event):
        if (
            self.game_settings.network_mode == NetworkMode.CLIENT
            and hasattr(event, 'object_id')
            and (
                event.object_id != self.players[self.player_id].id
                or event.event_type not in (
                    EventType.CHANGE_MOVEMENT, EventType.TURN, EventType.FIRE)
            )
        ):
            # When running as a client, don't send events not related to
            # our player's control
            return
        event.timestamp = self.get_timestamp()
        self._emit_event(event)

    def receive_event(self, event: Event):
        self.apply_event(event)

    def apply_event(self, event: Event):
        if (
            hasattr(event, 'object_id')
            and (obj := self.objects.get(event.object_id)) is not None
            and obj.apply_event(event)
        ):
            # The event was consumed by the object.
            return
        # The event was not consumed by an object, let's apply it here
        if event.event_type == EventType.CREATE_OBJECT:
            obj_type = SpaceObject.object_types_register[event.object_type]
            args = {
                'center': event.center,
                'radius': event.radius,
                'movement': event.movement,
                'angle': event.angle,
                'rotation': event.rotation,
                'object_id': event.object_id,
            }
            obj = obj_type(game=self, **args)
            self.add_object(obj)
            obj.update(self.get_timestamp() - event.timestamp)
        elif event.event_type == EventType.CREATE_SHIP:
            obj_type = SpaceObject.object_types_register[event.object_type]
            args = {
                'center': event.center,
                'radius': event.radius,
                'movement': event.movement,
                'angle': event.angle,
                'rotation': event.rotation,
                'properties': event.ship_properties,
                'object_id': event.object_id,
            }
            obj = obj_type(game=self, **args)
            self.add_object(obj)
            obj.update(self.get_timestamp() - event.timestamp)
        elif event.event_type == EventType.CREATE_STAR:
            if self._star_index >= self.game_settings.star_number:
                logger.error(
                    'Too much stars created (%d, while %d were planned)',
                    self._star_index + 1, self.game_settings.star_number
                )
                self._star_index += 1
                return
            self.background_stars_pos[self._star_index] = event.center
            self.background_stars_attrs[self._star_index] = (
                event.size, tuple(event.color)
            )
            self._star_index += 1
        elif event.event_type == EventType.START_GAME:
            self.game_start_time = time.monotonic() * 1000 - event.timestamp
            self.state = GameState.PLAYING
        elif event.event_type == EventType.INIT_PLAYER:
            self.player_id = event.player_id
        elif event.event_type == EventType.START_SYNC:
            if self.game_settings.network_mode != NetworkMode.CLIENT:
                return
            self._current_sync_events.clear()
            self._current_sync_id = event.sync_id
        elif event.event_type == EventType.SYNC_OBJECT:
            if self.game_settings.network_mode != NetworkMode.CLIENT:
                return
            self._current_sync_events.append(event)
        elif event.event_type == EventType.END_SYNC:
            if self.game_settings.network_mode != NetworkMode.CLIENT:
                return
            if event.sync_id != self._current_sync_id:
                self._current_sync_events.clear()
                self._current_sync_id = None
                return
            for obj in self.iter_objects():
                obj.synced = False
            for ev in self._current_sync_events:
                obj = self.objects[ev.object_id]
                obj.center = ev.center
                obj.movement = ev.movement
                obj.angle = ev.angle
                obj.synced = True
            for obj in list(self.iter_objects()):
                if not obj.synced:
                    obj.delete()
            self._hidden_objects.clear()
        elif event.event_type == EventType.GAME_SETTINGS:
            if self.game_settings.network_mode != NetworkMode.CLIENT:
                return
            self.game_settings.dimensions = event.dimensions
            self.game_settings.star_density = event.star_density
            self.game_settings.player_number = event.player_number
            self.game_settings.bot_number = event.bot_number
            self.game_settings.game_mode = event.game_mode
        elif event.event_type == EventType.END_GAME:
            self.end_game()
        elif event.event_type == EventType.ADD_WINNER:
            player = self.players.get(event.player_id)
            if player is None:
                logger.error('Invalid winner: no player with id %s',
                             event.player_id)
                return
            if event.player_id == self.player_id:
                self.state = GameState.WON
            self.winners.append(player)
        elif event.event_type == EventType.PLAYER_QUIT:
            player = self.players[event.player_id]
            player.lives = 0
            player.destroyed = True
            player.quit_game = True

    def reset(self):
        logger.info('Game resetting')

        # All objects are in self.objects, but there are references to players
        # and bots in specific dicts, as they are very used.
        self.local_objects = {}
        self.objects = {}
        self.players = {}
        self.bots = {}
        self.scheduled_items = []
        self._hidden_objects = {}

        self.game_start_time = time.monotonic() * 1000

        self.last_full_sync = self.get_timestamp()

        # We add stars in a numpy array with a predefined length, we need to
        # keep track of its index
        self._star_index = 0

        self.background_stars_pos = np.zeros(
            (self.game_settings.star_number, 2))
        self.background_stars_attrs = [None] * self.game_settings.star_number

        self.state = GameState.INITIALIZED

        self.winners = []

        if self.game_settings.network_mode == NetworkMode.OFFLINE:
            self.player_id = None

    def start_game(self):
        logger.info('Starting game')

        self.init_ships()
        self.init_items()
        self.init_asteroids()
        self.init_stars()

        self.emit_event(StartGameEvent())

    def init_ships(self):
        for ship in self.players.values():
            self.send_object_event(ship)

        for _ in range(self.game_settings.bot_number):
            self.send_object_event(BotShip(
                self, (np.random.randint(0, self.dimensions[0]),
                       np.random.randint(0, self.dimensions[1])),
            ))

        if self.game_mode == GameMode.TEAM:
            team_size = len(self.players) // 2
            players = iter(self.players.values())
            for _ in range(team_size):
                next(players).team = 1
            for p in players:
                p.team = 2

    def init_items(self):
        # Schedule initial items. 1 means the tank is full and we have the max
        # number of bullets
        self.schedule_item(1, BulletRefillItem)
        self.schedule_item(1, TankRefillItem)

    def init_asteroids(self):
        for _ in range(ASTEROID_COUNT):
            center = np.array((
                np.random.randint(0, self.dimensions[0]),
                np.random.randint(0, self.dimensions[1]),
            ))
            asteroid = Asteroid(self, center, ASTEROID_FULL_SIZE)
            # Make sure we don't create an asteroid too near of a player
            while any(player.respawn_area.collides(asteroid)
                      for player in self.players.values()):
                center = np.array((
                    np.random.randint(0, self.dimensions[0]),
                    np.random.randint(0, self.dimensions[1])
                ))
                asteroid.center = center
            self.send_object_event(asteroid)

    def init_stars(self):
        for ev in self.create_stars_event():
            self.emit_event(ev)

    def update(self, dt):
        if self.state == GameState.PLAYING:
            if (
                self.game_settings.network_mode == NetworkMode.SERVER
                and self.last_full_sync + 500 < self.get_timestamp()
            ):
                self.send_sync_event()
            if self.game_settings.network_mode != NetworkMode.CLIENT:
                # Check if game is over
                self.check_game_state()

            self.spawn_items()
            # iter_objects lists items, asteroids and bullets and players
            # obj.update moves asteroids and bullets and checks item/player
            # collisions
            for obj in list(self.iter_objects()):
                obj.update(dt)

    def send_sync_event(self):
        sync_id = next(self._sync_id_counter)
        self.emit_event(StartSyncEvent(sync_id))
        for obj in self.objects.values():
            self.emit_event(SyncObjectEvent(
                obj.id, obj.center, obj.movement, obj.angle
            ))
        self.emit_event(EndSyncEvent(sync_id))
        self.last_full_sync = self.get_timestamp()

    def create_stars_event(self):
        for _ in range(len(self.background_stars_pos)):
            yield CreateStarEvent(
                (np.random.randint(0, self.dimensions[0]),
                 np.random.randint(0, self.dimensions[1])),
                random.choice(STAR_SIZES), Color(*random.choice(STAR_COLORS)),
            )

    def create_object_event(self, obj):
        if obj.object_type not in (
                SpaceObjectType.PLAYER, SpaceObjectType.BOT):
            return CreateObjectEvent(
                obj.object_type, obj.id, center=obj.center, radius=obj.radius,
                movement=obj.movement, angle=obj.angle, rotation=obj.rotation)
        else:
            return CreateShipEvent(
                obj.object_type, obj.id, center=obj.center, radius=obj.radius,
                movement=obj.movement, angle=obj.angle, rotation=obj.rotation,
                ship_properties=obj.properties)

    def send_object_event(self, obj):
        self.emit_event(self.create_object_event(obj))

    def add_object(self, obj, *, local=False):
        if local and self.game_settings.network_mode != NetworkMode.OFFLINE:
            # Don't mix local objects with others to avoid id conflicts
            self.local_objects[obj.id] = obj
            obj._local_object = True
            return
        if obj.id in self.objects:
            logger.error(
                'trying to add object %s with id %s, but there is already one '
                '(%s)', obj, obj.id, self.objects[obj.id])
            return
        self.objects[obj.id] = obj
        if obj.object_type == SpaceObjectType.PLAYER:
            self.players[obj.id] = obj
        elif obj.object_type == SpaceObjectType.BOT:
            self.bots[obj.id] = obj

    def iter_objects(self, *obj_types: SpaceObjectType):
        if len(obj_types) == 0:
            return itertools.chain(self.objects.values(),
                                   self.local_objects.values())
        return (obj for obj in itertools.chain(self.objects.values(),
                                               self.local_objects.values())
                if obj.object_type in obj_types)

    def iter_ships(self):
        return itertools.chain(self.players.values(), self.bots.values())

    def schedule_item(self, fraction, type_):
        """Schedule an item of given type_, for a time relative to fraction (it
        could be the fraction of fuel left for example)"""
        schedule = (int(fraction * ITEM_MAX_DELAY)
                    - np.random.randint(0, ITEM_DELAY_RANGE_SIZE))
        if schedule < ITEM_MIN_DELAY:
            schedule = ITEM_MIN_DELAY
        center = np.array((
            np.random.randint(0, self.dimensions[0]),
            np.random.randint(0, self.dimensions[1]),
        ))
        heapq.heappush(self.scheduled_items, (
            time.monotonic() * 1000 + schedule, type_(self, center)
        ))

    def spawn_items(self):
        """Schedule items spawn and add scheduled items"""
        if self.game_settings.network_mode == NetworkMode.CLIENT:
            return
        current_time = time.monotonic() * 1000
        while (len(self.scheduled_items) > 0
               and self.scheduled_items[0][0] < current_time):
            schedule, item = heapq.heappop(self.scheduled_items)
            self.send_object_event(item)
            if isinstance(item, BulletRefillItem):
                avg_bullets_number = (sum(
                    p.bullets_number for p in self.players.values())
                    / len(self.players))
                avg_max_bullets = (sum(
                    p.properties.max_bullets for p in self.players.values())
                    / len(self.players))
                fraction = avg_bullets_number / avg_max_bullets
            elif isinstance(item, TankRefillItem):
                avg_tank_filling = (sum(
                    p.tank_filling for p in self.players.values())
                    / len(self.players))
                avg_full_tank = (sum(
                    p.properties.full_tank for p in self.players.values())
                    / len(self.players))
                fraction = avg_tank_filling / avg_full_tank
            else:
                raise TypeError(f"Unknown item type {type(item)}")
            self.schedule_item(fraction, type(item))

    def check_game_state(self):
        # TODO: better game state checking
        if all(p.lives == 0 for p in self.players.values()):
            self.emit_event(EndGameEvent())

        if self.game_settings.game_mode == GameMode.COOP:
            # Check if there are not any asteroids anymore (can't use len on an
            # iterator)
            if next(self.iter_objects(SpaceObjectType.ASTEROID), None) is None:
                for p in self.players.values():
                    self.emit_event(AddWinnerEvent(p.id))
                self.emit_event(EndGameEvent())

        elif (self.game_settings.game_mode == GameMode.PVP
              and len(self.players) > 1):

            alive_players = []
            for p in self.players.values():
                if p.lives > 0:
                    alive_players.append(p)
                    if len(alive_players) > 1:
                        break

            if len(alive_players) == 1:
                self.emit_event(AddWinnerEvent(alive_players[0].id))
                self.emit_event(EndGameEvent())

        elif self.game_settings.game_mode == GameMode.TEAM:
            alive_teams = {p.team for p in self.players.values()
                           if p.lives > 0}
            if len(alive_teams) == 1:
                alive_team = next(iter(alive_teams))
                for p in self.players.values():
                    if p.team == alive_team:
                        self.emit_event(AddWinnerEvent(p.id))
                self.emit_event(EndGameEvent())

    def end_game(self):
        if self.state == GameState.WON:
            # state may be set to GameState.WON through AddWinnerEvent
            self.emit_event(SoundEvent(sound=Sound.WIN))
        elif self.state == GameState.PLAYING:
            self.state = GameState.LOST
            self.emit_event(SoundEvent(sound=Sound.LOSE))

        for player in self.players.values():
            print(player.statistics_text())

        self.end_time = self.get_timestamp()

    def play_again_same_players(self):
        """Reset the game, but keep the players with their IDs and names. This
        function is run after the end of online multiplayer games."""
        players = self.players
        self.reset()
        for p in players.values():
            if p.quit_game:
                continue
            self.players[p.id] = PlayerShip(
                self, (np.random.randint(0, self.dimensions[0]),
                       np.random.randint(0, self.dimensions[1])),
                properties=p.properties, object_id=p.id)

    def add_player(self, properties=None):
        ship = PlayerShip(
            self, (np.random.randint(0, self.dimensions[0]),
                   np.random.randint(0, self.dimensions[0])),
            properties=properties,
        )
        self.players[ship.id] = ship
        if self.state != GameState.INITIALIZED:
            self.send_object_event(ship)
        return ship.id

    def player_quit(self, player_id):
        if self.state != GameState.INITIALIZED:
            self.emit_event(PlayerQuitEvent(player_id))
        else:
            del self.players[player_id]

    def delete(self, obj):
        if not hasattr(obj, 'id'):
            raise AttributeError(f'Trying to delete {obj}, but it does not '
                                 'have any id.')
        if not obj._local_object:
            if self.game_settings.network_mode == NetworkMode.CLIENT:
                # Keep a reference to the object until next sync with server,
                # so the deletion can be reverted in case the client deleted
                # an object the server didn't (they're hidden anyway).
                self._hidden_objects[obj.id] = obj
            del self.objects[obj.id]
        else:
            del self.local_objects[obj.id]

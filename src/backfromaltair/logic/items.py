#! /usr/bin/env python3

import numpy as np

from .base import register_space_object, SpaceObject
from .data import SoundEvent, SpaceObjectType

from ..constants import ITEM_SIZE, Sound
from ..icons import BulletIcon, TankIcon


# TODO: handle actions in multiplayer online mode
class Item(SpaceObject):
    def __init__(self, game, center, radius=None, *, movement=None, angle=None,
                 rotation=None, shape=None, color=None, object_id=None):
        super().__init__(game, center, radius=ITEM_SIZE / 2,
                         movement=[0, 0], rotation=0, object_id=object_id)
        self.create_icon()
        self.game.emit_event(SoundEvent(sound=Sound.ITEM_APPEARED))

    def action(self, player):
        raise NotImplementedError(
            f'Item subclasses should override action. Item subclass '
            f'{type(self)} does not.')

    def create_icon(self):
        raise NotImplementedError(
            f'Item subclasses should override action. Item subclass '
            f'{type(self)} does not.')

    def calc_shape(self, angle=0, center=None):
        if center is None:
            center = np.array((0, 0))
        return np.array((
            center + (-self.radius, -self.radius),
            center + (self.radius, -self.radius),
            center + (self.radius, self.radius),
            center + (-self.radius, self.radius),
        ))

    def update(self, dt):
        for ship in self.game.iter_ships():
            if self.collides(ship):
                self.action(ship)
                self.delete_event()


@register_space_object(SpaceObjectType.ITEM_TANK_REFILL)
class TankRefillItem(Item):
    def action(self, player):
        self.game.emit_event(SoundEvent(sound=Sound.POWER_UP))
        player.tank_filling = player.properties.full_tank

    def create_icon(self):
        self.icon = TankIcon(self.radius * 1.5, fraction_filled=0)
        self.icon_width = self.icon.width
        self.icon_offset = np.array((self.radius - self.icon_width / 2,
                                     self.radius / 4))


@register_space_object(SpaceObjectType.ITEM_BULLET_REFILL)
class BulletRefillItem(Item):
    def action(self, player):
        self.game.emit_event(SoundEvent(sound=Sound.POWER_UP))
        player.bullets_number = player.properties.max_bullets

    def create_icon(self):
        self.icon = BulletIcon(self.radius * 1.5)
        self.icon_width = self.icon.width
        self.icon_offset = np.array((self.radius - self.icon_width / 2,
                                     self.radius / 4))

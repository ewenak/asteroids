#! /usr/bin/env python3

import math
import random

import numpy as np

from .base import register_space_object, SpaceObject, polygon
from .data import SoundEvent, SpaceObjectType

from ..constants import (
    # Global constants
    Sound,
    # Asteroid constants
    ASTEROID_FULL_SIZE, ASTEROID_MAX_ROTATION, ASTEROID_MIN_ROTATION,
    ASTEROID_MAX_SPEED, ASTEROID_MIN_SPEED, ASTEROID_SPLIT_COUNT,
    ASTEROID_SIDES,
    # Bullet constants
    BULLET_SIZE, BULLET_SPEED, BULLET_WIDTH,
)


@register_space_object(SpaceObjectType.AREA)
class Area(SpaceObject):
    def __init__(self, game, center, dimensions):
        self.game = game
        self.dimensions = np.array(dimensions)
        self.center = np.array(center)
        self.movement = np.array((0, 0))
        self.rotation = 0
        self.angle = 0
        self.destroyed = False
        self.color = 'white'

        self.box = self.calc_box()
        self.shape = self.calc_shape()

    def calc_box(self):
        return np.array(((self.center - self.dimensions // 2),
                         (self.center + self.dimensions // 2)))

    def calc_shape(self, angle=0, center=(0, 0)):
        return np.array((
            -self.dimensions / 2,
            (+self.dimensions[0] / 2, -self.dimensions[1] / 2),
            +self.dimensions / 2,
            (-self.dimensions[0] / 2, +self.dimensions[1] / 2),
        ))


@register_space_object(SpaceObjectType.ASTEROID)
class Asteroid(SpaceObject):
    def __init__(self, game, center, radius, *, movement=None, angle=None,
                 rotation=None, object_id=None):
        if movement is None:
            movement = self.get_movement()
        if rotation is None:
            rotation = self.get_rotation()
        super().__init__(game, center, radius, movement=movement, angle=angle,
                         rotation=rotation, object_id=object_id)

    @staticmethod
    def get_rotation():
        rot = random.uniform(ASTEROID_MIN_ROTATION, ASTEROID_MAX_ROTATION)
        if random.random() < 0.5:
            return -rot
        else:
            return rot

    @staticmethod
    def get_movement():
        x = np.random.randint(ASTEROID_MIN_SPEED * 10,
                              ASTEROID_MAX_SPEED * 10 + 1) / 10
        if random.random() < 0.5:
            x = -x
        y = np.random.randint(ASTEROID_MIN_SPEED * 10,
                              ASTEROID_MAX_SPEED * 10 + 1) / 10
        if random.random() < 0.5:
            y = -y
        return np.array((x / 10, y / 10))

    def calc_shape(self, angle=0, center=(0, 0)):
        return polygon(ASTEROID_SIDES, angle, center, self.radius)

    def split(self):
        if self.radius < ASTEROID_FULL_SIZE / (2**ASTEROID_SPLIT_COUNT):
            self.delete_event()
            return

        self.game.emit_event(SoundEvent(
            sound=Sound.ASTEROID_BY_RADIUS[self.radius],
        ))
        x, y = self.center
        asteroid1 = Asteroid(self.game, (x, y), self.radius / 2)
        self.game.send_object_event(asteroid1)
        asteroid2 = Asteroid(self.game, (x, y), self.radius / 2)
        self.game.send_object_event(asteroid2)
        self.delete_event()

    def update(self, dt):
        for bullet in self.game.iter_objects(SpaceObjectType.BULLET):
            if self.collides(bullet):
                bullet.ship.statistics.asteroids_hit += 1
                bullet.delete_event()
                self.split()
                return

        for ship in self.game.iter_ships():
            if self.collides(ship):
                ship.delete_event()
        super().update(dt)


@register_space_object(SpaceObjectType.BULLET)
class Bullet(SpaceObject):
    def __init__(self, game, center, radius=None, *, movement=None, angle=None,
                 rotation=None, speed=BULLET_SPEED, ship=None, object_id=None):
        if movement is None:
            movement = (0, 0)
        if radius is None:
            radius = BULLET_SIZE
        super().__init__(game, center, radius, movement=movement,
                         rotation=rotation, angle=angle, object_id=None)
        self.ship = ship
        self.speed = speed
        self.distance_travelled = 0
        self.game.emit_event(SoundEvent(sound=Sound.BULLET))

    def calc_shape(self, angle=0, center=(0, 0)):
        line = [
            np.array((math.cos(angle),
                      math.sin(angle))) * self.radius + center,
            np.array((math.cos(math.pi + angle),
                      math.sin(math.pi + angle))) * self.radius + center
        ]
        if BULLET_WIDTH > 0:
            # To make it have a width of BULLET_WIDTH
            vdiff = np.array((math.cos(angle + math.pi / 2),
                              math.sin(angle + math.pi / 2))) * BULLET_WIDTH
            return [
                line[0],
                line[0] + vdiff,
                line[1] + vdiff,
                line[1],
            ]
        else:
            return line

    def get_movement(self, angle=None, speed=None):
        if angle is None:
            angle = self.angle
        if speed is None:
            speed = self.speed
        return np.array((math.cos(angle), math.sin(angle))) * speed

    def update(self, dt):
        self.distance_travelled += self.speed * dt
        if self.distance_travelled >= self.ship.properties.bullet_range:
            self.delete()
        super().update(dt)

#! /usr/bin/env python3

import itertools
import logging
import math

import numpy as np

try:
    import collision
    shapely = None
    as_shapely_object = None
except ImportError:
    import shapely
    collision = None

    def as_shapely_object(shape):
        # Polygons must have at least 4 coordinates, and bullets have only 2
        if len(shape) < 4:
            return shapely.LineString(shape)
        else:
            return shapely.Polygon(shape)
else:
    try:
        # We only use them here, as they're compatible with collision and are
        # faster than collision's Vectors
        from pygame.math import Vector2
    except ImportError:
        # It seems pygame is not installed (we can still run in server mode).
        # Let's just use collision's default vectors.
        from collision import Vector as Vector2

from .data import (ChangeMovementEvent, DestroyObjectEvent, Event, EventType,
                   TurnEvent)

from ..constants import SPACE_OBJ_COLOR

logger = logging.getLogger(__name__)


def polygon(num_sides, tilt_angle, center, radius):
    points = np.zeros((num_sides, 2))
    for i in range(num_sides):
        points[i] = (
            radius * math.cos(2 * math.pi * i / num_sides
                              + tilt_angle) + center[0],
            radius * math.sin(2 * math.pi * i / num_sides
                              + tilt_angle) + center[1])
    return points


def calc_rotation_matrix(angle):
    cos = np.cos(angle)
    sin = np.sin(angle)
    return np.array((
        (cos, sin),
        (-sin, cos)
    ))


def get_wrap_translations(box, game, renderer):
    repetitions = [np.array((0, 0))]

    if (box[1][0] > game.dimensions[0]
            - renderer.game_frame.dimensions[0] / 2):
        repetitions.append(np.array((-game.dimensions[0], 0)))

    if (box[1][1] > game.dimensions[1]
            - renderer.game_frame.dimensions[1] / 2):
        repetitions.append(np.array((0, -game.dimensions[1])))

    if box[0][0] < renderer.game_frame.dimensions[0] / 2:
        repetitions.append(np.array((game.dimensions[0], 0)))

    if box[0][1] < renderer.game_frame.dimensions[1] / 2:
        repetitions.append(np.array((0, game.dimensions[1])))

    return np.array(
        [v1 + v2 for v1, v2
         in itertools.combinations(repetitions, 2)] + [np.array((0, 0))])


def register_space_object(object_type):
    def apply_type(cls):
        SpaceObject.object_types_register[object_type] = cls
        cls.object_type = object_type
        return cls
    return apply_type


class SpaceObject:
    _id_counter = itertools.count()
    object_types_register = {}

    def __init__(self, game, center, radius, *, movement=None, angle=None,
                 rotation=None, shape=None, color=None, object_id=None):
        self.game = game
        self.center = np.array(center)
        if movement is None:
            movement = (0, 0)
        self.movement = np.array(movement, dtype=float)
        self._movement_local = np.array(movement, dtype=float)
        if angle is None:
            self.angle = 0
        else:
            self.angle = angle
        if rotation is None:
            self.rotation = 0
        else:
            self.rotation = rotation
        self._angle_local = rotation
        self.destroyed = False
        self.radius = radius
        self.color = color if color is not None else SPACE_OBJ_COLOR
        self.shape = shape if shape is not None else self.calc_shape()
        if self.angle != 0:
            rotation_matrix = calc_rotation_matrix(self.angle)
            self.shape = self.shape @ rotation_matrix
        self.box = self.calc_box()
        if object_id is None:
            self.id = next(SpaceObject._id_counter)
        else:
            self.id = object_id
        self._local_object = False
        self.synced = True

    def calc_box(self):
        return np.array((self.center - self.radius, self.center + self.radius))

    def update_movement(self, dt):
        self.center = ((self.center + self.movement * dt)
                       % self.game.dimensions)

    def update(self, dt):
        self.update_movement(dt)
        self.box = self.calc_box()
        if self.rotation:
            self.angle = (self.angle + self.rotation * dt) % (2 * np.pi)
            self.shape = self.shape @ calc_rotation_matrix(
                self.rotation * dt)

    def calc_points(self, renderer):
        """Calculate translations to see the other sides of the game when the
        player is at the edge.
        Returns a list of shapes. Shapes are numpy arrays with coordinates
        relative to screen."""
        points = []

        viewport_translation = (renderer.zoom_factor + renderer.viewport_center
                                - renderer.viewport[0])

        for translation in get_wrap_translations(
                self.box, self.game, renderer):
            in_game_translation = (self.center - renderer.viewport_center
                                   + translation)

            points.append((self.shape + in_game_translation)
                          @ renderer.zoom_matrix + viewport_translation)

        return points

    def calc_shape(self, angle=0, center=(0, 0)):
        raise NotImplementedError(f'SpaceObject subclasses should override'
                                  f'calc_shape. {type(self)} does not.')

    def collides(self, other):
        if (self.box[0][0] < other.box[1][0]
                and self.box[1][0] > other.box[0][0]
                and self.box[0][1] < other.box[1][1]
                and self.box[1][1] > other.box[0][1]):
            if collision:
                poly1 = collision.Poly(Vector2(*self.center),
                                       [Vector2(*p) for p in self.shape], 0)
                poly2 = collision.Poly(Vector2(*other.center),
                                       [Vector2(*p) for p in other.shape], 0)
                if collision.collide(poly1, poly2):
                    return True
            else:
                p1 = as_shapely_object(self.shape + self.center)
                p2 = as_shapely_object(other.shape + other.center)
                if p1.intersects(p2):
                    return True
        return False

    def delete(self):
        if self.destroyed:
            return
        self.game.delete(self)
        self.destroyed = True

    def delete_event(self):
        if self.destroyed:
            return
        event = DestroyObjectEvent(self.id)
        self.game.emit_event(event)

    def change_movement_event(self, movement):
        self._movement_local = movement
        event = ChangeMovementEvent(
            object_id=self.id, movement=movement, center=self.center,
        )
        self.game.emit_event(event)

    def turn_event(self, dt, rotation):
        self._angle_local += rotation * dt
        event = TurnEvent(
            object_id=self.id, angle=self._angle_local, rotation=self.rotation,
        )
        self.game.emit_event(event)

    def apply_event(self, event: Event):
        """Apply an event on self. Returns True if the event's type is handled
        by the object, False if not."""
        logger.debug('Timestamp difference: %d',
                     self.game.get_timestamp() - event.timestamp)
        if event.event_type == EventType.CHANGE_MOVEMENT:
            self.movement[:] = event.movement
            self.center[:] = event.center
            self.update_movement(self.game.get_timestamp() - event.timestamp)
            return True
        elif event.event_type == EventType.DESTROY_OBJECT:
            self.delete()
            return True
        elif event.event_type == EventType.TURN:
            dt = self.game.get_timestamp() - event.timestamp

            self.angle = event.angle + self.rotation * dt

            self.shape[:] = self.calc_shape(event.angle)

            return True
        return False

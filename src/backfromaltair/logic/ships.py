#! /usr/bin/env python3

import logging
import math
import numpy as np

from .base import calc_rotation_matrix, register_space_object, SpaceObject
from .data import (Event, EventType, FireEvent, GameMode, NetworkMode,
                   SoundEvent, ShipProperties, ShipStatistics, SpaceObjectType)
from .objects import Area, Bullet

from ..constants import (
    # Global constants
    RESPAWN_AREA_RADIUS, Sound,
    # Bullet constants
    BULLET_SPEED, BULLET_SIZE, BULLET_SLOW_DOWN,
    # Player constants
    PLAYER_VERTICES, PLAYER_FULL_TANK, PLAYER_MIN_TIME_DEAD,
    # Bot constants
    BOT_TURN_THRESHOLD,
)

logger = logging.getLogger(__name__)


class Ship(SpaceObject):
    def __init__(self, game, center, radius=None, *, movement=None, angle=None,
                 rotation=None, properties=None, respawn_area=None,
                 object_id=None):
        if properties is None:
            properties = ShipProperties()

        self.properties = properties

        if radius is None:
            radius = self.properties.radius

        super().__init__(game, center, radius, movement=movement, angle=angle,
                         rotation=0, object_id=object_id)

        self.statistics = ShipStatistics()

        self.color = tuple(self.properties.color)
        self.tank_filling = self.properties.full_tank
        self.bullets_number = self.properties.max_bullets
        self.lives = self.properties.max_lives
        self.next_fire = None
        self.wait_to_respawn = False
        self.last_hit = 0

        self.quit_game = False
        self.death_timestamp = None

        self.acceleration = 0

        if respawn_area is not None:
            self.respawn_area = respawn_area
        else:
            self.respawn_area = Area(
                self.game, self.center,
                (RESPAWN_AREA_RADIUS, RESPAWN_AREA_RADIUS),
            )
            self.respawn_area.color = (50, 50, 50)

    def turn_left(self, dt):
        self.turn_event(dt, -self.properties.turn_speed)

    def turn_right(self, dt):
        self.turn_event(dt, self.properties.turn_speed)

    def fire_event(self, dt):
        event = FireEvent(object_id=self.id)
        self.game.emit_event(event)

    def fire(self, dt):
        if self.wait_to_respawn or self.destroyed:
            return

        bullets = []
        current_time = self.game.get_timestamp()

        # next_fire is None on first shoot
        if self.next_fire is None:
            self.next_fire = current_time

        while (self.next_fire <= current_time and self.bullets_number != 0):
            self.bullets_number -= 1
            self.statistics.shoots += 1

            center = (self.center
                      + (np.array((math.cos(self.angle), math.sin(self.angle)))
                         * (self.radius + BULLET_SIZE / 2)))
            movement = self.movement + (math.cos(self.angle) * BULLET_SPEED,
                                        math.sin(self.angle) * BULLET_SPEED)
            bullets.append(Bullet(
                self.game, center, movement=movement, angle=self.angle,
                ship=self,
            ))
            # Do not call send_object_event because a fire event was already
            # sent.
            self.game.add_object(bullets[-1], local=True)

            # Action / reaction, spaceship slows down after firing the bullet
            movement_change = (
                np.array((math.cos(self.angle), math.sin(self.angle)))
                * BULLET_SLOW_DOWN)
            self.movement += movement_change

            self.next_fire += self.properties.fire_period

    def thrust(self, dt, acceleration=None):
        if self.wait_to_respawn or self.destroyed:
            return

        if acceleration is None:
            acceleration = self.properties.acceleration
        elif acceleration == 0:
            # Some Controls may be always calling thrust with a different value
            # for acceleration (see GamepadControls.handle_inputs). This would
            # reset the acceleration the ship was undergoing when firing.
            return

        movement_change = np.array((
            (math.cos(self.angle) * acceleration * dt),
            (math.sin(self.angle) * acceleration * dt)
        )) / 10

        speed_change = math.hypot(movement_change[0], movement_change[1])
        if speed_change > self.tank_filling:
            return

        speed = self.speed(movement_change)

        if speed > self.properties.max_speed:
            speed = self.properties.max_speed

        self.tank_filling -= speed_change
        self.statistics.total_fuel += speed_change

        # Use _movement_local which will be directly updated to take into
        # account acceleration events which have not yet been applied

        summed_movement_vector = self._movement_local + movement_change

        self.change_movement_event(
            speed * summed_movement_vector
            / math.hypot(summed_movement_vector[0], summed_movement_vector[1]),
        )
        self.acceleration = acceleration / self.properties.acceleration

    def calc_shape(self, angle=0, center=(0, 0), radius=None):
        # We exceptionally add a radius parameter so the method can be used
        # without a ship object, for the status line.
        if radius is None:
            radius = self.radius

        points = np.zeros((len(PLAYER_VERTICES), 2))
        for i, a in enumerate(PLAYER_VERTICES):
            points[i] = (
                radius * math.cos(a + angle) + center[0],
                radius * math.sin(a + angle) + center[1]
            )
        return points

    def check_bullet_collisions(self):
        if self.game.game_mode == GameMode.PVP:
            for bullet in list(self.game.iter_objects(SpaceObjectType.BULLET)):
                if self.collides(bullet) and bullet.ship is not self:
                    bullet.ship.statistics.ships_hit += 1
                    self.delete_event()
                    bullet.delete_event()
        elif self.game.game_mode == GameMode.TEAM:
            for bullet in list(self.game.iter_objects(SpaceObjectType.BULLET)):
                if (bullet.ship.team != self.team
                        and self.collides(bullet)):
                    bullet.ship.statistics.ships_hit += 1
                    self.delete_event()
                    bullet.delete_event()

    def update(self, dt):
        if self.destroyed:
            return
        if (self.next_fire is not None
            and self.next_fire < self.game.get_timestamp()):
            # If we were firing, self.next_fire would have been updated before
            # calling self.update(). Since we are not firing, let's set it to
            # None to avoid firing lots of bullets next time we fire.
            self.next_fire = None
        if not self.wait_to_respawn and not self.destroyed:
            super().update(dt)
            self.check_bullet_collisions()
        else:
            # Time in ms
            if self.game.get_timestamp() < (self.last_hit
                                            + PLAYER_MIN_TIME_DEAD):
                return
            for o in self.game.iter_objects(
                    SpaceObjectType.BULLET, SpaceObjectType.ASTEROID):
                if o.collides(self.respawn_area):
                    return

            # No collision, we can respawn in the center
            self.center = (self.respawn_area.box[0]
                           + self.respawn_area.box[1]) / 2
            self.calc_box()

            # Reset angle, and recalculate shape so that they're synchronized
            self._angle_local = 0
            self.angle = 0
            self.shape = self.calc_shape()

            self.wait_to_respawn = False

    def speed(self, added_movement=None):
        if added_movement is None:
            added_movement = (0, 0)
        summed_movement = self.movement + added_movement
        return math.hypot(summed_movement[0], summed_movement[1])

    def delete(self):
        # Custom delete code
        if self.wait_to_respawn or self.lives == 0:
            return
        self.game.emit_event(SoundEvent(sound=Sound.SHIP_EXPLOSION))
        self.lives -= 1
        self.statistics.deaths += 1
        if self.lives == 0:
            self.destroyed = True
            self.death_timestamp = self.game.get_timestamp()
        else:
            self.change_movement_event(np.array((0, 0)))
            self.wait_to_respawn = True
            # In milliseconds
            self.last_hit = self.game.get_timestamp()

    def apply_event(self, event: Event):
        if event.event_type == EventType.FIRE:
            self.next_fire = event.timestamp
            # self.fire takes dt as parameter, but doesn't use it
            self.fire(dt=None)
            return True
        else:
            return super().apply_event(event)

    def statistics_text(self, *, debug=False):
        text = f"""Shoots: {self.statistics.shoots}
Asteroids hit: {self.statistics.asteroids_hit}
Ships hit: {self.statistics.ships_hit}
Total fuel used: {self.statistics.total_fuel / PLAYER_FULL_TANK * 100}
Deaths: {self.statistics.deaths}
"""
        if debug:
            text += f"""Position: {self.center}
"""
            if self.game.game_settings.network_mode == NetworkMode.CLIENT:
                text += f"""Player id: {self.game.player_id}
"""

        return text


@register_space_object(SpaceObjectType.PLAYER)
class PlayerShip(Ship):
    pass


@register_space_object(SpaceObjectType.BOT)
class BotShip(Ship):
    def __init__(self, game, center, radius=None, *, movement=None, angle=None,
                 rotation=None, properties=None, respawn_area=None,
                 object_id=None):
        if properties is None:
            properties = ShipProperties()

        self.properties = properties

        super().__init__(game, center, movement=[0, 0], rotation=0,
                         object_id=object_id)
        self.properties.max_speed = 0.2
        self.target_ship = None
        self.target_ship_distance = 0
        self.target_angle = self.angle
        self.transformed_pos = [0, 0]

    def select_target_ship(self):
        for ship in self.game.iter_ships():
            if ((self.target_ship is None
                 or (np.linalg.norm(self.center - ship.center))
                 < self.target_ship_distance)
                    and ship is not self and not ship.destroyed):
                return ship
        return None

    def update(self, dt):
        if self.destroyed:
            return
        if (self.target_ship is None or self.target_ship.destroyed
                or self.target_ship.wait_to_respawn):
            self.target_ship = self.select_target_ship()
            if self.target_ship is None:
                # No ship not destroyed was found
                self.delete_event()
                return

        # To decide in which direction to turn, we calculate the tangent of the
        # angle bot ship - target ship, by applying a transformation to get
        # into a frame where the bot is at (0, 0) with an angle of 0. Then we
        # can compare the tangent of our angle with BOT_TURN_THRESHOLD (which
        # is approximately the average dt * self.properties.turn_speed).
        #            |    Target
        #            |     /
        #            |    /
        #            |   /
        #            |  /
        #            | /
        #            |/)       <- we calculate this angle
        # ----------Bot------------------------->
        #            |
        #            |
        #            v

        self.target_ship_distance = np.linalg.norm(
            self.center - self.target_ship.center)
        transformed_pos = ((self.target_ship.center - self.center)
                           @ calc_rotation_matrix(-self.angle))
        self.transformed_pos = transformed_pos
        tan_bot_target_angle = (
            transformed_pos[1] / (abs(transformed_pos[0]) or 1))
        if tan_bot_target_angle > BOT_TURN_THRESHOLD:
            self.rotation = self.properties.turn_speed
        elif tan_bot_target_angle < -BOT_TURN_THRESHOLD:
            self.rotation = -self.properties.turn_speed
        elif transformed_pos[0] < 0:
            self.rotation = self.properties.turn_speed
        else:
            self.rotation = 0
            if self.target_ship_distance < self.properties.bullet_range:
                self.fire(dt)
        if transformed_pos[0] > 0:
            self.thrust(dt)
        super().update(dt)

    def statistics_text(self, *, debug=False):
        text = super().statistics_text(debug=debug)
        if debug:
            text += f"""Current angle: {self.angle}
Target ship center: {self.target_ship.center}
Target ship distance: {self.target_ship_distance}
Transformed pos: {self.transformed_pos}
"""
        return text

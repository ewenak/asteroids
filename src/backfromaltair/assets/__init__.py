#! /usr/bin/env python3

from pathlib import Path


def get_assets_directory():
    return Path(__file__).parent

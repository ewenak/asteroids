# Sound themes

Sound themes are directories or zip files. They must be in the following structure:

```
sound_theme
├── music
│   ├── 01-first_song.ogg
│   ├── 02-second_song.ogg
│   └── ...
└── sounds
    ├── asteroid_1.ogg
    ├── asteroid_2.ogg
    ├── asteroid_3.ogg
    ├── asteroid_4.ogg
    ├── bullet.ogg
    ├── lose.ogg
    ├── ship_explosion.ogg
    └── win.ogg
```

Only .ogg files will be played. If a sound file is not present, it will be
ignored and no sound will be played on that event. Music files will loop, being
played in order.

Only zip files are used in web version. Sound themes directories are automatically
converted to zip files in the web build script.

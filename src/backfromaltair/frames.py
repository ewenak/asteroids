#! /usr/bin/env python3

from dataclasses import dataclass
import logging
import re

import numpy as np
import pygame

from .constants import BUTTON_FONT_SIZE, INITIAL_FONT_SIZE, SCROLL_INCREMENT

logger = logging.getLogger(__name__)


_text_surf_cache = {}


def _dynamic_dimensions_to_absolute(dynamic_dimensions, container_dimensions):
    if callable(dynamic_dimensions):
        return np.array(dynamic_dimensions(container_dimensions), dtype='int')
    return np.array([
        dim(container_dim) if callable(dim) else dim
        for dim, container_dim in zip(
            dynamic_dimensions, container_dimensions)
    ], dtype='int')


@dataclass
class ProportionalToContainer:
    prop_constant: float | tuple[float, float]

    def __call__(self, dim):
        try:
            return [factor * d for factor, d in zip(self.prop_constant, dim,
                                                    strict=True)]
        except TypeError:
            try:
                return [self.prop_constant * d for d in dim]
            except TypeError:
                return self.prop_constant * dim


container_pos = container_dims = container_x = container_y = \
        ProportionalToContainer(1)


class Frame:
    def __init__(self, container, pos, dimensions, font=None, debug_name=None,
                 opacity=None):
        self.debug_name = debug_name
        self.dynamic_pos = pos
        self.dynamic_dimensions = dimensions

        self.subframes = []

        self.opacity = opacity

        if isinstance(container, Frame):
            self._container_frame = container
            self.on_resize(self._container_frame.dimensions)
            self._container_frame.subframes.append(self)
        elif isinstance(container, pygame.Surface):
            self._container_frame = None

            self.update_absolute_dimensions(container.get_size())

            if not (self.dimensions[0] == container.get_width()
                    and self.dimensions[1] == container.get_height()):
                raise NotImplementedError(
                    'Frame with pygame.Surface container and different '
                    'dimensions from the surface is not implemented right now.'
                )

            if tuple(pos) == (0, 0):
                self.surface = container
            else:
                raise NotImplementedError(
                    'Frame with pygame.Surface container and non-zero '
                    'position is not implemented right now.')

            if font is None:
                font = pygame.font.SysFont(None, INITIAL_FONT_SIZE)
        else:
            raise TypeError(f'container must be either a Frame or '
                            f'pygame.Surface object, not a {type(container)}')

        self.font = font if font is not None else self._container_frame.font

    def update_absolute_dimensions(self, container_dimensions):
        self.pos = _dynamic_dimensions_to_absolute(
            self.dynamic_pos, container_dimensions)
        self.dimensions = _dynamic_dimensions_to_absolute(
            self.dynamic_dimensions, container_dimensions)
        logger.debug('name: %s; container dimensions: %s; dynamic_pos: %s '
                     'effective pos: %s; dynamic_dimensions: %s effective '
                     'dimensions: %s', self.debug_name, container_dimensions,
                     self.dynamic_pos, self.pos, self.dynamic_dimensions,
                     self.dimensions)

    def on_resize(self, container_dimensions):
        self.update_absolute_dimensions(container_dimensions)

        if isinstance(self._container_frame, Frame):
            self.surface = pygame.Surface(self.dimensions)
            self.surface.set_alpha(self.opacity)

        for subframe in self.subframes:
            subframe.on_resize(self.dimensions)

    def delete(self):
        for subframe in self.subframes:
            subframe.delete()
        self._container_frame.subframes.remove(self)

    def contains_point(self, pos):
        bottomright = self.pos + self.dimensions
        if (self.pos <= pos).all() and (pos < bottomright).all():
            return True
        return False

    def translate_point(self, pos):
        return pos + self.pos

    def _process_position(self, pos):
        return pos

    def _set_font_size(self, size):
        # set_point_size is only supported by pygame-ce. If it is not
        # supported, a reasonable font size was chosen on initialization
        if hasattr(self.font, 'set_point_size'):
            self.font.set_point_size(size)

    def _split_words(self, text):
        words = re.split(r'\b', text)

        if len(words) == 1:
            return words

        if len(words[0]) > 0:
            words[1] = ''.join((words[0], words[1]))
        del words[0]

        return [''.join((words[i], words[i + 1]))
                for i in range(0, len(words), 2)]

    def _word_pos(self, words_size, max_width, pos=(0, 0)):
        x, y = 0, 0
        total_width = 0
        current_line_height = 0

        for word, (width, height) in words_size:
            if max_width is not None and x + width > max_width:
                x = 0
                y += current_line_height
                current_line_height = height
            yield word, (pos[0] + x, pos[1] + y)
            x += width
            if x > total_width:
                total_width = x
            if height > current_line_height:
                current_line_height = height
            if word.endswith('\n'):
                x = 0
                y += current_line_height
                current_line_height = 0
        yield None, (total_width, y + current_line_height)

    def _render_text_size(self, word, color, wraplength=None):
        if wraplength is None:
            surface = self.font.render(
                word.strip('\n'), antialias=False, color=color)
        else:
            surface = self.font.render(
                word.strip('\n'), antialias=False, color=color,
                wraplength=wraplength)
        return surface, surface.get_size()

    def draw_text(self, pos, text, color=None, point_size=None,
                  max_width=None):
        pos = self._process_position(pos)
        if color is None:
            color = (255, 255, 255)
        if point_size is None:
            point_size = BUTTON_FONT_SIZE

        cache_id = (text, tuple(color), point_size)

        if (text_surf_size := _text_surf_cache.get(cache_id)) is not None:
            self.surface.blit(text_surf_size[0], pos)
            return text_surf_size[1]

        self._set_font_size(point_size)

        # Multiline text rendering is only supported by pygame-ce, but it may
        # be better implemented / optimized than our custom code.
        if (('\n' not in text and max_width is None)
            or getattr(pygame, 'IS_CE', False)):
            text_surf, size = self._render_text_size(
                text, color, wraplength=max_width)
            if (
                (isinstance(color, pygame.Color) and (alpha := color.a) != 255)
                or (isinstance(color, tuple) and len(color) == 4
                    and (alpha := color[3]) != 255)
            ):
                text_surf.set_alpha(alpha)
            self.surface.blit(text_surf, pos)
            return size
        else:
            words_pos = list(self._word_pos(
                (self._render_text_size(word, color)
                 for word in self._split_words(text)),
                max_width, pos=pos))
            _, size = words_pos.pop()
            self.surface.blits(words_pos)
            return size

    def text_size(self, text, point_size=None, max_width=None):
        if point_size is None:
            point_size = BUTTON_FONT_SIZE
        self._set_font_size(point_size)
        if '\n' not in text and max_width is None:
            return self.font.size(text)
        else:
            words_sizes = (
                (word, self.font.size(word.strip('\n')))
                for word in self._split_words(text)
            )
            for word, (x, y) in self._word_pos(words_sizes, max_width):
                if word is None:
                    return x, y
            raise RuntimeError('should have ended with None')

    def get_linesize(self, point_size=None):
        if point_size is None:
            point_size = BUTTON_FONT_SIZE
        self._set_font_size(point_size)
        return self.font.get_linesize()

    def draw_rect(self, topleft, bottomright, stroke, fill=None):
        x1, y1 = self._process_position(topleft)
        x2, y2 = self._process_position(bottomright)

        if fill is not None:
            w, h = x2 - x1, y2 - y1
            pygame.draw.rect(self.surface, fill, (x1, y1, w, h))

        pygame.draw.lines(self.surface, stroke, closed=True,
                          points=((x1, y1),
                                  (x2, y1),
                                  (x2, y2),
                                  (x1, y2)))

    def draw_polygon(self, points, stroke, fill=None):
        if fill is not None:
            pygame.draw.polygon(self.surface, fill, points)
        pygame.draw.lines(self.surface, stroke, closed=True,
                          points=[self._process_position(p) for p in points])

    def draw_circle(self, center, radius, color='white'):
        pygame.draw.circle(self.surface, color, self._process_position(center),
                           radius)

    def draw_frame(self, pos, frame):
        pos = self._process_position(pos)

        if isinstance(frame, Frame):
            self.surface.blit(frame.surface, pos)
        elif isinstance(frame, pygame.Surface):
            self.surface.blit(frame, pos)
        else:
            raise TypeError(f"Unsupported type for draw_frame: {type(frame)}")

    def fill(self, color):
        self.surface.fill(color)

    def render(self):
        if self._container_frame is not None:
            self._container_frame.surface.blit(self.surface, self.pos)
        else:
            raise RuntimeError('tried to render a Frame without Frame '
                               'container')

    def render_outline(self, color='white'):
        if self._container_frame is not None:
            self._container_frame.draw_rect(
                self.pos - (1, 1), self.pos + self.dimensions, color)
        else:
            raise RuntimeError('tried to render a Frame without Frame '
                               'container')


class ScrollableFrame(Frame):
    def __init__(self, container, pos, real_dimensions, virtual_dimensions,
                 font=None, debug_name=None):
        self._virtual_dimensions = virtual_dimensions
        self._fill_color = None
        self.scroll_offset = np.array([0., 0.])
        super().__init__(container, pos, real_dimensions, font, debug_name)

    def update_absolute_dimensions(self, container_dimensions):
        super().update_absolute_dimensions(container_dimensions)
        self._real_dimensions = self.dimensions
        self.set_virtual_dimensions(self._virtual_dimensions,
                                    container_dimensions=container_dimensions)

    def set_virtual_dimensions(self, dimensions, container_dimensions=None):
        logger.debug('setting virtual dimensions to %s', dimensions)
        if container_dimensions is None:
            container_dimensions = self._container_frame.dimensions

        self._virtual_dimensions = dimensions
        self.dimensions = _dynamic_dimensions_to_absolute(
            self._virtual_dimensions, container_dimensions)
        self.min_offset = np.array([0, 0])
        self.max_offset = np.array([
            max(int(virt - real), 0) for virt, real in zip(
                self.dimensions, self._real_dimensions
            )
        ])
        self._check_offset_bounds()

        self.surface = pygame.Surface(self.dimensions)
        for subframe in self.subframes:
            subframe.on_resize(self.dimensions)
        logger.debug("set dimensions to virtual dimensions: %s",
                     self._virtual_dimensions)

    def _check_offset_bounds(self):
        for i in range(len(self.scroll_offset)):
            self.scroll_offset[i] = min(
                max(self.scroll_offset[i], self.min_offset[i]),
                self.max_offset[i],
            )

    def scroll_down(self, increment=SCROLL_INCREMENT):
        logger.debug("scroll_down")
        self.scroll_offset[1] += increment
        self._check_offset_bounds()

    def scroll_up(self, increment=SCROLL_INCREMENT):
        logger.debug("scroll_up")
        self.scroll_offset[1] -= increment
        self._check_offset_bounds()

    def scroll_right(self, increment=SCROLL_INCREMENT):
        logger.debug("scroll_right")
        self.scroll_offset[0] += increment
        self._check_offset_bounds()

    def scroll_left(self, increment=SCROLL_INCREMENT):
        logger.debug("scroll_left")
        self.scroll_offset[0] -= increment
        self._check_offset_bounds()

    def scroll(self, movement):
        logger.debug("scrolling by %s", movement)
        self.scroll_offset += movement
        self._check_offset_bounds()

    def scroll_to_top(self):
        self.scroll_offset[1] = 0

    def scroll_to_bottom(self):
        self.scroll_offset[1] = self.dimensions[1] - self._real_dimensions[1]

    def scroll_into_view(self, point1, point2=None):
        logger.debug("scroll_into_view(%s, %s)", point1, point2)
        if point2 is not None:
            self.scroll_into_view(point2)

        logger.debug("point1: %s; scroll_offset: %s; self.dimensions: %s; "
                     "surface: %s", point1, self.scroll_offset,
                     self.dimensions, self.surface)
        for i in range(len(self.scroll_offset)):
            if point1[i] < self.scroll_offset[i]:
                self.scroll_offset[i] = point1[i]
            elif point1[i] > self.scroll_offset[i] + self._real_dimensions[i]:
                self.scroll_offset[i] += point1[i] - (self.scroll_offset[i] +
                                                      self._real_dimensions[i])

    def fill(self, color):
        self._fill_color = color
        super().fill(color)

    def render(self):
        if self._fill_color is not None:
            self._container_frame.draw_rect(
                self.pos, self.pos + self._real_dimensions,
                self._fill_color, self._fill_color
            )
            self._fill_color = None
        self._container_frame.surface.blit(
            self.surface, self.pos - self.scroll_offset,
            area=(
                self.scroll_offset, self.scroll_offset + self._real_dimensions
            )
        )

    def handle_key_press(self, key):
        if key == pygame.K_HOME:
            self.scroll_to_top()
            return True
        elif key == pygame.K_END:
            self.scroll_to_bottom()
            return True
        elif key == pygame.K_PAGEUP:
            self.scroll_up(self.dimensions[1] * 0.8)
            return True
        elif key == pygame.K_PAGEDOWN:
            self.scroll_down(self.dimensions[1] * 0.8)
            return True
        elif key == pygame.K_UP:
            self.scroll_up()
            return True
        elif key == pygame.K_DOWN:
            self.scroll_down()
            return True
        return False

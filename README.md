# Back from Altair

My own take on this classic game. Design inspired by
[Super Retro Mega Wars](https://github.com/retrowars/retrowars).

## Playing

You can [play in your web browser](https://ewenak.gitlab.io/backfromaltair/).

Controls are listed in the
[about page](https://ewenak.gitlab.io/backfromaltair/about.html#controls)

To play locally, you need to install the following dependencies:
- [pygame-ce](https://pyga.me),
- [numpy](https://numpy.org),
- [aiohttp](https://docs.aiohttp.org/en/stable/),
- [platformdirs](https://github.com/platformdirs/platformdirs)
- And either [shapely](https://shapely.readthedocs.io/) or
  [collision](https://github.com/qwertyquerty/collision/).

Run `pip install git+https://gitlab.com/ewenak/backfromaltair` to download and
install this game and its dependencies or download it manually and run `pip
install -r requirements.txt` to install the dependencies.

The online version uses [pygame-web](https://pygame-web.github.io). You can
build it locally with the shell script `script/build_pygbag.sh`. To do so, you
need these build dependencies:
- [pygbag](https://pygame-web.github.io)
- rsync
- git
- wget

Enjoy!

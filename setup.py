#! /usr/bin/env python3

# Adapted from https://github.com/pypa/setuptools/discussions/3909#discussioncomment-5718455

import os
import sys
import tomllib
from pathlib import Path

from setuptools import Command, setup
from setuptools.command.build import build

sys.path.append(os.path.dirname(__file__))

from script.generate_events import read_data, generate_file     # NOQA: E402


class custom_build(build):
    sub_commands = [("generate_events", None), *build.sub_commands]


class generate_events(Command):
    def initialize_options(self):
        self.input_file = 'events/events.toml'
        self.output_file = 'backfromaltair/logic/data.py'
        self.generation_command = 'script/generate_events.py'

        self.input_data = None

        self.build_lib = None
        self.editable_mode = False

    def finalize_options(self):
        self.set_undefined_options("build_py", ("build_lib", "build_lib"))

    def read_input(self):
        self.input_file = Path(self.input_file)
        self.output_file = Path(self.build_lib) / self.output_file
        if self.input_data is None:
            with open(self.input_file, 'rb') as f:
                self.input_data = tomllib.load(f)

    def get_source_files(self):
        self.read_input()
        source_files = [str(self.input_file), str(self.generation_command)]
        if self.input_data.get('header_code_file') is not None:
            source_files.append(str(
                self.input_file.parent / self.input_data['header_code_file']))
        return source_files

    def get_outputs(self):
        return [self.output_file]

    def get_output_mapping(self):
        return {self.input_file: self.output_file}

    def run(self):
        if self.editable_mode:
            # Let developer run the generation script
            return
        self.read_input()
        self.output_file.parent.mkdir(parents=True, exist_ok=True)
        data = read_data(self.input_file)
        with open(self.output_file, 'w') as output:
            generate_file(data, parent_dir=self.input_file.parent,
                          output=output)

setup(
    cmdclass={"build": custom_build, "generate_events": generate_events},
)

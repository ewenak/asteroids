(function() {
    class WSBridge {
        constructor(url, on_open, on_message, on_error, on_close) {
            this.jsWebSocket = new WebSocket(url);
            this.jsWebSocket.binaryType = "arraybuffer";

            this.on_open = on_open;
            this.on_message = on_message;
            this.on_error = on_error;
            this.on_close = on_close;

            const self = this;
            function message_handler(event) {
                let data;
                if (event.data instanceof ArrayBuffer) {
                    data = new Uint8Array(event.data);
                } else if (event.data instanceof Blob) {
                    data = new Uint8Array(event.data.arrayBuffer());
                } else {
                    data = event.data;
                }
                console.log(data, event.data);
                self.on_message(event, data);
            }

            this.jsWebSocket.addEventListener("open", this.on_open);
            this.jsWebSocket.addEventListener("message", message_handler);
            this.jsWebSocket.addEventListener("error", this.on_error);
            this.jsWebSocket.addEventListener("close", this.on_close);
        }
        send_bytes(data) {
            // https://stackoverflow.com/a/16245768
            if (typeof data === "string") {
                const byteCharacters = atob(data);
                data = new Uint8Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    data[i] = byteCharacters.charCodeAt(i);
                }
            } else if (!data instanceof Uint8Array) {
                data = new Uint8Array(data);
            }
            this.jsWebSocket.send(data);
        }
        close() {
            this.jsWebSocket.close();
        }
    }
    window.WSBridge = WSBridge;
    function connectWebSocket(url, on_open, on_message, on_error, on_close) {
        return new WSBridge(url, on_open, on_message, on_error, on_close);
    }
    window.connectWebSocket = connectWebSocket;
})();
